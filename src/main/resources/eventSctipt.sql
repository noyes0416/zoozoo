USE ZOOZOO;

-------------------
-- 지난주 주간베스트글 삭제 Batch Script
-- 매월 첫째날 00:15 실행
-------------------
CREATE EVENT delete_weeklyBestBoard
ON SCHEDULE EVERY 1 WEEK STARTS '2022-07-31 00:20:00'
DO
	DELETE FROM ZooZooStat.WeeklyBestBoard;

-------------------
-- 주간베스트글 Batch Script
-- 매주 일요일 00:20 실행
-------------------
CREATE EVENT insert_weeklyBestBoard
ON SCHEDULE EVERY 1 WEEK STARTS '2022-07-31 00:20:00'
DO
	INSERT INTO ZooZooStat.WeeklyBestBoard (year, month, week, boardNo, title, selectionType)
	SELECT YEAR(NOW())
	      ,MONTH(NOW())
	      ,Week(NOW())
	      ,B.typeValue
	      ,C.title
	      ,D.selectionType
	FROM   Board          A
	JOIN   Likes          B ON A.boardNo     = B.typeValue AND B.type = 2
	JOIN   Text           C ON A.boardNo     = C.typeValue AND C.type = 2
	JOIN   SelectionValue D ON A.selectionNo = D.selectionNo
	WHERE  A.displayYN = 'Y'
	AND    B.regDt     >= DATE_SUB(DATE_FORMAT(CURRENT_TIMESTAMP, '%Y-%m-%d'), INTERVAL 7 DAY)
	AND    B.regDt     <= DATE_FORMAT(CURRENT_TIMESTAMP, '%Y-%m-%d')
	GROUP BY B.typeValue, C.title, D.selectionType
	ORDER BY COUNT(B.typeValue) DESC, B.typeValue
	LIMIT 3;

-------------------
-- 지난달 월간베스트글 삭제 Batch Script
-- 매월 첫째날 00:25 실행
-------------------
CREATE EVENT delete_monthlyBestBoard
ON SCHEDULE EVERY 1 MONTH STARTS '2022-08-01 00:25:00'
DO
	DELETE FROM ZooZooStat.MonthlyBestBoard;

-------------------
-- 월간베스트글 Batch Script
-- 매월 첫째날 00:30 실행
-------------------
CREATE EVENT insert_monthlyBestBoard
ON SCHEDULE EVERY 1 MONTH STARTS '2022-08-01 00:30:00'
DO
	INSERT INTO ZooZooStat.MonthlyBestBoard (year, month, boardNo, title, selectionType)
	SELECT YEAR(NOW())  AS year
          ,MONTH(NOW()) AS month
          ,B.typeValue  AS boardNo
          ,C.title
          ,D.selectionType
    FROM   Board          A
    JOIN   Likes          B ON A.boardNo     = B.typeValue AND B.type = 2
    JOIN   Text           C ON A.boardNo     = C.typeValue AND C.type = 2
    JOIN   SelectionValue D ON A.selectionNo = D.selectionNo
    WHERE  A.displayYN = 'Y'
    AND    A.regDt BETWEEN (LAST_DAY(DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY) - INTERVAL 1 MONTH) + INTERVAL 1 DAY)
                   AND LAST_DAY(DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY))
    GROUP BY B.typeValue, C.title, D.selectionType
    ORDER BY COUNT(B.typeValue) DESC, B.typeValue
    LIMIT 3;