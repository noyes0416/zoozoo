-- Calendar Sample
INSERT INTO Text(type, typeValue, content, creator) VALUES(1, 1, 'Calendar text test', 1);
INSERT INTO Text(type, typeValue, content, creator) VALUES(1, 2, 'Second Calendar text test', 1);
UPDATE Record
SET updDt = now()
WHERE recordNo = 1;

UPDATE Record
SET updDt = now()
WHERE recordNo = 2;

-- Board Sample
INSERT INTO Text(type, typeValue, title, content, creator) VALUES(2, 1, 'First Board Title', 'First Board Content', 1);
INSERT INTO Text(type, typeValue, title, content, creator) VALUES(2, 2, 'Second Board Title', 'Second Board Content', 1);

-- Board Reply Sample
INSERT INTO Text(type, typeValue, content, creator) VALUES(3, 1, 'First Reply', 1);
INSERT INTO Text(type, typeValue, content, creator) VALUES(3, 2, '2 depth of First Reply', 1);
INSERT INTO Text(type, typeValue, content, creator) VALUES(3, 3, 'Second Reply', 2);
INSERT INTO Text(type, typeValue, content, creator) VALUES(3, 4, 'Third Reply', 1);

