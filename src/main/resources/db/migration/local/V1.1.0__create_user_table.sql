CREATE TABLE IF NOT EXISTS User (
	 userNo     BIGINT       AUTO_INCREMENT
	,userName   VARCHAR(20)  NOT NULL UNIQUE
	,email      VARCHAR(50)  NOT NULL
	,snsType    TINYINT      NOT NULL
	,tokenKey   VARCHAR(512) NULL
	,tokenValue VARCHAR(512) NULL
	,state      TINYINT      NOT NULL DEFAULT 1
	,regDt      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP
	,updDt	    DATETIME     NULL
	,PRIMARY KEY(userNo)
	,UNIQUE KEY User_email_snsType (email, snsType)
);

CREATE INDEX User_tokenKey ON User(tokenKey);

INSERT INTO User(userName, email, snsType) VALUES('애마곤듀', 'gw.myung@kt.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('예지체고', 'shiny.g.1203@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('Vx51E992Bm', 'devkang0001@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('first', 'first@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('second', 'second@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('third', 'third@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('4444', '4444@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('fifth', 'fifth@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('sixth', 'sixth@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('seventh', 'seventh@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('eight', 'eight@gmail.com', 1);
INSERT INTO User(userName, email, snsType) VALUES('999999999', '999999999@gmail.com', 1);