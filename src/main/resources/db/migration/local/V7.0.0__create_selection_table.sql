CREATE TABLE IF NOT EXISTS SelectionType (
	 selectionType  BIGINT AUTO_INCREMENT
	,description    VARCHAR(100) NOT NULL
	,regDt   		DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	,updDt	 		DATETIME NULL
	,PRIMARY KEY(selectionType)
);

CREATE TABLE IF NOT EXISTS SelectionValue (
	 selectionNo    BIGINT NOT NULL
	,selectionType  BIGINT NOT NULL
	,firstDepth     INT NOT NULL DEFAULT 0
	,secondDepth    INT NOT NULL DEFAULT 0
	,description    VARCHAR(100) NOT NULL
	,regDt   		DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	,updDt	 		DATETIME NULL
	,PRIMARY KEY(selectionType, firstDepth, secondDepth)
);