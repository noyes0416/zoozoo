CREATE TABLE IF NOT EXISTS Text (
	 textNo  		    BIGINT AUTO_INCREMENT
	,type 				TINYINT NOT NULL
	,typeValue			BIGINT NOT NULL
	,title              VARCHAR(300)
	,content 			VARCHAR(5000)
	,creator     	    BIGINT NOT NULL
	,lastUpdUser 	    BIGINT NULL
	,regDt   			DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	,updDt	 			DATETIME NULL
	,PRIMARY KEY(textNo)
);

CREATE INDEX Text_type_typeValue ON Text(type, typeValue);
