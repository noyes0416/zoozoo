CREATE TABLE IF NOT EXISTS Report (
    reportNo      BIGINT   AUTO_INCREMENT
   ,type          TINYINT      NOT NULL
   ,typeValue     BIGINT       NOT NULL
   ,reporterNo    BIGINT       NOT NULL
   ,reportTypeNo  TINYINT      NOT NULL
   ,note          VARCHAR(250) NULL
   ,state         TINYINT      NOT NULL DEFAULT 1
   ,regDt         DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP
   ,updDt         DATETIME     NULL
   ,PRIMARY KEY(reportNo)
);

CREATE TABLE IF NOT EXISTS UserReport (
    userNo          BIGINT       NOT NULL
   ,reportedCount   TINYINT      NOT NULL DEFAULT 0
   ,tempDeniedCount TINYINT      NOT NULL DEFAULT 0
   ,note            VARCHAR(250) NULL
   ,regDt           DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP
   ,updDt           DATETIME     NULL
   ,PRIMARY KEY(userNo)
);