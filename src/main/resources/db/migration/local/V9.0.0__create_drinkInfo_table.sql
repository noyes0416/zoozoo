CREATE TABLE IF NOT EXISTS DrinkInfo (
	 mstDrinkInfoNo	    BIGINT      AUTO_INCREMENT
	,type               TINYINT NOT NULL
	,typeValue          BIGINT NOT NULL
	,location 			VARCHAR(500) NULL
	,price 				INT NULL
	,sharedMemberNos    VARCHAR(500) NULL
	,settlementYn 		CHAR(1) NULL
	,drunkenYn   		CHAR(1) NULL
	,creator     	    BIGINT NOT NULL
	,lastUpdUser 	    BIGINT NULL
	,regDt   			DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	,updDt	 			DATETIME NULL
	,PRIMARY KEY(mstDrinkInfoNo)
);

CREATE TABLE IF NOT EXISTS DrinkInfoDtl (
	 drinkInfoNo		BIGINT NOT NULL
	,mstDrinkInfoNo	    BIGINT NOT NULL
    ,selectionType      BIGINT NOT NULL
    ,selectionNos       VARCHAR(200) NOT NULL
    ,manualValue        VARCHAR(100) NULL
	,creator     	    BIGINT NOT NULL
	,lastUpdUser 	    BIGINT NULL
	,displayYn          CHAR(1) NOT NULL DEFAULT 'Y'
	,regDt   			DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	,updDt	 			DATETIME NULL
	,PRIMARY KEY(mstDrinkInfoNo, selectionType)
);