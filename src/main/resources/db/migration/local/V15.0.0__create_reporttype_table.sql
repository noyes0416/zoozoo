CREATE TABLE IF NOT EXISTS ReportType (
    reportTypeNo TINYINT     NOT NULL
   ,reason       VARCHAR(50) NOT NULL
   ,PRIMARY KEY(reportTypeNo)
);

INSERT INTO ReportType(reportTypeNo, reason) VALUES (1, '욕설');
INSERT INTO ReportType(reportTypeNo, reason) VALUES (2, '사생활 침해');
INSERT INTO ReportType(reportTypeNo, reason) VALUES (3, '음란성');
INSERT INTO ReportType(reportTypeNo, reason) VALUES (4, '도배');
INSERT INTO ReportType(reportTypeNo, reason) VALUES (5, '홍보');
INSERT INTO ReportType(reportTypeNo, reason) VALUES (99, '기타');
