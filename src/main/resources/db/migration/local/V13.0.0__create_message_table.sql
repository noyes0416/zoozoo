CREATE TABLE IF NOT EXISTS Message (
	 messageNo	    BIGINT   AUTO_INCREMENT
	,senderNo 	    BIGINT   NOT NULL
	,receiverNo		BIGINT   NOT NULL
    ,readYN         CHAR(1)  NOT NULL DEFAULT 'N'
    ,displayYN      CHAR(1)  NOT NULL DEFAULT 'Y'
	,regDt          DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	,updDt	 		DATETIME NULL
	,PRIMARY KEY(messageNo)
);

INSERT INTO Message(senderNo, receiverNo) VALUES(1, 2);
INSERT INTO Text(type, typeValue, content, creator) VALUES (4, 1, 'From 1 To 2 Message', 1);
INSERT INTO Message(senderNo, receiverNo) VALUES(1, 2);
INSERT INTO Text(type, typeValue, content, creator) VALUES (4, 2, 'From 1 To 2 Message 22', 1);
INSERT INTO Message(senderNo, receiverNo) VALUES(2, 1);
INSERT INTO Text(type, typeValue, content, creator) VALUES (4, 3, 'From 2 To 1 Message', 2);
INSERT INTO Message(senderNo, receiverNo) VALUES(2, 1);
INSERT INTO Text(type, typeValue, content, creator) VALUES (4, 4, 'From 2 To 1 Message 22', 2);
INSERT INTO Message(senderNo, receiverNo) VALUES(1, 3);
INSERT INTO Text(type, typeValue, content, creator) VALUES (4, 5, 'From 1 To 3 Message', 1);