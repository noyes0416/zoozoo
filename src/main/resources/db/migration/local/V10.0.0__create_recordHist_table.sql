CREATE TABLE IF NOT EXISTS RecordHist (
    seq                         BIGINT       AUTO_INCREMENT
    ,recordRecordNo	            BIGINT
    ,recordCalNo	            BIGINT
    ,recordYear	                VARCHAR(4)
    ,recordMonth	            VARCHAR(2)
    ,recordDay	                VARCHAR(2)
    ,recordCreator	            BIGINT
    ,recordLastUpdUser	        BIGINT
    ,recordRegDt	            DATETIME
    ,recordUpdDt	            DATETIME
    ,imageMstMstImageNo	        BIGINT
    ,imageMstType	            TINYINT
    ,imageMstTypeValue	        BIGINT
    ,imageMstRegDt	            DATETIME
    ,imageMstUpdDt	            DATETIME
    ,imageDtlImageNo	        BIGINT
    ,imageDtlMstImageNo	        BIGINT
    ,imageDtlFileNm	            VARCHAR(256)
    ,imageDtlFilePath	        VARCHAR(512)
    ,imageDtlCreator	        BIGINT
    ,imageDtlLastUpdUser	    BIGINT
    ,imageDtlRegDt	            DATETIME
    ,imageDtlUpdDt	            DATETIME
    ,textMstTextNo	            BIGINT
    ,textType	                TINYINT
    ,textTypeValue	            BIGINT
    ,textContent	            VARCHAR(5000)
    ,textCreator	            BIGINT
    ,textLastUpdUser	        BIGINT
    ,textRegDt	                DATETIME
    ,textUpdDt	                DATETIME
    ,drinkInfoMstDrinkInfoNo	BIGINT
    ,drinkInfoType	            TINYINT
    ,drinkInfoTypeValue	        BIGINT
    ,drinkInfoLocation	        VARCHAR(500)
    ,drinkInfoPrice	            INTEGER
    ,drinkInfoSharedMemberNos	VARCHAR(500)
    ,drinkInfoSettlementYn	    CHAR(1)
    ,drinkInfoDrunkenYn	        CHAR(1)
    ,drinkInfoCreator	        BIGINT
    ,drinkInfoLastUpdUser	    BIGINT
    ,drinkInfoRegDt	            DATETIME
    ,drinkInfoUpdDt	            DATETIME
    ,delUserNo                  BIGINT NOT NULL
    ,delDt                      DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
    ,PRIMARY KEY(seq)
)