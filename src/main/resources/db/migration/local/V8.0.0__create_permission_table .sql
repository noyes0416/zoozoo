CREATE TABLE IF NOT EXISTS Permission(
     permissionNo 	    BIGINT AUTO_INCREMENT
    ,type               TINYINT NOT NULL
    ,typeValue          BIGINT NOT NULL
    ,userNo			    BIGINT NOT NULL
    ,auth               CHAR(4) NOT NULL DEFAULT 'R'
    ,note               VARCHAR(200) NULL
    ,state              TINYINT NOT NULL DEFAULT 1
    ,deniedStartDt      DATETIME NULL
    ,deniedEndDt        DATETIME NULL
    ,regUserNo 	        BIGINT NOT NULL
	,regDt   			DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
    ,updUserNo 	        BIGINT NULL
	,updDt	 			DATETIME NULL
	,PRIMARY KEY(permissionNo)
);