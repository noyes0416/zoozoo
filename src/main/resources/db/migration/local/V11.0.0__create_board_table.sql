CREATE TABLE IF NOT EXISTS Board (
	 boardNo	    BIGINT   AUTO_INCREMENT
	,selectionNo    BIGINT   NOT NULL
	,userNo         BIGINT   NOT NULL
	,viewCount      INT      NOT NULL DEFAULT 0
	,displayYN      CHAR(1)  NOT NULL DEFAULT 'Y'
	,regDt          DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	,updDt	 		DATETIME NULL
	,PRIMARY KEY(boardNo)
);

INSERT INTO Board(selectionNo, userNo) VALUES(121, 1);
INSERT INTO Board(selectionNo, userNo) VALUES(121, 1);

CREATE TABLE IF NOT EXISTS BoardReply (
     replyNo       BIGINT   AUTO_INCREMENT
    ,originReplyNo BIGINT   NOT NULL DEFAULT 0
    ,boardNo       BIGINT   NOT NULL
    ,userNo        BIGINT   NOT NULL
    ,displayYN     CHAR(1)  NOT NULL DEFAULT 'Y'
    ,regDt         DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	,updDt         DATETIME NULL
	,PRIMARY KEY(replyNo)
);

INSERT INTO BoardReply(boardNo, userNo) VALUES(1, 1);
INSERT INTO BoardReply(boardNo, originReplyNo, userNo) VALUES(1, 1, 1);
INSERT INTO BoardReply(boardNo, userNo) VALUES(1, 2);
INSERT INTO BoardReply(boardNo, userNo) VALUES(2, 1);