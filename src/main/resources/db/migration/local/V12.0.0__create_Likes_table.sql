CREATE TABLE IF NOT EXISTS Likes (
	 likesNo	    BIGINT   AUTO_INCREMENT
	,userNo         BIGINT   NOT NULL
	,type 			TINYINT  NOT NULL
	,typeValue		BIGINT   NOT NULL
	,regDt          DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	,PRIMARY KEY(likesNo)
);

INSERT INTO Likes(userNo, type, typeValue) VALUES(1, 2, 1);