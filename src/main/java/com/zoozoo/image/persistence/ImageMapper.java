package com.zoozoo.image.persistence;

import com.zoozoo.image.domain.Image;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImageMapper
{
    Image selectImage(int type, int typeValue);
    void removeImageMst(int type, int typeValue);
    void removeImageDtl(int mstImageNo);
    int selectMstImageNo(int type, int typeValue);
    int isExistsImageMst(int type, int typeValue);
}
