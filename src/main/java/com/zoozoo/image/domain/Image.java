package com.zoozoo.image.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@Component
@NoArgsConstructor
public class Image {
    private int imageNo;
    private int type;
    private int typeValue;
    private String fileName;
    private String filePath;
    private int creator;
    private int lastUpdUser;
}
