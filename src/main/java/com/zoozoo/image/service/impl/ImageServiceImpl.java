package com.zoozoo.image.service.impl;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.image.domain.Image;
import com.zoozoo.image.persistence.ImageMapper;
import com.zoozoo.image.service.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {
    private final ImageMapper imageMapper;
    private String methodName;

    public Image selectImage(int type, int typeValue)
    {
        if(imageMapper.isExistsImageMst(type, typeValue) > 0 )
        {
            return Optional.ofNullable(imageMapper.selectImage(type, typeValue)).orElseGet(()->{return new Image();});
        }
        else
        {
            return new Image();
        }
    }

    public Result removeImage(int type, int typeValue)
    {
        methodName = "[removeImage]";
        try
        {
            if(imageMapper.isExistsImageMst(type, typeValue) !=0 )
            {
                int mstImageNo = Optional.ofNullable(imageMapper.selectMstImageNo(type, typeValue)).orElseGet(()->{return 0;});

                imageMapper.removeImageDtl(mstImageNo);
                imageMapper.removeImageMst(type, typeValue);

            }
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }
}
