package com.zoozoo.image.service;

import com.zoozoo.common.domain.Result;
import com.zoozoo.image.domain.Image;
import org.springframework.stereotype.Service;

@Service
public interface ImageService {
    Image selectImage(int type, int typeValue);
    Result removeImage(int type, int typeValue);
}
