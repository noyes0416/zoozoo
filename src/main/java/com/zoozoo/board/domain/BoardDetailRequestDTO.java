package com.zoozoo.board.domain;

import com.zoozoo.common.enumeration.TextType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class BoardDetailRequestDTO {
    private final int userNo;
    private final int boardNo;
    @ApiModelProperty(hidden = true)
    private final int boardType;

    @Builder
    public BoardDetailRequestDTO(final int userNo, final int boardNo) {
        this.userNo = userNo;
        this.boardNo = boardNo;
        this.boardType = TextType.BOARD.getCode();
    }
}
