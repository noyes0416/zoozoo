package com.zoozoo.board.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class BoardListResponseDTO {
    private int boardNo;
    private int selectionNo;
    private String description;
    private int userNo;
    private String userName;
    private String title;
    private String content;
    private int viewCount;
    private String sessionUserLikeYN;
    private int likeCount;
    private int replyCount;
    private String regDt;
}
