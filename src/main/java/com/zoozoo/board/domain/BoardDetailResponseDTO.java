package com.zoozoo.board.domain;

import com.zoozoo.boardReply.domain.ReplyListResponseDTO;
import lombok.Getter;

import java.util.List;

@Getter
public class BoardDetailResponseDTO {
    private int boardNo;
    private int selectionNo;
    private String description;
    private int userNo;
    private String userName;
    private String title;
    private String content;
    private String sessionUserLikeYN;
    private int viewCount;
    private int likeCount;
    private String shareURL = "www.temporaryURL.com";
    private String regDt;
    private int replyTotalCount;
    private List<ReplyListResponseDTO> replyList;

    public BoardDetailResponseDTO setReplyTotalCount(int replyTotalCount) {
        this.replyTotalCount = replyTotalCount;
        return this;
    }

    public BoardDetailResponseDTO setReplyList(List<ReplyListResponseDTO> replyList) {
        this.replyList = replyList;
        return this;
    }
}
