package com.zoozoo.board.domain;

import com.zoozoo.common.enumeration.TextType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class UpdateBoardRequestDTO {
    private final int boardNo;
    @ApiModelProperty(hidden = true)
    private int userNo;
    private final int selectionNo;
    private final String title;
    private final String content;
    @ApiModelProperty(hidden = true)
    private final int boardType;
    private final String displayYN;

    @Builder
    public UpdateBoardRequestDTO(final int boardNo, final int selectionNo, final String title, final String content, final String displayYN) {
        this.boardNo = boardNo;
        this.selectionNo = selectionNo;
        this.title = title;
        this.content = content;
        this.boardType = TextType.BOARD.getCode();
        this.displayYN = displayYN;
    }

    public UpdateBoardRequestDTO setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
