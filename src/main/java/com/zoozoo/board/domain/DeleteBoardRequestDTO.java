package com.zoozoo.board.domain;

import com.zoozoo.common.domain.CommonMessage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class DeleteBoardRequestDTO {
    private final int boardNo;
    private final int userNo;
    @ApiModelProperty(hidden = true)
    private final String displayYN;

    @Builder
    public DeleteBoardRequestDTO(final int boardNo, final int userNo) {
        this.boardNo = boardNo;
        this.userNo = userNo;
        this.displayYN = CommonMessage.COMMON_N;
    }
}
