package com.zoozoo.board.domain;

import com.zoozoo.common.enumeration.TextType;
import com.zoozoo.common.util.CommonUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class BoardListRequestDTO {
    @ApiModelProperty(hidden = true)
    private int userNo;
    private final int pageNo;
    private final int pageSize;
    private final int selectionType;
    @ApiModelProperty(hidden = true)
    private List<Integer> selectionNosList;
    @ApiModelProperty(hidden = true)
    private final int boardType;

    @Builder
    public BoardListRequestDTO(final int pageNo, final int pageSize, final int selectionType, final String selectionNos) {
        this.pageNo = this.setPageNo(pageNo, pageSize);
        this.pageSize = pageSize;
        this.selectionType = selectionType;
        if (!CommonUtil.isNullOrEmpty(selectionNos)) {
            this.selectionNosList = setSelectionNosList(selectionNos);
        }
        this.boardType = TextType.BOARD.getCode();
    }

    public BoardListRequestDTO setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }

    private int setPageNo(final int pageNo, final int pageSize) {
        return pageNo * pageSize;
    }

    private List<Integer> setSelectionNosList(final String selectionNos) {
        return Arrays.stream(selectionNos.split(","))
                .filter(selectionNo -> !selectionNo.equals("0"))
                .map(Integer::valueOf)
                .collect(Collectors.toList());
    }
}
