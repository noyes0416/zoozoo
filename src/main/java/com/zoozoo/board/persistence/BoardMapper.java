package com.zoozoo.board.persistence;

import com.zoozoo.board.domain.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BoardMapper {

    int selectBoardCount(int boardNo);

    List<BoardListResponseDTO> selectBoardList(BoardListRequestDTO requestDTO);

    BoardDetailResponseDTO selectBoardDetail(BoardDetailRequestDTO requestDTO);

    int insertBoard(InsertBoardRequestDTO requestDTO);

    void insertText(InsertBoardRequestDTO requestDTO);

    void updateBoard(UpdateBoardRequestDTO requestDTO);

    void updateText(UpdateBoardRequestDTO requestDTO);

    int deleteBoardDisplayN(DeleteBoardRequestDTO requestDTO);

    int updateBoardDisplayN(int boardNo);

    void updateBoardViewCount(int boardNo);

    int selectBoardWriterNo(int boardNo);
}
