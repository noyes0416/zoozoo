package com.zoozoo.board.service;

import com.zoozoo.board.domain.*;
import com.zoozoo.common.domain.Result;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BoardService {

    List<BoardListResponseDTO> selectBoardList(BoardListRequestDTO requestDTO);

    BoardDetailResponseDTO selectBoardDetail(BoardDetailRequestDTO requestDTO);

    Result insertBoard(InsertBoardRequestDTO requestDTO);

    Result updateBoard(UpdateBoardRequestDTO requestDTO);

    Result deleteBoard(DeleteBoardRequestDTO requestDTO);
}
