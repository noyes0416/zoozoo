package com.zoozoo.board.service;

import com.zoozoo.board.domain.*;
import com.zoozoo.board.persistence.BoardMapper;
import com.zoozoo.boardReply.domain.ReplyListRequestDTO;
import com.zoozoo.boardReply.service.BoardReplyService;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.zoozoo.common.domain.CommonMessage.FAIL_MESSAGE;
import static com.zoozoo.common.domain.CommonMessage.OK_MESSAGE;

@RequiredArgsConstructor
@Service
public class BoardServiceImpl implements BoardService {

    private final BoardMapper boardMapper;
    private final BoardReplyService boardReplyService;

    public List<BoardListResponseDTO> selectBoardList(BoardListRequestDTO request) {
        return boardMapper.selectBoardList(request);
    }

    public BoardDetailResponseDTO selectBoardDetail(BoardDetailRequestDTO request) {
        int boardNo = request.getBoardNo();
        int userNo = request.getUserNo();

        if (this.isBoardExists(boardNo)) {
            ReplyListRequestDTO originReplyRequest = ReplyListRequestDTO.builder()
                    .userNo(userNo)
                    .boardNo(boardNo)
                    .build();

            updateBoardViewCount(boardNo);

            return boardMapper.selectBoardDetail(request)
                    .setReplyTotalCount(boardReplyService.selectReplyTotalCount(boardNo))
                    .setReplyList(boardReplyService.selectAllDepthReplyList(originReplyRequest));
        }
        else {
            throw new RuntimeException(CommonMessage.BOARD_NOT_EXISTS);
        }
    }

    private boolean isBoardExists(int boardNo) {
        int boardCount = boardMapper.selectBoardCount(boardNo);

        return (boardCount == 1);
    }

    @Transactional(rollbackFor = Exception.class)
    public Result insertBoard(InsertBoardRequestDTO requestDTO) {
        boardMapper.insertBoard(requestDTO);
        int boardNo = requestDTO.getBoardNo();
        boardMapper.insertText(requestDTO.setBoardNo(boardNo));

        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }

    @Transactional(rollbackFor = Exception.class)
    public Result updateBoard(UpdateBoardRequestDTO requestDTO) {
        boardMapper.updateBoard(requestDTO);
        boardMapper.updateText(requestDTO);

        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }

    public Result deleteBoard(DeleteBoardRequestDTO requestDTO) {
        int executedRowCount = boardMapper.deleteBoardDisplayN(requestDTO);

        return (executedRowCount == 1) ?
                Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build() :
                Result.builder().code(HttpStatus.INTERNAL_SERVER_ERROR.value()).message(FAIL_MESSAGE).build();

    }

    private void updateBoardViewCount(int boardNo) {
        boardMapper.updateBoardViewCount(boardNo);
    }
}
