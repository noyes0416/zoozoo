package com.zoozoo.board.controller;

import com.zoozoo.board.domain.*;
import com.zoozoo.board.service.BoardService;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping(value = "/api/v1/board")
@RequiredArgsConstructor
@RestController
@Api(description = "커뮤니티 게시글")
public class BoardController {

    private final BoardService boardService;

    @ApiOperation(value = "커뮤니티 게시글 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "페이지 번호 (최초 : 0, 이후 1씩 증가)", required = true),
            @ApiImplicitParam(name = "pageSize", value = "페이징 사이즈 (default : 20)", required = true),
            @ApiImplicitParam(name = "selectionType", value = "주종/지역 카테고리 타입 (주종:1, 지역:8)", required = true),
            @ApiImplicitParam(name = "selectionNos", value = "주종/지역 카테고리 번호들 (필터 N개인 경우 구분자 ',' 사용. (ex: 111,122))")
    })
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<BoardListResponseDTO>> getBoardList(HttpServletRequest request, final int pageNo, final int pageSize, final int selectionType, final String selectionNos) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        BoardListRequestDTO requestDTO = BoardListRequestDTO.builder()
                .selectionType(selectionType)
                .selectionNos(selectionNos)
                .pageNo(pageNo)
                .pageSize(pageSize).build().setUserNo(userSession.getUserNo());

        List<BoardListResponseDTO> boardList = boardService.selectBoardList(requestDTO);

        return ResponseEntity.ok().body(boardList);
    }

    @ApiOperation(value = "커뮤니티 게시글 상세조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "boardNo", value = "게시글 번호", required = true)
    })
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public ResponseEntity<BoardDetailResponseDTO> getBoardDetail(HttpServletRequest request, @RequestParam("boardNo") int boardNo) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);

        BoardDetailRequestDTO requestDTO = BoardDetailRequestDTO.builder()
                .userNo(userSession.getUserNo())
                .boardNo(boardNo)
                .build();

        BoardDetailResponseDTO boardDetail = boardService.selectBoardDetail(requestDTO);
        return ResponseEntity.ok().body(boardDetail);
    }

    @ApiOperation(value = "커뮤니티 게시글 등록")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Result> postBoard(HttpServletRequest request, @RequestBody InsertBoardRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setUserNo(userSession.getUserNo());

        Result result = boardService.insertBoard(requestDTO);

        return ResponseEntity.ok().body(result);
    }

    @ApiOperation(value = "커뮤니티 게시글 수정")
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<Result> putBoard(HttpServletRequest request, @RequestBody UpdateBoardRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setUserNo(userSession.getUserNo());

        Result result = boardService.updateBoard(requestDTO);

        return ResponseEntity.ok().body(result);
    }

    @ApiOperation(value = "커뮤니티 게시글 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "boardNo", value = "게시글 번호", required = true)
    })
    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<Result> deleteBoard(HttpServletRequest request, @RequestParam int boardNo) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        DeleteBoardRequestDTO requestDTO = DeleteBoardRequestDTO.builder()
                .boardNo(boardNo)
                .userNo(userSession.getSnsType())
                .build();

        Result result = boardService.deleteBoard(requestDTO);

        return ResponseEntity.ok().body(result);
    }
}
