package com.zoozoo.signIn.controller;

import com.zoozoo.common.annotation.NoHeaderLogging;
import com.zoozoo.common.domain.*;
import com.zoozoo.common.jwt.JWTServiceInterface;
import com.zoozoo.common.oauth.OAuthServiceInterface;
import com.zoozoo.signIn.domain.RecreateJWTToken;
import com.zoozoo.signIn.domain.SignInRequestDTO;
import com.zoozoo.signIn.domain.SignInResponseDTO;
import com.zoozoo.signIn.domain.SignOutRequestDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api")
@RequiredArgsConstructor
@RestController
@Api(description = "로그인")
public class SignInController {

    private final OAuthServiceInterface oAuthService;
    private final JWTServiceInterface jwtService;

    @NoHeaderLogging
    @ApiOperation(value = "로그인")
    @RequestMapping(value = "/signIn", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<SignInResponseDTO> signIn (@RequestBody SignInRequestDTO signInRequestDto) {
        SignInResponseDTO signInResponseDto = new SignInResponseDTO();

        OAuthRequestDTO oAuthRequest = OAuthRequestDTO.builder()
                .idToken(signInRequestDto.getOauthToken())
                .snsType(signInRequestDto.getSnsType())
                .build();

        OAuthResponseDTO oAuthResponse = oAuthService.verifyToken(oAuthRequest);
        if (oAuthResponse != null && oAuthResponse.isVerified()) {
            JWTTokenDTO jwtTokenDTO = jwtService.createToken(oAuthResponse);
            signInResponseDto.setJwtToken(jwtTokenDTO.getTokenValue());
        }

        return ResponseEntity.ok().body(signInResponseDto);
    }

    @NoHeaderLogging
    @ApiOperation(value = "JWT 토큰 재생성")
    @RequestMapping(value = "/recreateJWTToken", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<SignInResponseDTO> recreateJWTToken(@RequestBody RecreateJWTToken recreateJWTToken) {
        SignInResponseDTO signInResponseDto = new SignInResponseDTO();

        JWTTokenDTO jwtTokenDTO = jwtService.recreateToken(recreateJWTToken);
        signInResponseDto.setJwtToken(jwtTokenDTO.getTokenValue());

        return ResponseEntity.ok().body(signInResponseDto);
    }

    @NoHeaderLogging
    @ApiOperation(value = "로그아웃")
    @RequestMapping(value = "/signOut", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Result> signOut (@RequestBody SignOutRequestDTO signOutRequestDTO) {

        String jwtToken = signOutRequestDTO.getJwtToken();
        JWTTokenDTO jwtTokenDTO = jwtService.parsingToken(jwtToken);

        jwtService.releaseToken(jwtTokenDTO);

        return ResponseEntity.ok().body(Result.builder()
                .code(HttpStatus.OK.value())
                .message(CommonMessage.OK_MESSAGE)
                .build());
    }

    @NoHeaderLogging
    @ApiOperation(value = "개발테스트용 jwt Token 생성")
    @RequestMapping(value = "/createSampleJWTToken", method = RequestMethod.GET)
    public String createSampleJWTToken () {
        SignInResponseDTO signInResponseDto = new SignInResponseDTO();

        // Local, AWS 모두 userNo = 1 데이터
        RecreateJWTToken recreateJWTToken = RecreateJWTToken.builder()
                .email("gw.myung@kt.com")
                .snsType(1)
                .build();

        JWTTokenDTO jwtTokenDTO = jwtService.recreateToken(recreateJWTToken);
        signInResponseDto.setJwtToken(jwtTokenDTO.getTokenValue());

        return signInResponseDto.getJwtToken();
    }
}
