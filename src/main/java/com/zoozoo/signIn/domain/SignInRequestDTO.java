package com.zoozoo.signIn.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
public class SignInRequestDTO {
    private String oauthToken;
    private int snsType;

    @Builder
    public SignInRequestDTO(String oauthToken, int snsType) {
        this.oauthToken = oauthToken;
        this.snsType = snsType;
    }
}
