package com.zoozoo.signIn.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SignOutRequestDTO {
    private String jwtToken;
}
