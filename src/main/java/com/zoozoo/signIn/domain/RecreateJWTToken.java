package com.zoozoo.signIn.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
public class RecreateJWTToken {
    private String email;
    private int snsType;

    @Builder
    public RecreateJWTToken(String email, int snsType) {
        this.email = email;
        this.snsType = snsType;
    }
}
