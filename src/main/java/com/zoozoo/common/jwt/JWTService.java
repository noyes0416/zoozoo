package com.zoozoo.common.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.JWTTokenDTO;
import com.zoozoo.common.domain.OAuthResponseDTO;
import com.zoozoo.common.enumeration.InterceptorExceptionType;
import com.zoozoo.common.exception.InterceptorException;
import com.zoozoo.common.util.CommonUtil;
import com.zoozoo.signIn.domain.RecreateJWTToken;
import com.zoozoo.user.domain.UserSessionDTO;
import com.zoozoo.user.service.UserService;
import io.jsonwebtoken.SignatureException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@Service
public class JWTService implements JWTServiceInterface {

    @Value("${jwt.secret.key}")
    private String SECRET_KEY;

    @Value("${jwt.issuer.key}")
    private String ISSUER;

    private static Long EXPIRED_TIME = 1000 * 60L * 60L * 1L;

    private final UserService userService;

    @Transactional(rollbackFor = Exception.class)
    public JWTTokenDTO recreateToken(RecreateJWTToken recreateJWTToken) {
        OAuthResponseDTO oAuthResponseDTO = OAuthResponseDTO.builder()
                .email(recreateJWTToken.getEmail())
                .snsType(recreateJWTToken.getSnsType())
                .build();

        return this.createToken(oAuthResponseDTO);
    }

    @Transactional(rollbackFor = Exception.class)
    public JWTTokenDTO createToken(OAuthResponseDTO oAuthDTO) {
        JWTTokenDTO jwtTokenDTO = null;
        Date now = new Date();
        String tokenKey = String.format("%s%s", oAuthDTO.getEmail(), now.getTime());

        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
            String token = JWT.create()
                    .withIssuer(ISSUER)
                    .withSubject(oAuthDTO.getEmail())
                    .withClaim("snsType", oAuthDTO.getSnsType())
                    .withClaim("tokenKey", tokenKey)
                    .withExpiresAt(new Date(now.getTime() + EXPIRED_TIME))
                    .sign(algorithm);

            jwtTokenDTO = JWTTokenDTO.builder()
                    .email(oAuthDTO.getEmail())
                    .snsType(oAuthDTO.getSnsType())
                    .tokenKey(tokenKey)
                    .tokenValue(token)
                    .build();

            this.doDBProcess(jwtTokenDTO);
        }
        catch (JWTCreationException ex){
            throw new InterceptorException(InterceptorExceptionType.INTERNERERROR);
        }

        return jwtTokenDTO;
    }

    private String createUserName() {
        String userName = CommonUtil.createRandomString();

        if (this.checkIsExistsUserName(userName)) {
            userName = CommonUtil.createExpandRandomString(userName);
        }

        return userName;
    }

    private boolean checkIsExistsUserName(String userName) {
        return (userService.selectUserCountByUserName(userName) == 1) ? true : false;
    }

    private void doDBProcess(JWTTokenDTO jwtTokenDTO) {
        if (this.checkIsExistsUser(jwtTokenDTO)) {
            this.updateUserToken(jwtTokenDTO);
        }
        else {
            jwtTokenDTO.setUserName(this.createUserName());
            this.insertUserToken(jwtTokenDTO);
        }
    }

    public void releaseToken(JWTTokenDTO jwtTokenDTO) {
        if (this.checkIsExistsUser(jwtTokenDTO)) {
            this.deleteUserToken(jwtTokenDTO);
        }
        else {
            throw new RuntimeException(CommonMessage.INVALID_EMAIL);
        }
    }

    private boolean checkIsExistsUser(JWTTokenDTO jwtTokenDTO) {

        int count = userService.selectUserCountByEmail(jwtTokenDTO);

        return (count == 1) ? true : false;
    }

    private void insertUserToken(JWTTokenDTO jwtTokenDTO) {
        userService.insertUser(jwtTokenDTO);
    }

    private void updateUserToken(JWTTokenDTO jwtTokenDTO) {
        userService.updateUserJWTToken(jwtTokenDTO);
    }

    private void deleteUserToken(JWTTokenDTO jwtTokenDTO) {
        userService.deleteUserJWTToken(jwtTokenDTO);
    }

    public void exchangeTokenToUserSession(HttpServletRequest request, JWTTokenDTO jwtTokenDTO) {
        UserSessionDTO userSessionDTO = null;

        if (this.checkIsExistsUser(jwtTokenDTO)) {
            userSessionDTO = userService.selectUserInfoByTokenKey(jwtTokenDTO);
        }

        request.setAttribute(CommonMessage.USER_SESSION, userSessionDTO);
    }

    public boolean verifyToken(String token) {
        try {
            if (token == null || token.isEmpty()) {
                return false;
            }

            JWTTokenDTO jwtTokenDTO = this.parsingToken(token);

            if (jwtTokenDTO == null) {
                return false;
            }

            return true;

        } catch (TokenExpiredException ex) {
            log.error("Expired JWT Token: {}", ex.getMessage());
            throw new InterceptorException(InterceptorExceptionType.EXPIREDTOKEN);
        }
        catch (SignatureException ex) {
            log.error("JWT Signature Invalid: {}", ex.getMessage());
            throw new InterceptorException(InterceptorExceptionType.UNAUTHORIZED);
        }
        catch (JWTVerificationException ex){
            log.error("JWT Verified failed: {}", ex.getMessage());
            throw new InterceptorException(InterceptorExceptionType.UNAUTHORIZED);
        }
        catch (Exception ex) {
            log.error("JWT Exception Occured: {}", ex.getMessage());
            throw new InterceptorException(InterceptorExceptionType.INTERNERERROR);
        }
    }

    public JWTTokenDTO parsingToken(String token) {
        Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer(ISSUER)
                .build();

        DecodedJWT decodedJWT = verifier.verify(token);
        Map<String, Claim> Claims = decodedJWT.getClaims();

        return JWTTokenDTO.builder()
                .email(Claims.get("sub").asString())
                .snsType(Claims.get("snsType").asInt())
                .tokenKey(Claims.get("tokenKey").asString())
                .build();
    }
}
