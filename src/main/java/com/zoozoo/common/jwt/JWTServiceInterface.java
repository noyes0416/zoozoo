package com.zoozoo.common.jwt;

import com.zoozoo.common.domain.JWTTokenDTO;
import com.zoozoo.common.domain.OAuthResponseDTO;
import com.zoozoo.signIn.domain.RecreateJWTToken;

import javax.servlet.http.HttpServletRequest;

public interface JWTServiceInterface {
    JWTTokenDTO recreateToken(RecreateJWTToken recreateJWTToken);

    JWTTokenDTO createToken(OAuthResponseDTO oAuthDTO);

    void releaseToken(JWTTokenDTO jwtTokenDTO);

    void exchangeTokenToUserSession(HttpServletRequest request, JWTTokenDTO jwtTokenDTO);

    boolean verifyToken(String token);

    JWTTokenDTO parsingToken(String token);
}
