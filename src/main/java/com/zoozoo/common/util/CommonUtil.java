package com.zoozoo.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Slf4j
@Component
public class CommonUtil {

    public String stringParser(String syntax, String separator, int index) {
        String[] parser = syntax.split(separator);
        return parser[index];
    }

    public static String stacktraceToString(Exception ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    public static String createRandomString() {
        int startChar = 48; // numeral 0
        int endChar = 122;  // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String randomString = random.ints(startChar, endChar + 1)
                .filter(i -> (i <= 57 || i>= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return randomString;
    }

    public static String createExpandRandomString(String basicRandomString) {
        StringBuilder sb = new StringBuilder();
        sb.append(basicRandomString);

        String now = Long.toString(System.nanoTime());
        int length = now.length();
        sb.append(now, length - 4, length);

        return sb.toString();
    }

    public static boolean isNullOrEmpty(String word) {
        return word == null || word.trim().length() == 0;
    }
}
