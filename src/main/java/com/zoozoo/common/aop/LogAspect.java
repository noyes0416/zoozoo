package com.zoozoo.common.aop;

import com.zoozoo.common.domain.JWTTokenDTO;
import com.zoozoo.common.jwt.JWTServiceInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class LogAspect {

    private final JWTServiceInterface jwtService;

    @Pointcut("execution(* com.zoozoo.*.controller.*.*(..))"
                + "&& !@annotation(com.zoozoo.common.annotation.NoHeaderLogging)")
    public void allControllerExceptSignIn() { }

    @Around(value = "allControllerExceptSignIn()")
    public Object doLogging(ProceedingJoinPoint joinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String header = request.getHeader("Bearer");
        long start = System.currentTimeMillis();
        JWTTokenDTO jwtTokenDTO = jwtService.parsingToken(header);

        try {
            return joinPoint.proceed(joinPoint.getArgs());
        }
        finally {
            long end = System.currentTimeMillis();

            log.info("[REQUEST] email:{} snsType:{} method:{} URL:{} Param:{} Body:{} ({}ms)",
                    jwtTokenDTO.getEmail(),
                    jwtTokenDTO.getSnsType(),
                    request.getMethod(),
                    request.getRequestURL(),
                    paramMapToString(request.getParameterMap()),
                    IOUtils.toString(request.getReader()),
                    end - start);
        }
    }

    @AfterReturning(pointcut = "allControllerExceptSignIn()", returning = "result")
    public void requestLogging(JoinPoint joinPoint, Object result) {
        log.info("[RESPONSE] result:{}", result);
    }

    @AfterThrowing(pointcut = "allControllerExceptSignIn()", throwing = "ex")
    public void afterThrowing(JoinPoint joinPoint, Exception ex) {
        log.error(String.format("[RESPONSE] Exception:{} ", ex));
    }

    private String paramMapToString(Map<String, String[]> paramMap) {
        return paramMap.entrySet().stream()
                .map(entry -> String.format("%s : %s", entry.getKey(), Arrays.toString(entry.getValue())))
                .collect(Collectors.joining(", "));
    }
}
