package com.zoozoo.common.exception;

import com.zoozoo.common.enumeration.InterceptorExceptionType;
import lombok.Getter;

@Getter
public class InterceptorException extends RuntimeException {
    private InterceptorExceptionType exceptionType;

    public InterceptorException(InterceptorExceptionType exceptionType) {
        super(exceptionType.getMessage());
        this.exceptionType = exceptionType;
    }
}
