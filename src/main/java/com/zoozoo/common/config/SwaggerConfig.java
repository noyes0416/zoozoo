package com.zoozoo.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .apiInfo(this.apiInfo())
                .securityContexts(Arrays.asList(securityContext()))
                .securitySchemes(Arrays.asList(apiKey()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("ZooZoo BackEnd API")
                .description(this.apiInfoDescription())
                .version("1.0")
                .build();
    }

    private String apiInfoDescription() {
        StringBuilder sb = new StringBuilder();

        sb.append("ZooZoo BackEnd API By Team Noyes\n");

        sb.append("\n[2022-08-07 이후 변경 사항]");
        sb.append("\n1.(User) 캘린더 멤버 닉네임으로 유저 조회 API 추가");
        sb.append("\n2.(Message) 메세지 API 추가");
        sb.append("\n3.(Timeline) 타임라인 조회 API 추가");
        sb.append("\n4.(Board) 게시글 상세보기시 조회수 + 1 로직 추가");
        sb.append("\n5.(Likes) 좋아요 취소 API 요청파라미터 수정");
        sb.append("\n \n");

        return sb.toString();
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", "Bearer", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("JWT", authorizationScopes));
    }
}