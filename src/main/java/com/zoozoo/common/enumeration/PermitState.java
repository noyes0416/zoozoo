package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum PermitState implements CodeEnum {
    PERMIT(1),
    TEMPORARY_DENIED(2),
    PERMANENT_DENIED(3);

    private final int stateCode;

    PermitState(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(PermitState.class)
    public static class TypeHandler extends CodeEnumTypeHandler<PermitState> {
        public TypeHandler() {
            super(PermitState.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
