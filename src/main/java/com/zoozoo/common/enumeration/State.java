package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum State implements CodeEnum {
    Normal(1),
    AbNormal(2);

    private final int stateCode;

    State(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(State.class)
    public static class TypeHandler extends CodeEnumTypeHandler<State> {
        public TypeHandler() {
            super(State.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
