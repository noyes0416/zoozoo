package com.zoozoo.common.enumeration;

import com.zoozoo.common.domain.CommonMessage;
import org.apache.ibatis.type.MappedTypes;

public enum PermissionStateType implements CodeEnum{
    PERMIT(1, "Y"),
    TEMPORARY_DENIED(2, "N"),
    PERMANENT_DENIED(3, "N");

    private final int stateCode;
    private final String permitFlag;

    PermissionStateType(int stateCode, String permitFlag) {
        this.stateCode = stateCode;
        this.permitFlag = permitFlag;
    }

    @MappedTypes(PermissionStateType.class)
    public static class TypeHandler extends CodeEnumTypeHandler<PermissionStateType> {
        public TypeHandler() {
            super(PermissionStateType.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }

    public static String getPermitFlagByState(final int state) {
        for(PermissionStateType stateType : PermissionStateType.values()) {
            if (stateType.stateCode == state)
                return stateType.permitFlag;
        }

        return CommonMessage.COMMON_N;
    }
}
