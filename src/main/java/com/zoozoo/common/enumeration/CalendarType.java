package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum CalendarType implements CodeEnum {
    PRIVATE(1),
    PUBLIC(2);

    private final int stateCode;

    CalendarType(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(CalendarType.class)
    public static class TypeHandler extends CodeEnumTypeHandler<CalendarType> {
        public TypeHandler() {
            super(CalendarType.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
