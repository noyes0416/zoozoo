package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum TextType implements CodeEnum {
    CALENDAR(1),
    BOARD(2),
    REPLY(3),
    MESSAGE(4),
    TIMELINE(5);

    private final int stateCode;

    TextType(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(TextType.class)
    public static class TypeHandler extends CodeEnumTypeHandler<TextType> {
        public TypeHandler() {
            super(TextType.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
