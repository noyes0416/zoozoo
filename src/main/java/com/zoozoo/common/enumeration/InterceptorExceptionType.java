package com.zoozoo.common.enumeration;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum InterceptorExceptionType {
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED, 4000, "Request UnAuthorized."),
    EXPIREDTOKEN(HttpStatus.BAD_REQUEST, 4001, "Expired Token."),
    INTERNERERROR(HttpStatus.INTERNAL_SERVER_ERROR, 4002, "Server Error.");

    private final HttpStatus status;
    private final int code;
    private String message;

    InterceptorExceptionType(HttpStatus status, int code) {
        this.status = status;
        this.code = code;
    }

    InterceptorExceptionType(HttpStatus status, int code, String message) {
        this.status = status;
        this.code = code;
        this.message = message;
    }
}
