package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum ReportStateType implements CodeEnum {
    BEFORE_WORK(1),
    AFTER_WORK(2);

    private final int stateCode;

    ReportStateType(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(ReportStateType.class)
    public static class TypeHandler extends CodeEnumTypeHandler<ReportStateType> {
        public TypeHandler() {
            super(ReportStateType.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
