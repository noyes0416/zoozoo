package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum ImageType implements CodeEnum{
    CALENDAR(1),
    COMMUNITY(2);

    private final int stateCode;

    ImageType(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(ImageType.class)
    public static class TypeHandler extends CodeEnumTypeHandler<ImageType> {
        public TypeHandler() {
            super(ImageType.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
