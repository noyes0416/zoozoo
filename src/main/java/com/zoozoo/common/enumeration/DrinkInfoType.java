package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum DrinkInfoType implements CodeEnum {
    PRIVATE(1),
    PUBLIC(2);

    private final int stateCode;

    DrinkInfoType(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(DrinkInfoType.class)
    public static class TypeHandler extends CodeEnumTypeHandler<DrinkInfoType> {
        public TypeHandler() {
            super(DrinkInfoType.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
