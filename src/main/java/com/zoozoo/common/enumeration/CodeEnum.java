package com.zoozoo.common.enumeration;

public interface CodeEnum {
    int getCode();
}
