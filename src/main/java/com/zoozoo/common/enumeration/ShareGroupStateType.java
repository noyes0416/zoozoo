package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum ShareGroupStateType implements CodeEnum {
    Normal(1),
    Deleted(2),
    Invited(3),
    Rejected(4);

    private final int stateCode;

    ShareGroupStateType(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(ShareGroupStateType.class)
    public static class TypeHandler extends CodeEnumTypeHandler<ShareGroupStateType> {
        public TypeHandler() {
            super(ShareGroupStateType.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
