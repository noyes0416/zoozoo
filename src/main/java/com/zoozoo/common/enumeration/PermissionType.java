package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum PermissionType implements CodeEnum{
    CALENDAR(1),
    COMMUNITY(2);

    private final int stateCode;

    PermissionType(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(PermissionType.class)
    public static class TypeHandler extends CodeEnumTypeHandler<PermissionType> {
        public TypeHandler() {
            super(PermissionType.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
