package com.zoozoo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum TimelineType implements CodeEnum {
    ADD_MEMBER(1, "신규 멤버", "%s 님이 %s 에 가입하셨습니다."),
    DEL_MEMBER(2, "멤버 탈퇴", "%s 님이 %s 에서 나가셨습니다."),
    NEW_RECORD(4, "신규 일정", "%s 에 새로운 일정이 등록 되었습니다."),
    WEEKLY_DRINK_BEST(5, "주간 주류 베스트", "%s"),
    WEEKLY_REGION_BEST(6, "주간 지역 베스트","%s"),
    MONTHLY_DRINK_BEST(7, "월간 주류 베스트","%s"),
    MONTHLY_REGION_BEST(8, "월간 지역 베스트","%s"),
    REPLY_ALARM(9, "댓글 알림","회원님의 게시물에 새로운 댓글이 등록 되었습니다."),
    LIKES_ALARM(10, "좋아요 알림","회원님의 게시물을 %s명이 좋아합니다!");

    private final int contentType;
    private final String contentTypeName;
    private final String contentFormat;

    TimelineType(int contentType, String contentTypeName, String contentFormat) {
        this.contentType = contentType;
        this.contentTypeName = contentTypeName;
        this.contentFormat = contentFormat;
    }

    @MappedTypes(TimelineType.class)
    public static class TypeHandler extends CodeEnumTypeHandler<TimelineType> {
        public TypeHandler() {
            super(TimelineType.class);
        }
    }

    @Override
    public int getCode() {
        return this.contentType;
    }

    public String getContentTypeName() {
        return this.contentTypeName;
    }

    public String getContentFormat() {
        return this.contentFormat;
    }
}
