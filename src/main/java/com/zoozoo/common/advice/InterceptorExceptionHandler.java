package com.zoozoo.common.advice;

import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.InterceptorExceptionType;
import com.zoozoo.common.exception.InterceptorException;
import com.zoozoo.common.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;

@Slf4j
@RestControllerAdvice
public class InterceptorExceptionHandler {

    @ExceptionHandler({InterceptorException.class})
    public ResponseEntity<Result> exceptionHandler(HttpServletRequest request, final InterceptorException e) {
        log.error(CommonUtil.stacktraceToString(e));
        e.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(Result.builder()
                        .code(InterceptorExceptionType.EXPIREDTOKEN.getCode())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<Result> exceptionHandler(HttpServletRequest request, final RuntimeException e) {
        log.error(CommonUtil.stacktraceToString(e));
        e.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Result.builder()
                        .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<Result> exceptionHandler(HttpServletRequest request, final AccessDeniedException e) {
        log.error(CommonUtil.stacktraceToString(e));
        e.printStackTrace();
        return ResponseEntity
                .status(InterceptorExceptionType.UNAUTHORIZED.getStatus())
                .body(Result.builder()
                        .code(InterceptorExceptionType.UNAUTHORIZED.getCode())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Result> exceptionHandler(HttpServletRequest request, final Exception e) {
        e.printStackTrace();
        return ResponseEntity
                .status(InterceptorExceptionType.INTERNERERROR.getStatus())
                .body(Result.builder()
                        .code(InterceptorExceptionType.INTERNERERROR.getCode())
                        .message(e.getMessage())
                        .build());
    }

}
