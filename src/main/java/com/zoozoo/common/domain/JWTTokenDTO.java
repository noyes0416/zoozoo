package com.zoozoo.common.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class JWTTokenDTO {
    private String email;
    private int snsType;
    private String userName;
    private String tokenKey;
    private String tokenValue;
}
