package com.zoozoo.common.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@ApiModel(value = "API 호출 응답 모델")
public class Result {

    @ApiModelProperty(example = "200")
    private int code;

    @ApiModelProperty(example = "OK")
    private String message;

    @Builder
    public Result (int code, String message) {
        this.code = code;
        this.message = message;
    }
}
