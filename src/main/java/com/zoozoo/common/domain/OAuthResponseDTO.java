package com.zoozoo.common.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class OAuthResponseDTO {
    private String idToken;
    private String email;
    private int snsType;
    private boolean isVerified = false;

    @Builder
    public OAuthResponseDTO(String idToken, String email, int snsType, boolean isVerified) {
        this.idToken = idToken;
        this.email = email;
        this.snsType = snsType;
        this.isVerified = isVerified;
    }
}
