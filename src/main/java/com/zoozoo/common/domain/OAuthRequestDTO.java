package com.zoozoo.common.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
public class OAuthRequestDTO {
    private String idToken;
    private int snsType;

    @Builder
    public OAuthRequestDTO(String idToken, int snsType) {
        this.idToken = idToken;
        this.snsType = snsType;
    }
}
