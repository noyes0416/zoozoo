package com.zoozoo.common.domain;

public class CommonMessage {
    public static int ADMIN_NO = 1;

    public static String USER_SESSION = "UserSession";
    public static String INVALID_EMAIL = "Invalid User Email";
    public static String OK_MESSAGE = "OK";
    public static String FAIL_MESSAGE = "Process Failed";

    public static String COMMON_Y = "Y";
    public static String COMMON_N = "N";
    public static String BOARD_NOT_EXISTS = "Board is not exists";

    public static int BLOCK_TEXT_BASE_REPORT_COUNT = 3;
    public static String REPORTER_IS_SAME_WITH_WRITER = "Reporter is same with writer";
    public static int TEMPORARY_DENIED_BASE_REPORT_COUNT = 3;
    public static int TEMPORARY_DENIED_DAY = 7;
}
