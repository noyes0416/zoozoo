package com.zoozoo.common.oauth;

import com.zoozoo.common.domain.OAuthRequestDTO;
import com.zoozoo.common.domain.OAuthResponseDTO;

public interface OAuthServiceInterface {
    OAuthResponseDTO verifyToken(OAuthRequestDTO oAuthRequestDTO);
}
