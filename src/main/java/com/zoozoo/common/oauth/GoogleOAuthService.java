package com.zoozoo.common.oauth;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.zoozoo.common.domain.OAuthRequestDTO;
import com.zoozoo.common.domain.OAuthResponseDTO;
import com.zoozoo.common.enumeration.InterceptorExceptionType;
import com.zoozoo.common.exception.InterceptorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Slf4j
@Service
public class GoogleOAuthService implements OAuthServiceInterface {

    @Value("${oauth.google.client.id}")
    private String CLIENT_ID;

    @Override
    public OAuthResponseDTO verifyToken(OAuthRequestDTO oAuthRequestDTO) {
        OAuthResponseDTO oAuthResponseDTO = null;

        NetHttpTransport transport = new NetHttpTransport();
        JsonFactory jsonFactory = new GsonFactory();

        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
            .setAudience(Collections.singletonList(CLIENT_ID))
            .build();

        try {
            GoogleIdToken googleIdToken = verifier.verify(oAuthRequestDTO.getIdToken());

            if (googleIdToken != null) {
                GoogleIdToken.Payload payload = googleIdToken.getPayload();

                if (Boolean.valueOf(payload.getEmailVerified())) {
                    oAuthResponseDTO = OAuthResponseDTO.builder()
                            .idToken(oAuthRequestDTO.getIdToken())
                            .email(payload.getEmail())
                            .snsType(oAuthRequestDTO.getSnsType())
                            .isVerified(true)
                            .build();
                }
            }
            else {
                throw new InterceptorException(InterceptorExceptionType.INTERNERERROR);
            }
        }
        catch (GeneralSecurityException ex) {
            log.error("GoogleOAuth SecurityException: {}", ex.getMessage());
            throw new InterceptorException(InterceptorExceptionType.UNAUTHORIZED);
        }
        catch (IOException ex) {
            log.error("GoogleOAuth IOException: {}", ex.getMessage());
            throw new InterceptorException(InterceptorExceptionType.INTERNERERROR);
        }
        catch (RuntimeException ex) {
            log.error("GoogleOAuth failed verified: {}", ex.getMessage());
            throw new InterceptorException(InterceptorExceptionType.EXPIREDTOKEN);
        }
        catch (Exception ex) {
            log.error("GoogleOAuth Exception: {}", ex.getMessage());
            throw new InterceptorException(InterceptorExceptionType.INTERNERERROR);
        }

        return oAuthResponseDTO;
    }
}
