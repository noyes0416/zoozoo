package com.zoozoo.common.interceptor;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.JWTTokenDTO;
import com.zoozoo.common.enumeration.InterceptorExceptionType;
import com.zoozoo.common.exception.InterceptorException;
import com.zoozoo.common.jwt.JWTServiceInterface;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@Component
public class JWTInterceptor extends HandlerInterceptorAdapter {

    private final JWTServiceInterface jwtService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        JWTTokenDTO jwtTokenDTO;

        //1. Request Header에서 token 추출
        String token = request.getHeader("Bearer");
        if (token == null || token.isEmpty()) {
            throw new InterceptorException(InterceptorExceptionType.UNAUTHORIZED);
        }

        //2. token 유효성 검증 및 token에서 payload의 tokenKey 추출
        if (jwtService.verifyToken(token)) {
            jwtTokenDTO = jwtService.parsingToken(token);
        }
        else {
            throw new InterceptorException(InterceptorExceptionType.UNAUTHORIZED);
        }

        //3. tokenKey를 이용하여 DB에 저장된 데이터 추출, 컨트롤러에서 사용할 데이터 세팅
        if (jwtTokenDTO != null) {
            jwtService.exchangeTokenToUserSession(request, jwtTokenDTO);
        }

        if (request.getAttribute(CommonMessage.USER_SESSION) == null) {
            return false;
        }

        return true;
    }
}
