package com.zoozoo.report.service;

import com.zoozoo.board.persistence.BoardMapper;
import com.zoozoo.boardReply.persistence.BoardReplyMapper;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.permission.domain.CommAuthRequestDto;
import com.zoozoo.permission.domain.CommTemporaryDeniedRequestDto;
import com.zoozoo.permission.persistence.PermissionMapper;
import com.zoozoo.report.domain.ReportBoardRequestDTO;
import com.zoozoo.report.domain.ReportReplyRequestDTO;
import com.zoozoo.report.persistence.ReportMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class ReportServiceImpl implements ReportService {

    private final ReportMapper reportMapper;
    private final BoardMapper boardMapper;
    private final BoardReplyMapper replyMapper;
    private final PermissionMapper permissionMapper;

    @Transactional(rollbackFor = Exception.class)
    public Result reportBoard(ReportBoardRequestDTO requestDTO) {

        // 1. 신고자가 글 작성자인지 확인
        if (!reportMapper.isReporterIsBoardWriter(requestDTO)) {

            // 2. 글 신고
            reportMapper.insertReportBoard(requestDTO);

            // 3. 해당 글 신고 횟수 확인
            int reportCount = reportMapper.selectBoardReportedCount(requestDTO);

            // 4. 글 신고 횟수 >= 3인 경우 글 비노출 처리 및 UPDATE Report.state = 2
            if (reportCount >= CommonMessage.BLOCK_TEXT_BASE_REPORT_COUNT) {
                boardMapper.updateBoardDisplayN(requestDTO.getBoardNo());
                reportMapper.updateBoardReportState(requestDTO.setAfterWorkState());

                // 5. 작성자 신고 + 1 회 및 누적 신고 횟수 >= 3인 경우 일시정지
                int writerNo = boardMapper.selectBoardWriterNo(requestDTO.getBoardNo());
                this.doUserReportTempDenied(writerNo);
            }

            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE).build();
        }

        return Result.builder()
                .code(HttpStatus.OK.value())
                .message(CommonMessage.REPORTER_IS_SAME_WITH_WRITER).build();
    }

    @Transactional(rollbackFor = Exception.class)
    public Result reportReply(ReportReplyRequestDTO requestDTO) {
        // 1. 신고자가 댓글 작성자인지 확인
        if (!reportMapper.isReporterIsReplyWriter(requestDTO)) {

            // 2. 댓글 신고
            reportMapper.insertReportReply(requestDTO);

            // 3. 해당 댓글 신고 횟수 확인
            int reportCount = reportMapper.selectReplyReportedCount(requestDTO);

            // 4. 댓글 신고 횟수 >= 3인 경우 댓글 비노출 처리 및 UPDATE Report.state = 2
            if (reportCount >= CommonMessage.BLOCK_TEXT_BASE_REPORT_COUNT) {
                replyMapper.updateReplyDisplayN(requestDTO.getReplyNo());
                reportMapper.updateReplyReportState(requestDTO.setAfterWorkState());

                // 5. 작성자 신고 + 1 회 및 누적 신고 횟수 >= 3인 경우 일시정지
                int writerNo = replyMapper.selectReplyWriterNo(requestDTO.getReplyNo());
                this.doUserReportTempDenied(writerNo);
            }

            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE).build();
        }

        return Result.builder()
                .code(HttpStatus.OK.value())
                .message(CommonMessage.REPORTER_IS_SAME_WITH_WRITER).build();
    }

    private void doUserReportTempDenied(int userNo) {
        reportMapper.updateReportedCount(userNo);
        int reportedCount = reportMapper.selectReportedCount(userNo);

        if ((reportedCount > 0) && (reportedCount % CommonMessage.TEMPORARY_DENIED_BASE_REPORT_COUNT == 0)) {
            reportMapper.updateTemporaryDeniedCount(userNo);

            CommTemporaryDeniedRequestDto requestDto = CommTemporaryDeniedRequestDto.builder().build().setUserNo(userNo);

            if (permissionMapper.selectCommPermForUser(CommAuthRequestDto.builder().userNo(userNo).build()) == null) {
                permissionMapper.insertUserTemporaryDenied(requestDto);
            }
            else {
                permissionMapper.updateUserTemporaryDenied(requestDto);
            }
        }
    }
}
