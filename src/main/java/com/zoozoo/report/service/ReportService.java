package com.zoozoo.report.service;

import com.zoozoo.common.domain.Result;
import com.zoozoo.report.domain.ReportBoardRequestDTO;
import com.zoozoo.report.domain.ReportReplyRequestDTO;
import org.springframework.stereotype.Service;

@Service
public interface ReportService {

    Result reportReply(ReportReplyRequestDTO requestDTO);

    Result reportBoard(ReportBoardRequestDTO requestDTO);
}
