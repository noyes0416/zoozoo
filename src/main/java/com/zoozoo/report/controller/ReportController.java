package com.zoozoo.report.controller;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.report.domain.ReportBoardRequestDTO;
import com.zoozoo.report.domain.ReportReplyRequestDTO;
import com.zoozoo.report.service.ReportService;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequestMapping(value = "/api/v1/report")
@RequiredArgsConstructor
@RestController
@Api(description = "신고")
public class ReportController {

    private final ReportService reportService;

    @ApiOperation(value = "커뮤니티 게시글 신고")
    @RequestMapping(value = "/board", method = RequestMethod.POST)
    public ResponseEntity<Result> reportBoard(HttpServletRequest request, @RequestBody ReportBoardRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setReporterNo(userSession.getUserNo());

        Result result = reportService.reportBoard(requestDTO);

        return ResponseEntity.ok().body(result);
    }

    @ApiOperation(value = "커뮤니티 댓글(대댓글) 신고")
    @RequestMapping(value = "/reply", method = RequestMethod.POST)
    public ResponseEntity<Result> reportReply(HttpServletRequest request, @RequestBody ReportReplyRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setReporterNo(userSession.getUserNo());

        Result result = reportService.reportReply(requestDTO);

        return ResponseEntity.ok().body(result);
    }
}
