package com.zoozoo.report.domain;

import com.zoozoo.common.enumeration.ReportStateType;
import com.zoozoo.common.enumeration.TextType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class ReportReplyRequestDTO {
    private final int replyNo;
    private final int reportTypeNo;
    @ApiModelProperty(hidden = true)
    private int reporterNo;
    @ApiModelProperty(hidden = true)
    private final int replyType;
    @ApiModelProperty(hidden = true)
    private int reportNo;
    @ApiModelProperty(hidden = true)
    private int state;

    @Builder
    public ReportReplyRequestDTO (final int replyNo, final int reportTypeNo) {
        this.replyNo = replyNo;
        this.reportTypeNo = reportTypeNo;
        this.replyType = TextType.REPLY.getCode();
        this.state = ReportStateType.BEFORE_WORK.getCode();
    }

    public ReportReplyRequestDTO setReporterNo(final int userNo) {
        this.reporterNo = userNo;
        return this;
    }

    public ReportReplyRequestDTO setAfterWorkState() {
        this.state = ReportStateType.AFTER_WORK.getCode();
        return this;
    }
}
