package com.zoozoo.report.domain;

import com.zoozoo.common.enumeration.ReportStateType;
import com.zoozoo.common.enumeration.TextType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class ReportBoardRequestDTO {
    private final int boardNo;
    private final int reportTypeNo;
    @ApiModelProperty(hidden = true)
    private int reporterNo;
    @ApiModelProperty(hidden = true)
    private final int boardType;
    @ApiModelProperty(hidden = true)
    private int reportNo;
    @ApiModelProperty(hidden = true)
    private int state;

    @Builder
    public ReportBoardRequestDTO (final int boardNo, final int reportTypeNo) {
        this.boardNo = boardNo;
        this.reportTypeNo = reportTypeNo;
        this.boardType = TextType.BOARD.getCode();
        this.state = ReportStateType.BEFORE_WORK.getCode();
    }

    public ReportBoardRequestDTO setReporterNo(final int userNo) {
        this.reporterNo = userNo;
        return this;
    }

    public ReportBoardRequestDTO setAfterWorkState() {
        this.state = ReportStateType.AFTER_WORK.getCode();
        return this;
    }
}
