package com.zoozoo.report.persistence;

import com.zoozoo.report.domain.ReportBoardRequestDTO;
import com.zoozoo.report.domain.ReportReplyRequestDTO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ReportMapper {

    Boolean isReporterIsBoardWriter(ReportBoardRequestDTO requestDTO);

    Boolean isReporterIsReplyWriter(ReportReplyRequestDTO requestDTO);

    int selectBoardReportedCount(ReportBoardRequestDTO requestDTO);

    int selectReplyReportedCount(ReportReplyRequestDTO requestDTO);

    void insertReportBoard(ReportBoardRequestDTO requestDTO);

    void insertReportReply(ReportReplyRequestDTO requestDTO);

    void updateBoardReportState(ReportBoardRequestDTO requestDTO);

    void updateReplyReportState(ReportReplyRequestDTO requestDTO);

    void updateReportedCount(int userNo);

    int selectReportedCount(int userNo);

    void updateTemporaryDeniedCount(int userNo);
}
