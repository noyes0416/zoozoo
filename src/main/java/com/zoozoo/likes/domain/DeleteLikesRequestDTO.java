package com.zoozoo.likes.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class DeleteLikesRequestDTO {
    @ApiModelProperty(hidden = true)
    private int userNo;
    private final int type;
    private final int typeValue;

    @Builder
    public DeleteLikesRequestDTO(final int type, final int typeValue) {
        this.type = type;
        this.typeValue = typeValue;
    }

    public DeleteLikesRequestDTO setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
