package com.zoozoo.likes.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
public class ExistsLikesRequestDTO {
    private final int userNo;
    private final int type;
    private final int typeValue;

    @Builder
    public ExistsLikesRequestDTO (final int userNo, final int type, final int typeValue) {
        this.userNo = userNo;
        this.type = type;
        this.typeValue = typeValue;
    }
}
