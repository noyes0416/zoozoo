package com.zoozoo.likes.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class InsertLikesRequestDTO {
    @ApiModelProperty(hidden = true)
    private int userNo;
    private final int type;
    private final int typeValue;

    @Builder
    public InsertLikesRequestDTO(final int type, final int typeValue) {
        this.type = type;
        this.typeValue = typeValue;
    }

    public InsertLikesRequestDTO setUserNo (final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
