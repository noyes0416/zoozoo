package com.zoozoo.likes.controller;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.likes.domain.InsertLikesRequestDTO;
import com.zoozoo.likes.domain.DeleteLikesRequestDTO;
import com.zoozoo.likes.service.LikesService;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequestMapping(value = "/api/v1/likes")
@RequiredArgsConstructor
@RestController
@Api(description = "좋아요")
public class LikesController {

    private final LikesService likesService;

    @ApiOperation(value = "좋아요")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Result> insertLikes(HttpServletRequest request, @RequestBody InsertLikesRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setUserNo(userSession.getUserNo());

        return ResponseEntity.ok().body(likesService.insertLikes(requestDTO));
    }

    @ApiOperation(value = "좋아요 취소")
    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<Result> deleteLikes(HttpServletRequest request, @RequestBody DeleteLikesRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setUserNo(userSession.getUserNo());

        return ResponseEntity.ok().body(likesService.deleteLikes(requestDTO));
    }
}
