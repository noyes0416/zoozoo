package com.zoozoo.likes.service;

import com.zoozoo.common.domain.Result;
import com.zoozoo.likes.domain.ExistsLikesRequestDTO;
import com.zoozoo.likes.domain.InsertLikesRequestDTO;
import com.zoozoo.likes.domain.DeleteLikesRequestDTO;
import com.zoozoo.likes.persistence.LikesMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.zoozoo.common.domain.CommonMessage.OK_MESSAGE;

@Service
@RequiredArgsConstructor
public class LikesServiceImpl implements LikesService {

    private final LikesMapper likesMapper;

    @Transactional(rollbackFor = Exception.class)
    public Result insertLikes(InsertLikesRequestDTO requestDTO) {
        ExistsLikesRequestDTO existsLikesRequestDTO = ExistsLikesRequestDTO.builder()
                .userNo(requestDTO.getUserNo())
                .type(requestDTO.getType())
                .typeValue(requestDTO.getTypeValue())
                .build();

        if (!this.isExistsLikes(existsLikesRequestDTO))
            likesMapper.insertLikes(requestDTO);

        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }

    @Transactional(rollbackFor = Exception.class)
    public Result deleteLikes(DeleteLikesRequestDTO requestDTO) {
        ExistsLikesRequestDTO existsLikesRequestDTO = ExistsLikesRequestDTO.builder()
                .userNo(requestDTO.getUserNo())
                .type(requestDTO.getType())
                .typeValue(requestDTO.getTypeValue())
                .build();

        if (this.isExistsLikes(existsLikesRequestDTO))
            likesMapper.deleteLikes(requestDTO);

        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }

    private boolean isExistsLikes(ExistsLikesRequestDTO requestDTO) {
        int likesCount = likesMapper.selectExistsLikes(requestDTO);
        return (likesCount == 1);
    }
}
