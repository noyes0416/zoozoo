package com.zoozoo.likes.service;

import com.zoozoo.common.domain.Result;
import com.zoozoo.likes.domain.InsertLikesRequestDTO;
import com.zoozoo.likes.domain.DeleteLikesRequestDTO;
import org.springframework.stereotype.Service;

@Service
public interface LikesService {

    Result insertLikes(InsertLikesRequestDTO requestDTO);

    Result deleteLikes(DeleteLikesRequestDTO requestDTO);
}
