package com.zoozoo.likes.persistence;

import com.zoozoo.likes.domain.ExistsLikesRequestDTO;
import com.zoozoo.likes.domain.InsertLikesRequestDTO;
import com.zoozoo.likes.domain.DeleteLikesRequestDTO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LikesMapper {

    int selectExistsLikes(ExistsLikesRequestDTO requestDTO);

    void insertLikes(InsertLikesRequestDTO requestDTO);

    void deleteLikes(DeleteLikesRequestDTO requestDTO);
}
