package com.zoozoo.permission.persistence;

import com.zoozoo.permission.domain.*;
import org.mapstruct.Mapper;

@Mapper
public interface PermissionMapper
{
    void insertPermission(PermissionRequestDto requestDto);
    String selectAuth(PermissionAuthRequestDto requestDto);
    void deletePermission(PermissionRequestDto requestDto);
    void updatePermission(PermissionRequestDto requestDto);
    int isExistsPermission(PermissionRequestDto requestDto);
    CommAuthResponseDto selectCommPermForUser(CommAuthRequestDto requestDto);
    void insertUserTemporaryDenied(CommTemporaryDeniedRequestDto requestDto);
    void updateUserTemporaryDenied(CommTemporaryDeniedRequestDto requestDto);
    void updateAllMemberPermission(PermissionAllRequestDto requestDto);
}
