package com.zoozoo.permission.service.impl;

import com.zoozoo.calendar.persistence.CalendarMapper;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.CalendarType;
import com.zoozoo.common.enumeration.PermissionType;
import com.zoozoo.common.enumeration.PermitState;
import com.zoozoo.common.enumeration.State;
import com.zoozoo.permission.domain.*;
import com.zoozoo.permission.persistence.PermissionMapper;
import com.zoozoo.permission.service.PermissionService;
import com.zoozoo.shareGroup.persistence.ShareGroupMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PermissionServiceImpl implements PermissionService {
    private String methodName;
    private final PermissionMapper permissionMapper;
    private final CalendarMapper calendarMapper;
    private final ShareGroupMapper shareGroupMapper;

    public Result insertPermission(PermissionRequestDto requestDto)
    {
        methodName = "[insertPermission]";
        try
        {
            permissionMapper.insertPermission(requestDto);
            return Result.builder()
                    .code(200)
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public Result updatePermission(PermissionRequestDto requestDto)
    {
        methodName = "[updatePermission]";
        try
        {
            permissionMapper.updatePermission(requestDto);
            return Result.builder()
                    .code(200)
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public Result deletePermission(PermissionRequestDto requestDto)
    {
        methodName = "[deletePermission]";
        try
        {
            permissionMapper.deletePermission(requestDto);
            return Result.builder()
                    .code(204)
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public Result updateMemberPermission(PermissionRequestDto requestDto)
    {
        methodName = "[updateMemberPermission]";
        try
        {
            if(calendarMapper.isValidCal(CalendarType.PUBLIC.getCode(), State.Normal.getCode(), requestDto.getTypeValue()) == 0)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : inValid calNo")
                        .build();
            }

            if(calendarMapper.selectCalAdmin(requestDto.getTypeValue()) != requestDto.getRegUserNo())
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Only calendar administrators can do this.")
                        .build();
            }

            if(shareGroupMapper.isExistsMember(requestDto.getUserNo(), requestDto.getTypeValue()) == 0)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : a non-existent member.")
                        .build();
            }

            if( isExistsPermission(requestDto) == 0)
            {
                Result insertPermission = insertPermission(requestDto);
                if(insertPermission.getCode() != 200)
                {
                    return Result.builder()
                            .code(insertPermission.getCode())
                            .message(insertPermission.getMessage())
                            .build();
                }
            }
            else
            {
                Result updatePermission = updatePermission(requestDto);
                if(updatePermission.getCode() != 200)
                {
                    return Result.builder()
                            .code(updatePermission.getCode())
                            .message(updatePermission.getMessage())
                            .build();
                }
            }



            return Result.builder()
                    .code(200)
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }


    public int isExistsPermission(PermissionRequestDto requestDto)
    {
        return Optional.ofNullable(permissionMapper.isExistsPermission(requestDto)).orElseGet(()->{return 0;});
    }

    public boolean checkPermission(int userNo, int type, int typeValue, String reqAction)
    {
        if(type == PermissionType.CALENDAR.getCode())
        {
            if(calendarMapper.isValidCal(CalendarType.PUBLIC.getCode(), State.Normal.getCode(), typeValue) > 0)
            {
                int calAdmin = calendarMapper.selectCalAdmin(typeValue);
                if(calAdmin == userNo)
                {
                    return true;
                }

                if(shareGroupMapper.isValidMember(userNo, typeValue, State.Normal.getCode()) > 0)
                {
                    PermissionAuthRequestDto requestDto =  new PermissionAuthRequestDto(userNo, type, typeValue, PermitState.PERMIT.getCode());
                    String auth = permissionMapper.selectAuth(requestDto);
                    if(auth.contains(reqAction.substring(0,1)))
                    {
                        return true;
                    }
                }
            }
        }
        else if(type == PermissionType.COMMUNITY.getCode())
        {
            return true;
        }
        return false;
    }

    public CommAuthResponseDto selectCommPermForUser(CommAuthRequestDto requestDto) {
        CommAuthResponseDto responseDto = permissionMapper.selectCommPermForUser(requestDto);

        if (responseDto == null) {
            responseDto = new CommAuthResponseDto();
        }
        else {
            responseDto.setResponseFlagValue();
        }

        return responseDto;
    }

    public Result updateAllMemberPermission(PermissionAllRequestDto requestDto)
    {
        methodName = "[updateAllMemberPermission]";
        try
        {
            if(calendarMapper.selectCalAdmin(requestDto.getTypeValue()) != requestDto.getRegUserNo())
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Only calendar administrators can do this.")
                        .build();
            }

            permissionMapper.updateAllMemberPermission(requestDto);
            return Result.builder()
                    .code(200)
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }
}
