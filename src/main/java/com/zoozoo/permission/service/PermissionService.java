package com.zoozoo.permission.service;

import com.zoozoo.common.domain.Result;
import com.zoozoo.permission.domain.CommAuthRequestDto;
import com.zoozoo.permission.domain.CommAuthResponseDto;
import com.zoozoo.permission.domain.PermissionAllRequestDto;
import com.zoozoo.permission.domain.PermissionRequestDto;
import org.springframework.stereotype.Service;

@Service
public interface PermissionService {
    public Result insertPermission(PermissionRequestDto requestDto);
    public boolean checkPermission(int userNo, int type, int typeValue, String reqAction);
    public Result deletePermission(PermissionRequestDto requestDto);
    public Result updatePermission(PermissionRequestDto requestDto);
    public int isExistsPermission(PermissionRequestDto requestDto);
    Result updateMemberPermission(PermissionRequestDto requestDto);
    CommAuthResponseDto selectCommPermForUser(CommAuthRequestDto requestDto);
    Result updateAllMemberPermission(PermissionAllRequestDto requestDto);
}
