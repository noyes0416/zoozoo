package com.zoozoo.permission.domain;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.enumeration.PermissionStateType;
import com.zoozoo.common.enumeration.PermissionType;
import lombok.Builder;
import lombok.Getter;

@Getter
public class CommTemporaryDeniedRequestDto {
    private final int temporaryDeniedDay;
    private final int temporaryDeniedState;
    private final int adminNo;
    private int userNo;
    private final int communityType;

    @Builder
    public CommTemporaryDeniedRequestDto() {
        this.temporaryDeniedDay = CommonMessage.TEMPORARY_DENIED_DAY;
        this.temporaryDeniedState = PermissionStateType.TEMPORARY_DENIED.getCode();
        this.adminNo = CommonMessage.ADMIN_NO;
        this.communityType = PermissionType.COMMUNITY.getCode();
    }

    public CommTemporaryDeniedRequestDto setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
