package com.zoozoo.permission.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.enumeration.PermissionStateType;
import lombok.Getter;

@Getter
public class CommAuthResponseDto {
    @JsonIgnore
    private int state;
    private String flag;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String deniedStartDt;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String deniedEndDt;

    public CommAuthResponseDto() {
        this.flag = CommonMessage.COMMON_Y;
    }

    public void setResponseFlagValue() {
        this.flag =  PermissionStateType.getPermitFlagByState(this.state);
    }
}
