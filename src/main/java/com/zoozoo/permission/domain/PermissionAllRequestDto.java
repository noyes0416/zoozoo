package com.zoozoo.permission.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
public class PermissionAllRequestDto {
    @ApiModelProperty(example = "1", notes = "1:캘린더, 2:커뮤니티")
    private int type;
    @ApiModelProperty(example = "1", notes = "(캘린더)calNo, (커뮤니티) ")
    private int typeValue;
    @ApiModelProperty(example = "R", notes = "권한(C:create, R:read, U:update, D:delete)")
    private String auth;
    @JsonIgnore
    @Setter
    private int regUserNo;
    @ApiModelProperty(example = "1", notes = "1:permit, 2:temporary, 3:permanent")
    private int state;


    @Builder
    public PermissionAllRequestDto(int type, int typeValue, String auth, int regUserNo, int state)
    {
        this.type = type;
        this.typeValue = typeValue;
        this.auth = auth;
        this.regUserNo = regUserNo;
        this.state = state;
    }

    public void setRegUserNo(int regUserNo) {
        this.regUserNo = regUserNo;
    }
}
