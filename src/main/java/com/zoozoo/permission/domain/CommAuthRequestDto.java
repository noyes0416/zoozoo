package com.zoozoo.permission.domain;

import com.zoozoo.common.enumeration.PermissionType;
import lombok.Builder;
import lombok.Getter;

@Getter
public class CommAuthRequestDto {
    private int userNo;
    private final int communityType;

    @Builder
    public CommAuthRequestDto(final int userNo) {
        this.userNo = userNo;
        this.communityType = PermissionType.COMMUNITY.getCode();
    }
}
