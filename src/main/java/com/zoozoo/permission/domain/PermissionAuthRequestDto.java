package com.zoozoo.permission.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class PermissionAuthRequestDto
{
    private int userNo;
    private int type;
    private int typeValue;
    private int state;

    @Builder
    public PermissionAuthRequestDto(int userNo, int type, int typeValue, int state)
    {
        this.userNo = userNo;
        this.type = type;
        this.typeValue = typeValue;
        this.state = state;
    }
}
