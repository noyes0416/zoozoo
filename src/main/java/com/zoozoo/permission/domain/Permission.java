package com.zoozoo.permission.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@Component
@NoArgsConstructor
public class Permission
{
    private int permissionNo;
    private int type;
    private int typeValue;
    private int userNo;
    private String auth;
    private String note;
    private int regUserNo;
    private int updUserNo;
}
