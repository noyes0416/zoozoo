package com.zoozoo.permission.controller;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.permission.domain.CommAuthRequestDto;
import com.zoozoo.permission.domain.CommAuthResponseDto;
import com.zoozoo.permission.domain.PermissionAllRequestDto;
import com.zoozoo.permission.domain.PermissionRequestDto;
import com.zoozoo.permission.service.PermissionService;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RequestMapping("/api/v1/permission")
@RequiredArgsConstructor
@RestController
@Api(description = "권한")
public class PermissionController
{
    private final PermissionService permissionService;

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ApiOperation(value = "그룹 멤버 권한 변경")
    public @ResponseBody ResponseEntity<Result> updatePermission(@RequestBody PermissionRequestDto requestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDto.setRegUserNo(userSession.getUserNo());
        return ResponseEntity.ok().body(permissionService.updateMemberPermission(requestDto));
    }

    @RequestMapping(value = "/all", method = RequestMethod.PUT)
    @ApiOperation(value = "그룹 멤버 권한 일괄 변경")
    public @ResponseBody ResponseEntity<Result> updateAllPermission(@RequestBody PermissionAllRequestDto requestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDto.setRegUserNo(userSession.getUserNo());
        return ResponseEntity.ok().body(permissionService.updateAllMemberPermission(requestDto));
    }

    @RequestMapping(value = "/community", method = RequestMethod.GET)
    @ApiOperation(value = "커뮤니티 입장 권한 확인")
    public @ResponseBody ResponseEntity<CommAuthResponseDto> selectCommPermForUser(HttpServletRequest request) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        CommAuthRequestDto requestDto = CommAuthRequestDto.builder()
                .userNo(userSession.getUserNo())
                .build();

        return ResponseEntity.ok().body(permissionService.selectCommPermForUser(requestDto));
    }
}
