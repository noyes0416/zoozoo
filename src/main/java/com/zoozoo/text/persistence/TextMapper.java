package com.zoozoo.text.persistence;

import com.zoozoo.text.domain.Text;
import com.zoozoo.text.domain.TextCURequestDto;
import org.mapstruct.Mapper;

@Mapper
public interface TextMapper
{
    Text selectText(int type, int typeValue);
    void insertText(TextCURequestDto requestDto);
    int selectTextNo(int type, int typeValue);
    void updateText(TextCURequestDto requestDto);
    void removeText(int type, int typeValue);
    int isExistsText(int type, int typeValue);
}
