package com.zoozoo.text.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@Component
@NoArgsConstructor
public class Text {
    private int textNo;
    private int type;
    private String content;
    private int creator;
    private int lastUpdUser;
}
