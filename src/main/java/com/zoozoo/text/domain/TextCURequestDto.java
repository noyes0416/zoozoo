package com.zoozoo.text.domain;

import com.zoozoo.record.domain.dto.req.RecordCURequestDto;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

@NoArgsConstructor
@Getter
public class TextCURequestDto {
    private int textNo;
    private int type;
    private int typeValue;
    private String content;
    private int userNo;

    @Builder
    public TextCURequestDto(int type, int typeValue) {
        this.type = type;
        this.typeValue = typeValue;
    }

    @Builder
    public TextCURequestDto(int textNo, int type, int typeValue, RecordCURequestDto requestDto)
    {
        this.textNo = textNo;
        this.type = type;
        this.typeValue = typeValue;
        this.content = requestDto.getText();
        this.userNo = requestDto.getUserNo();
    }

    @Builder
    public TextCURequestDto(int type, int typeValue, RecordCURequestDto requestDto)
    {
        this.type = type;
        this.typeValue = typeValue;
        this.content = requestDto.getText();
        this.userNo = requestDto.getUserNo();
    }

    public TextCURequestDto setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }

    public TextCURequestDto setContent(final String content) {
        this.content = content;
        return this;
    }
}
