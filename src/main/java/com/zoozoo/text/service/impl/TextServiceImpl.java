package com.zoozoo.text.service.impl;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.TextType;
import com.zoozoo.record.domain.dto.req.RecordRequestDto;
import com.zoozoo.text.domain.Text;
import com.zoozoo.text.domain.TextCURequestDto;
import com.zoozoo.text.persistence.TextMapper;
import com.zoozoo.text.service.TextService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TextServiceImpl implements TextService
{
    private final TextMapper textMapper;
    private String methodName;

    public Text selectText(int type, int typeValue)
    {
        if(isExistsText(type, typeValue) > 0)
        {
            return Optional.ofNullable(textMapper.selectText(type, typeValue)).orElseGet(()->{return new Text();});
        }
        else
        {
            return new Text();
        }
    }

    public Result insertText(TextCURequestDto requestDto)
    {
        methodName = "[insertText]";

        try
        {
            textMapper.insertText(requestDto);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public Result updateText(TextCURequestDto requestDto)
    {
        methodName = "[insertText]";

        try
        {
            textMapper.updateText(requestDto);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public int selectTextNo(int type, int typeValue)
    {
        return Optional.ofNullable(textMapper.selectTextNo(type, typeValue)).orElseGet(()->{return 0;});
    }

    public int isExistsText(int type, int typeValue)
    {
        return Optional.ofNullable( textMapper.isExistsText(type, typeValue)).orElseGet(()->{return 0;});
    }

    public Result removeText(int type, int typeValue)
    {
        methodName = "[removeText]";
        try
        {
            textMapper.removeText(type, typeValue);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }
}
