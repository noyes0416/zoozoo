package com.zoozoo.text.service;

import com.zoozoo.common.domain.Result;
import com.zoozoo.text.domain.Text;
import com.zoozoo.text.domain.TextCURequestDto;
import org.springframework.stereotype.Service;

@Service
public interface TextService
{
    Result insertText(TextCURequestDto requestDto);
    Result updateText(TextCURequestDto requestDto);
    int selectTextNo(int type, int typeValue);
    Result removeText(int type, int typeValue);
    int isExistsText(int type, int typeValue);
    Text selectText(int type, int typeValue);
}
