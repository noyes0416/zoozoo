package com.zoozoo.timeline.service;

import com.zoozoo.timeline.domain.TimelineListResponseDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TimelineService {
    List<TimelineListResponseDTO> selectTimelineList(int userNo);
}
