package com.zoozoo.timeline.service;

import com.zoozoo.common.enumeration.PermissionStateType;
import com.zoozoo.common.enumeration.TimelineType;
import com.zoozoo.permission.domain.CommAuthRequestDto;
import com.zoozoo.permission.domain.CommAuthResponseDto;
import com.zoozoo.permission.persistence.PermissionMapper;
import com.zoozoo.timeline.domain.MapperResponseDTO;
import com.zoozoo.timeline.domain.TimelineListResponseDTO;
import com.zoozoo.timeline.persistence.TimelineMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TimelineServiceImpl implements TimelineService {

    private final TimelineMapper timelineMapper;
    private final PermissionMapper permissionMapper;

    private List<TimelineListResponseDTO> timelineList;

    public List<TimelineListResponseDTO> selectTimelineList(int userNo) {
        timelineList = new ArrayList<>();

        this.setCalendarList(userNo);
        if (this.isExistsPermission(userNo)) {
            this.setBestBoardList(userNo);
            this.setCommunityActiveList(userNo);
        }

        return this.timelineListSort();
    }

    private void setCalendarList(int userNo) {
        List<MapperResponseDTO> calendarRawList = timelineMapper.selectCalendarList(userNo);

        calendarRawList.forEach(calendarRaw -> {
            for (TimelineType timelineType : TimelineType.values()) {
                if (calendarRaw.getContentType() == timelineType.getCode()) {
                    String contentFormat = timelineType.getContentFormat();

                    TimelineListResponseDTO timeline = TimelineListResponseDTO.builder()
                            .contentType(calendarRaw.getContentType())
                            .contentTypeName(timelineType.getContentTypeName())
                            .content((calendarRaw.getContentType() == TimelineType.NEW_RECORD.getCode()) ?
                                    String.format(contentFormat, calendarRaw.getCalendarName()) :
                                    String.format(contentFormat, calendarRaw.getUserName(), calendarRaw.getCalendarName()))
                            .regDt(calendarRaw.getRegDt())
                            .eventType(calendarRaw.getEventType())
                            .eventTypeValue(calendarRaw.getEventTypeValue())
                            .build();

                    timelineList.add(timeline);
                }
            }
        });
    }

    private boolean isExistsPermission(int userNo) {
        CommAuthRequestDto request = CommAuthRequestDto.builder()
                .userNo(userNo).build();

        CommAuthResponseDto response = permissionMapper.selectCommPermForUser(request);

        return (response == null || response.getState() == PermissionStateType.PERMIT.getCode()) ? true : false;
    }

    private void setBestBoardList(int userNo) {
        List<MapperResponseDTO> bestBoardRawList = timelineMapper.selectBestBoardList();

        bestBoardRawList.forEach(bestBoardRaw -> {
            for (TimelineType timelineType : TimelineType.values()) {
                if (bestBoardRaw.getContentType() == timelineType.getCode()) {
                    String contentFormat = timelineType.getContentFormat();

                    TimelineListResponseDTO timeline = TimelineListResponseDTO.builder()
                            .contentType(bestBoardRaw.getContentType())
                            .contentTypeName(timelineType.getContentTypeName())
                            .content(String.format(contentFormat, bestBoardRaw.getContent()))
                            .regDt(bestBoardRaw.getRegDt())
                            .eventType(bestBoardRaw.getEventType())
                            .eventTypeValue(bestBoardRaw.getEventTypeValue())
                            .build();

                    timelineList.add(timeline);
                }
            }
        });
    }

    private void setCommunityActiveList(int userNo) {
        List<MapperResponseDTO> communityActiveRawList = timelineMapper.selectCommunityActiveList(userNo);

        communityActiveRawList.forEach(communityActiveRaw -> {
            for (TimelineType timelineType : TimelineType.values()) {
                if (communityActiveRaw.getContentType() == timelineType.getCode()) {
                    String contentFormat = timelineType.getContentFormat();

                    TimelineListResponseDTO timeline = TimelineListResponseDTO.builder()
                            .contentType(communityActiveRaw.getContentType())
                            .contentTypeName(timelineType.getContentTypeName())
                            .content(String.format(contentFormat, communityActiveRaw.getLikesCount()))
                            .regDt(communityActiveRaw.getRegDt())
                            .eventType(communityActiveRaw.getEventType())
                            .eventTypeValue(communityActiveRaw.getEventTypeValue())
                            .build();

                    timelineList.add(timeline);
                }
            }
        });
    }

    private List<TimelineListResponseDTO> timelineListSort() {
        return timelineList.stream()
                .sorted(Comparator.comparing(TimelineListResponseDTO::getRegDt).reversed())
                .collect(Collectors.toList());
    }
}
