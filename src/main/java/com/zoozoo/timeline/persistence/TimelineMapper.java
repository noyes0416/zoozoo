package com.zoozoo.timeline.persistence;

import com.zoozoo.timeline.domain.MapperResponseDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface TimelineMapper {

    List<MapperResponseDTO> selectCalendarList(int userNo);

    List<MapperResponseDTO> selectBestBoardList();

    List<MapperResponseDTO> selectCommunityActiveList(int userNo);
}
