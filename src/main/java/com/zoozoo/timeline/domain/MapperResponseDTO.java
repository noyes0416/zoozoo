package com.zoozoo.timeline.domain;

import lombok.Getter;

@Getter
public class MapperResponseDTO {
    private int contentType;
    private String content;
    private String calendarName;
    private String userName;
    private String likesCount;
    private String regDt;
    private int eventType;
    private int eventTypeValue;
}
