package com.zoozoo.timeline.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class TimelineListResponseDTO {
    private int contentType;
    private String contentTypeName;
    private String content;
    private String regDt;
    private int eventType;
    private int eventTypeValue;
}
