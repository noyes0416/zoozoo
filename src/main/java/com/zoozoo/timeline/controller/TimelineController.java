package com.zoozoo.timeline.controller;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.timeline.domain.TimelineListResponseDTO;
import com.zoozoo.timeline.service.TimelineService;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/api/v1/timeline")
@RequiredArgsConstructor
@RestController
@Api(description = "타임라인")
public class TimelineController {
    private final TimelineService timelineService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "타임라인 조회", response = TimelineListResponseDTO.class, responseContainer = "List")
    public ResponseEntity<List<TimelineListResponseDTO>> selectTimeLine(HttpServletRequest request) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);

        List<TimelineListResponseDTO> list = timelineService.selectTimelineList(userSession.getUserNo());

        return ResponseEntity.ok().body(list);
    }
}
