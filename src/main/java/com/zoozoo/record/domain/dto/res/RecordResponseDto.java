package com.zoozoo.record.domain.dto.res;

import com.zoozoo.image.domain.Image;
import com.zoozoo.record.domain.Record;
import com.zoozoo.text.domain.Text;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.util.List;

@Getter
@ApiModel(value = "일정 응답 모델")
public class RecordResponseDto {
    private int recordNo;
    private String year;
    private String month;
    private String day;
    private String text = null;
    private String image = null;
    private DrinkInfoResponseDto drinkInfo = null;

    public RecordResponseDto(Record record, Text text, Image image, DrinkInfoResponseDto drinkInfo)
    {
        this.recordNo = record.getRecordNo();
        this.year = record.getYear();
        this.month = record.getMonth();
        this.day = record.getDay();
        this.text = text.getContent();
        this.image = image.getFilePath();
        this.drinkInfo = drinkInfo;
    }
}
