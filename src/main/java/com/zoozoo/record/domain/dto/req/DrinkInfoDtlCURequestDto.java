package com.zoozoo.record.domain.dto.req;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class DrinkInfoDtlCURequestDto {
    private int mstDrinkInfoNo;
    private int selectionType;
    private String selectionNos;
    private String manualValue;
    private int userNo;

    @Builder
    public DrinkInfoDtlCURequestDto(int mstDrinkInfoNo, int selectionType, String selectionNos, String manualValue, int userNo)
    {
        this.mstDrinkInfoNo = mstDrinkInfoNo;
        this.selectionType = selectionType;
        this.selectionNos = selectionNos;
        this.manualValue = manualValue;
        this.userNo = userNo;
    }
}
