package com.zoozoo.record.domain.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zoozoo.common.enumeration.DrinkInfoType;
import com.zoozoo.common.util.CommonUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Getter
@ApiModel(value = "일정 등록·수정 요청 모델")
public class RecordCURequestDto {
    @JsonIgnore
    private int type;
    @ApiModelProperty(example = "1")
    private int  calNo;
    @ApiModelProperty(example = "2022-05-01")
    private String ymd;
    @JsonIgnore
    private String year;
    @JsonIgnore
    private String month;
    @JsonIgnore
    private String day;
    @ApiModelProperty(example = "JejuDo")
    private String location;
    @ApiModelProperty(example = "5000", dataType = "int")
    private int price;
    @ApiModelProperty(example = "1^2^3")
    private String sharedMemberNos;
    @ApiModelProperty(example = "Y")
    private char settlementYn;
    @ApiModelProperty(example = "N")
    private char drunkenYn;
    @JsonIgnore
    private int userNo;
    @ApiModelProperty(example = "textData")
    private String text;
    @ApiModelProperty(example = "imagePath")
    private String image;
    private List<DrinkInfoDtlRequestDto> drinkInfos;

    @Builder
    public RecordCURequestDto(int userNo, int calNo, String ymd, int price, String location
                                 , String sharedMemberNos, char settlementYn, char drunkenYn, String text, String image
                                 , List<DrinkInfoDtlRequestDto> drinkInfoDtlList)
    {
        CommonUtil commonUtil = new CommonUtil();
        this.userNo = userNo;
        this.type = DrinkInfoType.PUBLIC.getCode();
        this.calNo = calNo;
        this.ymd = ymd;
        this.year =  commonUtil.stringParser(ymd, "-" ,0);
        this.month = commonUtil.stringParser(ymd, "-" ,1);
        this.day = commonUtil.stringParser(ymd, "-" ,2);
        this.price = price;
        this.location = location;
        this.sharedMemberNos = sharedMemberNos;
        this.settlementYn = settlementYn;
        this.drunkenYn = drunkenYn;
        this.text = text;
        this.image = image;
        this.drinkInfos = drinkInfoDtlList;
    }
}
