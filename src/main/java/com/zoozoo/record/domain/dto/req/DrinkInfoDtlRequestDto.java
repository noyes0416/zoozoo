package com.zoozoo.record.domain.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@ApiModel(value = "음주 상세 정보 요청 모델")
public class DrinkInfoDtlRequestDto {
    @ApiModelProperty(example = "1", dataType = "int")
    private int selectionType;
    @ApiModelProperty(example = "100^210")
    private String selectionNos;
    @ApiModelProperty(example = "testValue")
    private String manualValue;

    @Builder
    public DrinkInfoDtlRequestDto(int selectionType, String selectionNos, String manualValue)
    {
        this.selectionType = selectionType;
        this.selectionNos = selectionNos;
        this.manualValue = manualValue;
    }
}
