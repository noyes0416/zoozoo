package com.zoozoo.record.domain.dto.req;

import com.zoozoo.common.util.CommonUtil;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class RecordRequestDto {
    private int calNo;
    private String year;
    private String month;
    private String day;

    @Builder
    public RecordRequestDto(int calNo, String YMD)
    {
        CommonUtil commonUtil = new CommonUtil();
        this.calNo = calNo;
        this.year =  commonUtil.stringParser(YMD, "-" ,0);
        this.month = commonUtil.stringParser(YMD, "-" ,1);
        this.day = commonUtil.stringParser(YMD, "-" ,2);
    }
}
