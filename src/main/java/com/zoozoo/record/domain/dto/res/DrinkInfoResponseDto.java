package com.zoozoo.record.domain.dto.res;

import com.zoozoo.record.domain.DrinkInfo;
import com.zoozoo.record.domain.DrinkInfoDtl;
import io.swagger.annotations.ApiModel;
import lombok.Getter;

import java.util.List;

@Getter
@ApiModel(value = "음주 정보 응답 모델")
public class DrinkInfoResponseDto {
    private int type;
    private int typeValue;
    private int price = 0;
    private String location = null;
    private String sharedMemberNos = null;
    private char settlementYn;
    private char drunkenYn;
    private List<DrinkInfoDtl> drinkInfoDtlList;

    public DrinkInfoResponseDto(DrinkInfo drinkInfo, List<DrinkInfoDtl> drinkInfoDtl)
    {
        this.type = drinkInfo.getType();
        this.typeValue = drinkInfo.getTypeValue();
        this.price = drinkInfo.getPrice();
        this.location = drinkInfo.getLocation();
        this.sharedMemberNos = drinkInfo.getSharedMemberNos();
        this.settlementYn = drinkInfo.getSettlementYn();
        this.drunkenYn = drinkInfo.getDrunkenYn();
        this.drinkInfoDtlList = drinkInfoDtl;
    }
}
