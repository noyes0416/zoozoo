package com.zoozoo.record.domain.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@ApiModel(value = "일정 삭제 요청 모델")
public class RecordDRequestDto {
    private int calNo;
    private int recordNo;
    @Setter
    @JsonIgnore
    private int userNo;

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }
}
