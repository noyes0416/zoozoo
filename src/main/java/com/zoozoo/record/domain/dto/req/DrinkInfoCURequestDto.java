package com.zoozoo.record.domain.dto.req;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class DrinkInfoCURequestDto {
    private int type;
    private int typeValue;
    private String location;
    private int price;
    private String sharedMemberNos;
    private char settlementYn;
    private char drunkenYn;
    private int userNo;

    @Builder
    public DrinkInfoCURequestDto(int type, int typeValue, RecordCURequestDto requestDto)
    {
        this.type = type;
        this.typeValue = typeValue;
        this.location = requestDto.getLocation();
        this.price = requestDto.getPrice();
        this.sharedMemberNos = requestDto.getSharedMemberNos();
        this.settlementYn = requestDto.getSettlementYn();
        this.drunkenYn = requestDto.getDrunkenYn();
        this.userNo = requestDto.getUserNo();
    }
}
