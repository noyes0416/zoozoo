package com.zoozoo.record.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@NoArgsConstructor
@Getter
public class DrinkInfo {
    private int mstDrinkInfoNo;
    private int type;
    private int typeValue;
    private String location;
    private int price;
    private String sharedMemberNos;
    private char settlementYn;
    private char drunkenYn;
    private int creator;
    private int lastUpdUser;
}
