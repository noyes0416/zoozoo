package com.zoozoo.record.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
@Getter
public class DrinkInfoDtl {
    private int drinkInfoNo;
    private int selectionType;
    private String selectionNos;
    private String manualValue;
    private char displayYn;
    private int creator;
    private int lastUpdUser;
}
