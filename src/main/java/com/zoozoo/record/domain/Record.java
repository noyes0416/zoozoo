package com.zoozoo.record.domain;

import com.zoozoo.common.domain.BaseTime;
import com.zoozoo.image.domain.Image;
import com.zoozoo.text.domain.Text;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
@Getter
public class Record {
    private int recordNo;
    private int calNo;
    private String year;
    private String month;
    private String day;
    private int creator;
    private int lastUpdUser;
    private BaseTime baseTime;

    private Text text;
    private Image image;
    private DrinkInfo drinkInfo;
}
