package com.zoozoo.record.controller;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.record.domain.dto.req.RecordCURequestDto;
import com.zoozoo.record.domain.dto.req.RecordDRequestDto;
import com.zoozoo.record.domain.dto.req.RecordRequestDto;
import com.zoozoo.record.domain.dto.res.RecordResponseDto;
import com.zoozoo.record.service.RecordService;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/api/v1/record")
@RequiredArgsConstructor
@RestController
@Api(description = "일정")
public class RecordController {
    private final RecordService recordService;
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "일정 조회", response = RecordResponseDto.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "calNo", value = "캘린더 번호", required = true),
            @ApiImplicitParam(name = "YMD", value = "조회일(yyyy-MM-dd)", required = true)
    })
    public @ResponseBody ResponseEntity<RecordResponseDto> selectRecord(int calNo, String YMD, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        RecordRequestDto requestDto = new RecordRequestDto(calNo, YMD);
        if(!recordService.selectRecord(requestDto, userSession.getUserNo()).equals(null))
        {
            return ResponseEntity.ok().body(recordService.selectRecord(requestDto, userSession.getUserNo()));
        }
        else
        {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "일정 등록", response = Result.class)
    public @ResponseBody ResponseEntity<Result> insertRecord(@RequestBody RecordCURequestDto requestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        RecordCURequestDto recordCURequestDto = new RecordCURequestDto(userSession.getUserNo(), requestDto.getCalNo(), requestDto.getYmd(), requestDto.getPrice(), requestDto.getLocation()
                                                                      ,requestDto.getSharedMemberNos(), requestDto.getSettlementYn(), requestDto.getDrunkenYn(), requestDto.getText(), requestDto.getImage()
                                                                      ,requestDto.getDrinkInfos());
        return ResponseEntity.ok().body(recordService.registerRecord(recordCURequestDto));
    }
    
    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ApiOperation(value = "일정 삭제", response = Result.class)
    public @ResponseBody ResponseEntity<Result> deleteRecord(@RequestBody RecordDRequestDto requestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDto.setUserNo(userSession.getUserNo());
        return ResponseEntity.ok().body(recordService.deleteRecord(requestDto));
    }
}
