package com.zoozoo.record.service.impl;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.DrinkInfoType;
import com.zoozoo.record.domain.DrinkInfo;
import com.zoozoo.record.domain.DrinkInfoDtl;
import com.zoozoo.record.domain.dto.req.DrinkInfoCURequestDto;
import com.zoozoo.record.domain.dto.req.DrinkInfoDtlCURequestDto;
import com.zoozoo.record.domain.dto.res.DrinkInfoResponseDto;
import com.zoozoo.record.persistence.DrinkInfoMapper;
import com.zoozoo.record.service.DrinkInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DrinkInfoServiceImpl implements DrinkInfoService {

    private final DrinkInfoMapper drinkInfoMapper;
    private String methodName;

    public Result insertDrinkInfo(DrinkInfoCURequestDto requestDro)
    {
        methodName = "[insertRecord]";

        try
        {
            drinkInfoMapper.insertDrinkInfo(requestDro);

            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public Result updateDrinkInfo(DrinkInfoCURequestDto requestDro)
    {
        methodName = "[updateDrinkInfo]";

        try
        {
            drinkInfoMapper.updateDrinkInfo(requestDro);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public int selectMstDrinkInfoNo(int type, int typeValue)
    {
        return Optional.ofNullable(drinkInfoMapper.selectMstDrinkInfoNo(type, typeValue)).orElseGet(()->{return 0;});
    }

    public Result insertDrinkInfoDtl(DrinkInfoDtlCURequestDto requestDto)
    {
        methodName = "[insertRecord]";

        try
        {
            drinkInfoMapper.insertDrinkInfoDtl(requestDto);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public Result registerDrinkInfoDtl(int mstDrinkInfoNo, int selectionType, String selectionNos, String manualValue, int userNo)
    {
        methodName = "[registerDrinkInfoDtl]";

        try
        {
            DrinkInfoDtlCURequestDto requestDro = new DrinkInfoDtlCURequestDto(mstDrinkInfoNo, selectionType, selectionNos, manualValue, userNo);

            if(isExistsDrinkInfoDtl(mstDrinkInfoNo, selectionType) > 0)
            {
                if(selectionNos.equals(""))
                {
                    Result deleteResult = deleteDrinkInfoDtl(mstDrinkInfoNo, selectionType, userNo);
                    return Result.builder()
                            .code(deleteResult.getCode())
                            .message(deleteResult.getMessage())
                            .build();
                }
                else
                {
                    Result updateResult = updateDrinkInfoDtl(requestDro);
                    return Result.builder()
                            .code(updateResult.getCode())
                            .message(updateResult.getMessage())
                            .build();
                }
            }
            else
            {
                if(!selectionNos.equals(""))
                {
                    Result insertResult = insertDrinkInfoDtl(requestDro);
                    return Result.builder()
                            .code(insertResult.getCode())
                            .message(insertResult.getMessage())
                            .build();
                }
                else
                {
                    return Result.builder()
                            .code(HttpStatus.OK.value())
                            .message(CommonMessage.OK_MESSAGE)
                            .build();
                }
            }
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public Result deleteDrinkInfoDtl(int mstDrinkInfoNo, int selectionType, int userNo)
    {
        methodName = "[deleteDrinkInfoDtl]";
        try
        {
            drinkInfoMapper.deleteDrinkInfoDtl(mstDrinkInfoNo, selectionType, userNo);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public Result updateDrinkInfoDtl(DrinkInfoDtlCURequestDto requestDto)
    {
        methodName = "[updateDrinkInfoDtl]";
        try
        {
            drinkInfoMapper.updateDrinkInfoDtl(requestDto);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public Result deleteDrinkInfoDtlBulk(int userNo, int recordNo)
    {
        methodName = "[deleteDrinkInfoDtlBulk]";
        try
        {
            int mstDrinkInfoNo = Optional.ofNullable(drinkInfoMapper.selectMstDrinkInfoNo(DrinkInfoType.PUBLIC.getCode(), recordNo)).orElseGet(()->{return 0;});

            if(mstDrinkInfoNo == 0)
            {
                return Result.builder()
                        .code(404)
                        .message("fail to delete drinkInfoDtl")
                        .build();
            }
            else
            {
                drinkInfoMapper.deleteDrinkInfoDtlBulk(userNo, mstDrinkInfoNo);

                return Result.builder()
                        .code(HttpStatus.OK.value())
                        .message(CommonMessage.OK_MESSAGE)
                        .build();
            }
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public Result removeDrinkInfo(int type, int typeValue)
    {
        methodName = "[removeDrinkInfo]";
        try
        {
            drinkInfoMapper.removeDrinkInfo(type, typeValue);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + " : something wrong!")
                    .build();
        }
    }

    public int isExistsDrinkInfoDtl(int mstDrinkInfoNo, int selectionType)
    {
        return drinkInfoMapper.isExistsDrinkInfoDtl(mstDrinkInfoNo, selectionType);
    }

    public DrinkInfoResponseDto selectDrinkInfo(int type,int typeValue)
    {
        if(drinkInfoMapper.isExistsDrinkInfo(type, typeValue) > 0)
        {
            DrinkInfo drinkInfo = drinkInfoMapper.selectDrinkInfo(type, typeValue);
            List<DrinkInfoDtl> drinkInfoDtl = drinkInfoMapper.selectDrinkInfoDtl(drinkInfo.getMstDrinkInfoNo());
            DrinkInfoResponseDto responseDto = new DrinkInfoResponseDto(drinkInfo, drinkInfoDtl);

            return responseDto;
        }
        else
        {
            return new DrinkInfoResponseDto(new DrinkInfo(), new ArrayList<>());
        }
    }

    public int isExistsDrinkInfo(int type,int typeValue)
    {
        return Optional.ofNullable(drinkInfoMapper.isExistsDrinkInfo(type, typeValue)).orElseGet(()->{return 0;});
    }
}
