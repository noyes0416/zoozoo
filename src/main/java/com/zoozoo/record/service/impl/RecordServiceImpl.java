package com.zoozoo.record.service.impl;

import com.zoozoo.calendar.domain.dto.req.CalRequestDto;
import com.zoozoo.calendar.domain.dto.res.CalResponseDto;
import com.zoozoo.calendar.domain.dto.res.CalResponseDtos;
import com.zoozoo.calendar.persistence.CalendarMapper;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.*;
import com.zoozoo.image.domain.Image;
import com.zoozoo.image.service.ImageService;
import com.zoozoo.permission.service.PermissionService;
import com.zoozoo.record.domain.DrinkInfo;
import com.zoozoo.record.domain.Record;
import com.zoozoo.record.domain.dto.req.DrinkInfoCURequestDto;
import com.zoozoo.record.domain.dto.req.RecordCURequestDto;
import com.zoozoo.record.domain.dto.req.RecordDRequestDto;
import com.zoozoo.record.domain.dto.req.RecordRequestDto;
import com.zoozoo.record.domain.dto.res.DrinkInfoResponseDto;
import com.zoozoo.record.domain.dto.res.RecordResponseDto;
import com.zoozoo.record.persistence.RecordMapper;
import com.zoozoo.record.service.DrinkInfoService;
import com.zoozoo.record.service.RecordService;
import com.zoozoo.text.domain.Text;
import com.zoozoo.text.domain.TextCURequestDto;
import com.zoozoo.text.service.TextService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RecordServiceImpl implements RecordService {
    private final RecordMapper recordMapper;
    private final CalendarMapper calendarMapper;
    private final PermissionService permissionService;
    private final TextService textService;
    private final DrinkInfoService drinkInfoService;
    private final ImageService imageService;
    private String methodName;

    public CalResponseDtos selectRecordList(CalRequestDto requestDto) {
        if(!permissionService.checkPermission(requestDto.getUserNo(), PermissionType.CALENDAR.getCode(), requestDto.getCalNo(), "R"))
        {
            return null;
        }
        else
        {
            List<CalResponseDto> responseDtos = recordMapper.selectRecordList(requestDto)
                    .stream().map(CalResponseDto::new)
                    .collect(Collectors.toList());
            CalResponseDtos response = new CalResponseDtos(responseDtos, requestDto);
            response.setCalNm(calendarMapper.selectCalNm(requestDto.getCalNo()));
            return response;
        }
    }

    public RecordResponseDto selectRecord(RecordRequestDto requestDto, int userNo)
    {
        if(!permissionService.checkPermission(userNo, PermissionType.CALENDAR.getCode(), requestDto.getCalNo(), "R"))
        {
            return null;
        }
        if(isExistsRecord(requestDto) > 0)
        {
            Record record = recordMapper.selectRecord(requestDto);
            Text text = textService.selectText(TextType.CALENDAR.getCode(), record.getRecordNo());
            Image image = imageService.selectImage(ImageType.CALENDAR.getCode(), record.getRecordNo());
            DrinkInfoResponseDto drinkInfo = drinkInfoService.selectDrinkInfo(DrinkInfoType.PUBLIC.getCode(), record.getRecordNo());
            RecordResponseDto responseDto = new RecordResponseDto(record, text, image, drinkInfo);
            return responseDto;
        }
        else
        {
            return new RecordResponseDto(new Record(), new Text(), new Image(), new DrinkInfoResponseDto(new DrinkInfo(), new ArrayList<>()));
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Result registerRecord(RecordCURequestDto requestDto)
    {
        methodName = "[registerRecord]";
        try
        {
            if(calendarMapper.isValidCal(CalendarType.PUBLIC.getCode(), State.Normal.getCode(), requestDto.getCalNo()) > 0)
            {
                if(isExistsRecord(new RecordRequestDto(requestDto.getCalNo(), requestDto.getYmd())) > 0)
                {
                    int recordNo = Optional.ofNullable(selectRecordNo(requestDto)).orElseGet(()->{return 0;});

                    Result updateResult = updateRecord(requestDto, recordNo);
                    return Result.builder()
                            .code(updateResult.getCode())
                            .message(updateResult.getMessage())
                            .build();
                }
                else
                {
                    Result insertResult = insertRecord(requestDto);
                    return Result.builder()
                            .code(insertResult.getCode())
                            .message(insertResult.getMessage())
                            .build();
                }
            }
            else
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : invalid calNo")
                        .build();
            }
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public Result updateRecord(RecordCURequestDto requestDto, int recordNo)
    {
        methodName = "[updateRecord]";
        try
        {
            if(!permissionService.checkPermission(requestDto.getUserNo(), PermissionType.CALENDAR.getCode(), requestDto.getCalNo(), "U"))
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Target member does not have permission to modify")
                        .build();
            }
            if(textService.isExistsText(TextType.CALENDAR.getCode(), recordNo) > 0)
            {
                int textNo = textService.selectTextNo(TextType.CALENDAR.getCode(), recordNo);
                TextCURequestDto textCURequestDto = new TextCURequestDto(textNo, TextType.CALENDAR.getCode(), recordNo, requestDto);
                Result textResult = textService.updateText(textCURequestDto);
                if(textResult.getCode() != 200)
                {
                    return Result.builder()
                            .code(textResult.getCode())
                            .message(textResult.getMessage())
                            .build();
                }
            }

            if(drinkInfoService.isExistsDrinkInfo(DrinkInfoType.PUBLIC.getCode(), recordNo) > 0)
            {
                DrinkInfoCURequestDto drinkInfoCURequestDro = new DrinkInfoCURequestDto(DrinkInfoType.PUBLIC.getCode(), recordNo, requestDto);
                Result drinkInfoResult = drinkInfoService.updateDrinkInfo(drinkInfoCURequestDro);
                if(drinkInfoResult.getCode() != 200)
                {
                    return Result.builder()
                            .code(drinkInfoResult.getCode())
                            .message(drinkInfoResult.getMessage())
                            .build();
                }

                int mstDrinkInfoNo = drinkInfoService.selectMstDrinkInfoNo(DrinkInfoType.PUBLIC.getCode(), recordNo);
                requestDto.getDrinkInfos().stream().forEach(a -> {
                    try
                    {
                        drinkInfoService.registerDrinkInfoDtl(mstDrinkInfoNo, a.getSelectionType(), a.getSelectionNos(), a.getManualValue(), requestDto.getUserNo());
                    }
                    catch (Exception e)
                    {
                        Result.builder()
                                .code(500)
                                .message("[registerDrinkInfoDtl]" + e.getMessage())
                                .build();
                    }
                });
            }
            //TODO : ImageMst Update
            //TODO : ImageDtl Update

            recordMapper.updateRecord(requestDto);

            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public Result insertRecord(RecordCURequestDto requestDto)
    {
        methodName = "[insertRecord]";
        try
        {
            if(!permissionService.checkPermission(requestDto.getUserNo(), PermissionType.CALENDAR.getCode(), requestDto.getCalNo(), "C"))
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Target member does not have permission to register")
                        .build();
            }
            recordMapper.insertRecord(requestDto);
            int recordNo = selectRecordNo(requestDto);
            if (recordNo > 0)
            {
                DrinkInfoCURequestDto drinkInfoCURequestDro = new DrinkInfoCURequestDto(DrinkInfoType.PUBLIC.getCode(), recordNo, requestDto);
                Result drinkInfoResult = drinkInfoService.insertDrinkInfo(drinkInfoCURequestDro);
                if(drinkInfoResult.getCode() != 200)
                {
                    return Result.builder()
                            .code(drinkInfoResult.getCode())
                            .message(drinkInfoResult.getMessage())
                            .build();
                }

                int mstDrinkInfoNo = drinkInfoService.selectMstDrinkInfoNo(drinkInfoCURequestDro.getType(), drinkInfoCURequestDro.getTypeValue());
                if(mstDrinkInfoNo > 0)
                {
                    requestDto.getDrinkInfos().stream().forEach(a -> {
                        try
                        {
                            drinkInfoService.registerDrinkInfoDtl(mstDrinkInfoNo, a.getSelectionType(), a.getSelectionNos(), a.getManualValue(), requestDto.getUserNo());
                        }
                        catch (Exception e)
                        {
                            Result.builder()
                                    .code(500)
                                    .message("[registerDrinkInfoDtl]" + e.getMessage())
                                    .build();
                        }
                    });
                }
                else
                {
                    return Result.builder()
                            .code(404)
                            .message(methodName + " : failed to insert drinkInfo")
                            .build();
                }

                if (!requestDto.getText().equals(""))
                {
                    TextCURequestDto textCURequestDto = new TextCURequestDto(TextType.CALENDAR.getCode(), recordNo, requestDto);
                    Result textResult = textService.insertText(textCURequestDto);
                    if(textResult.getCode() != 200)
                    {
                        return Result.builder()
                                .code(textResult.getCode())
                                .message(textResult.getMessage())
                                .build();
                    }
                }

                //TODO : image Insert
                //TODO : imageDtl Insert

                return Result.builder()
                        .code(HttpStatus.OK.value())
                        .message(CommonMessage.OK_MESSAGE)
                        .build();
            }
            else
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : failed to insert record")
                        .build();
            }
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public int selectRecordNo(RecordCURequestDto requestDto)
    {
        return Optional.ofNullable(recordMapper.selectRecordNo(requestDto)).orElseGet(()->{return 0;});
    }

    public int isExistsRecord(RecordRequestDto requestDto)
    {
        return recordMapper.isExistsRecord(requestDto);
    }

    @Transactional(rollbackFor = Exception.class)
    public Result deleteRecord(RecordDRequestDto requestDto)
    {
        methodName = "[deleteRecord]";
        try
        {
            if(!permissionService.checkPermission(requestDto.getUserNo(), PermissionType.CALENDAR.getCode(), requestDto.getCalNo(), "D"))
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Target member does not have permission to delete")
                        .build();
            }
            Result insertResult = insertRecordHist(requestDto.getUserNo(), requestDto.getRecordNo());
            if(insertResult.getCode() != 200)
            {
                return Result.builder()
                        .code(insertResult.getCode())
                        .message(insertResult.getMessage())
                        .build();
            }

            Result deleteDtlResult = drinkInfoService.deleteDrinkInfoDtlBulk(requestDto.getUserNo(), requestDto.getRecordNo());
            if(deleteDtlResult.getCode() != 200)
            {
                return Result.builder()
                        .code(deleteDtlResult.getCode())
                        .message(deleteDtlResult.getMessage())
                        .build();
            }

            Result removeResult = removeRecord(requestDto.getRecordNo());
            if(removeResult.getCode() != 200)
            {
                return Result.builder()
                        .code(removeResult.getCode())
                        .message(removeResult.getMessage())
                        .build();
            }

            return Result.builder()
                    .code(204)
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public Result insertRecordHist(int userNo, int recordNo)
    {
        methodName = "[insertRecordHist]";
        try
        {
            recordMapper.insertRecordHist(userNo, recordNo);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public Result removeRecord(int recordNo)
    {
        methodName = "[removeRecord]";
        try
        {

            Result imageResult = imageService.removeImage(ImageType.CALENDAR.getCode(), recordNo);
            if(imageResult.getCode() != 200)
            {
                return Result.builder()
                        .code(imageResult.getCode())
                        .message(imageResult.getMessage())
                        .build();
            }

            Result drinkInfoResult = drinkInfoService.removeDrinkInfo(DrinkInfoType.PUBLIC.getCode(), recordNo);
            if(drinkInfoResult.getCode() != 200)
            {
                return Result.builder()
                        .code(drinkInfoResult.getCode())
                        .message(drinkInfoResult.getMessage())
                        .build();
            }

            Result textResult = textService.removeText(TextType.CALENDAR.getCode(), recordNo);
            if(textResult.getCode() != 200)
            {
                return Result.builder()
                        .code(textResult.getCode())
                        .message(textResult.getMessage())
                        .build();
            }

            recordMapper.removeRecord(recordNo);
            return Result.builder()
                    .code(HttpStatus.OK.value())
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }
}
