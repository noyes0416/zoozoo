package com.zoozoo.record.service;

import com.zoozoo.common.domain.Result;
import com.zoozoo.record.domain.dto.req.DrinkInfoCURequestDto;
import com.zoozoo.record.domain.dto.req.DrinkInfoDtlCURequestDto;
import com.zoozoo.record.domain.dto.res.DrinkInfoResponseDto;
import org.springframework.stereotype.Service;

@Service
public interface DrinkInfoService {
    Result insertDrinkInfo(DrinkInfoCURequestDto requestDro);
    int selectMstDrinkInfoNo(int type, int typeValue);
    Result updateDrinkInfo(DrinkInfoCURequestDto requestDro);
    Result registerDrinkInfoDtl (int mstDrinkInfoNo, int selectionType, String selectionNos, String manualValue, int userNo);
    Result deleteDrinkInfoDtlBulk(int userNo, int recordNo);
    Result removeDrinkInfo(int type,int typeValue);
    DrinkInfoResponseDto selectDrinkInfo(int type,int typeValue);
    int isExistsDrinkInfo(int type,int typeValue);
}
