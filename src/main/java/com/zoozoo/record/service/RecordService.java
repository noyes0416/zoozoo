package com.zoozoo.record.service;

import com.zoozoo.calendar.domain.dto.req.CalRequestDto;
import com.zoozoo.calendar.domain.dto.res.CalResponseDtos;
import com.zoozoo.common.domain.Result;
import com.zoozoo.record.domain.dto.req.RecordCURequestDto;
import com.zoozoo.record.domain.dto.req.RecordDRequestDto;
import com.zoozoo.record.domain.dto.req.RecordRequestDto;
import com.zoozoo.record.domain.dto.res.RecordResponseDto;
import org.springframework.stereotype.Service;

@Service
public interface RecordService {
    CalResponseDtos selectRecordList(CalRequestDto requestDto);
    RecordResponseDto selectRecord(RecordRequestDto requestDto, int userNo);
    Result registerRecord(RecordCURequestDto requestDto);
    Result deleteRecord(RecordDRequestDto requestDto);
}
