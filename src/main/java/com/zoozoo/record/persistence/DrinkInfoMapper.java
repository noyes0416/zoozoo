package com.zoozoo.record.persistence;

import com.zoozoo.record.domain.DrinkInfo;
import com.zoozoo.record.domain.DrinkInfoDtl;
import com.zoozoo.record.domain.dto.req.DrinkInfoCURequestDto;
import com.zoozoo.record.domain.dto.req.DrinkInfoDtlCURequestDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DrinkInfoMapper
{
    DrinkInfo selectDrinkInfo(int type, int typeValue);
    List<DrinkInfoDtl> selectDrinkInfoDtl(int mstDrinkInfoNo);
    void insertDrinkInfo(DrinkInfoCURequestDto requestDro);
    int selectMstDrinkInfoNo(int type, int typeValue);
    void insertDrinkInfoDtl(DrinkInfoDtlCURequestDto requestDro);
    void updateDrinkInfo(DrinkInfoCURequestDto requestDro);
    int selectDrinkInfoNo(int mstDrinkInfoNo, int selectionType);
    void deleteDrinkInfoDtl(int mstDrinkInfoNo, int selectionType, int userNo);
    void updateDrinkInfoDtl(DrinkInfoDtlCURequestDto requestDro);
    void deleteDrinkInfoDtlBulk(int userNo, int mstDrinkInfoNo);
    void removeDrinkInfo(int type, int typeValue);
    int isExistsDrinkInfoDtl(int mstDrinkInfoNo, int selectionType);
    int isExistsDrinkInfo(int type, int typeValue);
}
