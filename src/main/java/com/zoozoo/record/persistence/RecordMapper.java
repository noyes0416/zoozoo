package com.zoozoo.record.persistence;

import com.zoozoo.calendar.domain.dto.req.CalRequestDto;
import com.zoozoo.record.domain.Record;
import com.zoozoo.record.domain.dto.req.RecordCURequestDto;
import com.zoozoo.record.domain.dto.req.RecordRequestDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface RecordMapper
{
    List<Record> selectRecordList(CalRequestDto calRequestDto);
    Record selectRecord(RecordRequestDto requestDto);
    void insertRecord(RecordCURequestDto requestDto);
    int selectRecordNo(RecordCURequestDto requestDto);
    int isExistsRecord(RecordRequestDto requestDto);
    void updateRecord(RecordCURequestDto requestDto);
    void insertRecordHist(int userNo, int recordNo);
    void removeRecord(int recordNo);
}
