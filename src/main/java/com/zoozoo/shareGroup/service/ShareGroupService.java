package com.zoozoo.shareGroup.service;

import com.zoozoo.common.domain.Result;
import com.zoozoo.shareGroup.domain.dto.MemberInviteResponseDto;
import com.zoozoo.shareGroup.domain.dto.MemberReplyRequestDto;
import com.zoozoo.shareGroup.domain.dto.MemberRequestDto;
import com.zoozoo.shareGroup.domain.dto.MemberResponseDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ShareGroupService {
    Result insertMember(MemberRequestDto requestDto);
    int isValidMember(int userNo, int calNo, int state);
    Result deleteMember(MemberRequestDto requestDto);
    Result replyInvite(MemberReplyRequestDto requestDto);
    List<MemberResponseDto> selectMember(int userNo, int calNo);
    Result insertShareGroup(int userNo, int calNo);
    List<MemberInviteResponseDto> selectMemberInvite(int userNo);
}
