package com.zoozoo.shareGroup.service.impl;

import com.zoozoo.calendar.persistence.CalendarMapper;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.*;
import com.zoozoo.permission.domain.PermissionRequestDto;
import com.zoozoo.permission.service.PermissionService;
import com.zoozoo.shareGroup.domain.dto.MemberInviteResponseDto;
import com.zoozoo.shareGroup.domain.dto.MemberReplyRequestDto;
import com.zoozoo.shareGroup.domain.dto.MemberRequestDto;
import com.zoozoo.shareGroup.domain.dto.MemberResponseDto;
import com.zoozoo.shareGroup.persistence.ShareGroupMapper;
import com.zoozoo.shareGroup.service.ShareGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ShareGroupServiceImpl implements ShareGroupService
{
    private final ShareGroupMapper shareGroupMapper;
    private final CalendarMapper calendarMapper;
    private final PermissionService permissionService;

    private String methodName;

    public Result insertShareGroup(int userNo, int calNo)
    {
        methodName = "[insertShareGroup]";
        try
        {
            shareGroupMapper.insertShareGroup(userNo, calNo);
            return Result.builder()
                    .code(200)
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Result insertMember(MemberRequestDto requestDto)
    {
        methodName = "[insertMember]";
        try
        {
            if(selectMemberCount(requestDto.getCalNo()) > 15)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Maximum number of member exceeded(15)")
                        .build();
            }
            if(calendarMapper.isValidCal(CalendarType.PUBLIC.getCode(), State.Normal.getCode(), requestDto.getCalNo()) == 0)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : InValid CalNo")
                        .build();
            }
            if(calendarMapper.selectCalAdmin(requestDto.getCalNo()) != requestDto.getAdminUser())
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Only calendar administrators can do this.")
                        .build();
            }
            if(isExistsMember(requestDto.getUserNo(), requestDto.getCalNo()) > 0)
            {
                if(selectMemberState(requestDto.getUserNo(), requestDto.getCalNo()) == ShareGroupStateType.Normal.getCode())
                {
                    return Result.builder()
                            .code(404)
                            .message(methodName + " : already registered member")
                            .build();
                }
                else if(selectMemberState(requestDto.getUserNo(), requestDto.getCalNo()) == ShareGroupStateType.Invited.getCode())
                {
                    shareGroupMapper.updateMember(requestDto.getUserNo(), requestDto.getCalNo(), ShareGroupStateType.Deleted.getCode());
                    return Result.builder()
                            .code(200)
                            .message(CommonMessage.OK_MESSAGE)
                            .build();
                }
                else
                {
                    shareGroupMapper.updateMember(requestDto.getUserNo(), requestDto.getCalNo(), ShareGroupStateType.Invited.getCode());
                    return Result.builder()
                            .code(200)
                            .message(CommonMessage.OK_MESSAGE)
                            .build();
                }
            }
            else
            {
                int shareGroupNo = selectShareGroupNo(requestDto.getCalNo());
                shareGroupMapper.insertMember(new MemberRequestDto(requestDto, shareGroupNo));

                return Result.builder()
                        .code(200)
                        .message(CommonMessage.OK_MESSAGE)
                        .build();
            }
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public int selectMemberCount(int calNo)
    {
        return Optional.ofNullable(shareGroupMapper.selectMemberCount(calNo)).orElseGet(()->{return 0;});
    }

    public int selectShareGroupNo(int calNo)
    {
        return Optional.ofNullable(shareGroupMapper.selectShareGroupNo(calNo)).orElseGet(()->{return 0;});
    }

    public int isValidMember(int userNo, int calNo, int state)
    {
        return Optional.ofNullable(shareGroupMapper.isValidMember(userNo, calNo, state)).orElseGet(()->{return 0;});
    }

    public int isExistsMember(int userNo, int calNo)
    {
        return Optional.ofNullable(shareGroupMapper.isExistsMember(userNo, calNo)).orElseGet(()->{return 0;});
    }

    public int selectMemberState(int userNo, int calNo)
    {
        return Optional.ofNullable(shareGroupMapper.selectMemberState(userNo, calNo)).orElseGet(()->{return 0;});
    }

    @Transactional(rollbackFor = Exception.class)
    public Result deleteMember(MemberRequestDto requestDto)
    {
        methodName = "[deleteMember]";
        try
        {
            if(isExistsMember(requestDto.getUserNo(), requestDto.getCalNo()) == 0)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : not registered member")
                        .build();
            }
            else
            {
                int calAdmin = calendarMapper.selectCalAdmin(requestDto.getCalNo());
                if(calendarMapper.isValidCal(CalendarType.PUBLIC.getCode(), State.Normal.getCode(), requestDto.getCalNo()) == 0)
                {
                    return Result.builder()
                            .code(404)
                            .message(methodName + " : InValid CalNo")
                            .build();
                }
                if(calAdmin == requestDto.getUserNo())
                {
                    return Result.builder()
                            .code(404)
                            .message(methodName + " : Administrators cannot leave the group. If you want to leave the group, please delegate the manager.")
                            .build();
                }
                if(calAdmin != requestDto.getAdminUser() && requestDto.getUserNo() != requestDto.getAdminUser())
                {
                    return Result.builder()
                            .code(404)
                            .message(methodName + " : Only calendar administrators can do this.")
                            .build();
                }
                if(selectMemberState(requestDto.getUserNo(), requestDto.getCalNo()) == ShareGroupStateType.Deleted.getCode())
                {
                    return Result.builder()
                            .code(404)
                            .message(methodName + " : already deleted member")
                            .build();
                }
                else
                {
                    shareGroupMapper.updateMember(requestDto.getUserNo(), requestDto.getCalNo(), ShareGroupStateType.Deleted.getCode());

                    PermissionRequestDto permissionDto = new PermissionRequestDto(PermissionType.CALENDAR.getCode(), requestDto.getCalNo(), requestDto.getUserNo(), calAdmin);
                    Result deletePermission = permissionService.deletePermission(permissionDto);
                    if(deletePermission.getCode() != 204)
                    {
                        return Result.builder()
                                .code(deletePermission.getCode())
                                .message(deletePermission.getMessage())
                                .build();
                    }

                    return Result.builder()
                            .code(204)
                            .message(CommonMessage.OK_MESSAGE)
                            .build();
                }
            }
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Result replyInvite(MemberReplyRequestDto requestDto)
    {
        methodName = "[replyInvite]";
        try
        {
            if(isExistsMember(requestDto.getUserNo(), requestDto.getCalNo()) == 0)
            {

                return Result.builder()
                        .code(404)
                        .message(methodName + " : not invited member")
                        .build();
            }
            else
            {
                if(requestDto.getAcceptYn() == 'Y')
                {

                    return acceptInvite(requestDto);
                }

                if(requestDto.getAcceptYn() == 'N')
                {
                    return declineInvite(requestDto);
                }

                return Result.builder()
                        .code(404)
                        .message(methodName + " : invalid request")
                        .build();
            }
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public Result acceptInvite(MemberReplyRequestDto requestDto)
    {
            methodName = "[acceptInvite]";
            try
            {
                if(selectMemberCount(requestDto.getCalNo()) > 15)
                {
                    return Result.builder()
                            .code(404)
                            .message(methodName + " : This group is overstaffed. Please contact the administrator")
                            .build();
                }
                else
                {
                    if(selectMemberState(requestDto.getUserNo(), requestDto.getCalNo()) == ShareGroupStateType.Invited.getCode()
                        || selectMemberState(requestDto.getUserNo(), requestDto.getCalNo()) == ShareGroupStateType.Rejected.getCode())
                    {
                        int calAdmin = calendarMapper.selectCalAdmin(requestDto.getCalNo());
                        if(calAdmin == 0)
                        {
                            return Result.builder()
                                    .code(404)
                                    .message(methodName + " : invalid calNo")
                                    .build();
                        }

                        shareGroupMapper.updateMember(requestDto.getUserNo(), requestDto.getCalNo(), ShareGroupStateType.Normal.getCode());

                        PermissionRequestDto permissionDto = new PermissionRequestDto(PermissionType.CALENDAR.getCode(), requestDto.getCalNo(), "R", requestDto.getUserNo(), calAdmin, PermitState.PERMIT.getCode());
                        if(permissionService.isExistsPermission(permissionDto) == 0)
                        {
                            Result insertPermission = permissionService.insertPermission(permissionDto);
                            if(insertPermission.getCode() != 200)
                            {
                                return Result.builder()
                                        .code(insertPermission.getCode())
                                        .message(insertPermission.getMessage())
                                        .build();
                            }
                        }
                        else
                        {
                            Result updatePermission = permissionService.updatePermission(permissionDto);
                            if(updatePermission.getCode() != 200)
                            {
                                return Result.builder()
                                        .code(updatePermission.getCode())
                                        .message(updatePermission.getMessage())
                                        .build();
                            }
                        }

                        return Result.builder()
                                .code(200)
                                .message(CommonMessage.OK_MESSAGE)
                                .build();
                    }
                    else
                    {
                        return Result.builder()
                                .code(404)
                                .message(methodName + " : invalid request")
                                .build();
                    }
                }
            }
            catch (Exception e)
            {
                return Result.builder()
                        .code(500)
                        .message(methodName + e.getMessage())
                        .build();
            }
    }

    public Result declineInvite(MemberReplyRequestDto requestDto)
    {
        methodName = "[declineInvite]";
        try
        {
            if(selectMemberState(requestDto.getUserNo(), requestDto.getCalNo()) == ShareGroupStateType.Invited.getCode())
            {
                shareGroupMapper.updateMember(requestDto.getUserNo(), requestDto.getCalNo(), ShareGroupStateType.Rejected.getCode());
                return Result.builder()
                        .code(200)
                        .message(CommonMessage.OK_MESSAGE)
                        .build();
            }
            else
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : invalid request")
                        .build();
            }
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public List<MemberResponseDto> selectMember(int userNo, int calNo)
    {
        if(!permissionService.checkPermission(userNo, PermissionType.CALENDAR.getCode(), calNo, "R"))
        {
            return null;
        }
        return Optional.ofNullable(shareGroupMapper.selectMember(calNo)).orElseGet(ArrayList::new);
    }

    public List<MemberInviteResponseDto> selectMemberInvite(int userNo)
    {
        return Optional.ofNullable(shareGroupMapper.selectMemberInvite(userNo)).orElseGet(ArrayList::new);
    }
}
