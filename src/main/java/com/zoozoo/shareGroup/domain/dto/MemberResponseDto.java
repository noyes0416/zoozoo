package com.zoozoo.shareGroup.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@ApiModel(value = "그룹 멤버 정보 응답 모델")
public class MemberResponseDto {
    @ApiModelProperty(example = "1", dataType = "int")
    private int calNo;
    @ApiModelProperty(example = "1", dataType = "int")
    private int shareGroupNo;
    @ApiModelProperty(example = "1", dataType = "int")
    private int userNo;
    @ApiModelProperty(example = "애마곤듀")
    private String userName;
    @ApiModelProperty(example = "1", dataType = "int")
    private int state;

    @Builder
    MemberResponseDto(int calNo, int shareGroupNo, int userNo, String userName, int state)
    {
        this.calNo = calNo;
        this.shareGroupNo = shareGroupNo;
        this.userNo = userNo;
        this.userName = userName;
        this.state = state;
    }
}
