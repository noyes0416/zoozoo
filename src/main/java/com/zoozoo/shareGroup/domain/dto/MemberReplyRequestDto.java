package com.zoozoo.shareGroup.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@ApiModel(value = "그룹 멤버 초대 상태 변경 요청 모델")
public class MemberReplyRequestDto {
    @JsonIgnore
    private int userNo;
    @ApiModelProperty(example = "1")
    private int calNo;
    @ApiModelProperty(example = "Y")
    private char acceptYn;

    @Builder
    public MemberReplyRequestDto(int userNo, int calNo, char acceptYn)
    {
        this.userNo = userNo;
        this.calNo = calNo;
        this.acceptYn = acceptYn;
    }
}
