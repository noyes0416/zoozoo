package com.zoozoo.shareGroup.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@ApiModel(value = "초대 정보 응답 모델")
public class MemberInviteResponseDto
{
    @ApiModelProperty(example = "1", dataType = "int")
    private int calNo;
    @ApiModelProperty(example = "Test", dataType = "string")
    private String calNm;
    @ApiModelProperty(example = "1", dataType = "int")
    private int userNo;
    @ApiModelProperty(example = "3", dataType = "int")
    private int state;

    @Builder
    MemberInviteResponseDto(int calNo, String calNm, int userNo, int state)
    {
        this.calNo = calNo;
        this.calNm = calNm;
        this.userNo = userNo;
        this.state = state;
    }
}
