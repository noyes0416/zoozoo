package com.zoozoo.shareGroup.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@ApiModel(value = "그룹 멤버 상태 변경 요청 모델")
public class MemberRequestDto {
    @JsonIgnore
    private int adminUser;
    @ApiModelProperty(example = "1")
    private int calNo;
    @ApiModelProperty(example = "1")
    private int userNo;
    @JsonIgnore
    private int shareGroupNo;

    @Builder
    public MemberRequestDto(int adminUser, int calNo, int userNo)
    {
        this.adminUser = adminUser;
        this.calNo = calNo;
        this.userNo = userNo;
    }

    @Builder
    public MemberRequestDto(MemberRequestDto requestDto, int shareGroupNo)
    {
        this.adminUser = requestDto.getAdminUser();
        this.calNo = requestDto.getCalNo();
        this.userNo = requestDto.getUserNo();
        this.shareGroupNo = shareGroupNo;
    }
}
