package com.zoozoo.shareGroup.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@Component
@NoArgsConstructor
public class ShareGroup {
    private int shareGroupNo;
    private int calNo;
    private int userNo;
    private int state;
}
