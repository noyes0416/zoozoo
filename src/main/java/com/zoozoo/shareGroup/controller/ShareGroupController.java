package com.zoozoo.shareGroup.controller;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.shareGroup.domain.dto.MemberInviteResponseDto;
import com.zoozoo.shareGroup.domain.dto.MemberReplyRequestDto;
import com.zoozoo.shareGroup.domain.dto.MemberRequestDto;
import com.zoozoo.shareGroup.domain.dto.MemberResponseDto;
import com.zoozoo.shareGroup.service.ShareGroupService;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/api/v1/shareGroup")
@RequiredArgsConstructor
@RestController
@Api(description = "공유 그룹")
public class ShareGroupController {
    private final ShareGroupService shareGroupService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "공유 그룹 멤버 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "calNo", value = "캘린더 번호", required = true)
    })
    public @ResponseBody
    ResponseEntity<List<MemberResponseDto>> selectMember(int calNo, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        if(!shareGroupService.selectMember(userSession.getUserNo(), calNo).equals(null))
        {
            return ResponseEntity.ok().body(shareGroupService.selectMember(userSession.getUserNo(), calNo));
        }
        else
        {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "공유 그룹 멤버 초대/초대 취소")
    public @ResponseBody ResponseEntity<Result> insertMember(@RequestBody MemberRequestDto requestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        return ResponseEntity.ok().body(shareGroupService.insertMember(new MemberRequestDto(userSession.getUserNo(), requestDto.getCalNo(), requestDto.getUserNo())));
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ApiOperation(value = "공유 그룹 멤버 추방(+탈퇴)")
    public @ResponseBody
    ResponseEntity<Result> deleteMember(@RequestBody MemberRequestDto requestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        return ResponseEntity.ok().body(shareGroupService.deleteMember(new MemberRequestDto(userSession.getUserNo(), requestDto.getCalNo(), requestDto.getUserNo())));
    }

    @RequestMapping(value = "/invite", method = RequestMethod.PUT)
    @ApiOperation(value = "공유 그룹 초대 수락/거절")
    public @ResponseBody ResponseEntity<Result> updateMember(@RequestBody MemberReplyRequestDto requestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        return ResponseEntity.ok().body(shareGroupService.replyInvite(new MemberReplyRequestDto(userSession.getUserNo(), requestDto.getCalNo(), requestDto.getAcceptYn())));
    }

    @RequestMapping(value = "/invite", method = RequestMethod.GET)
    @ApiOperation(value = "공유 그룹 초대 조회")
    public @ResponseBody ResponseEntity<List<MemberInviteResponseDto>> selectMemberReply(HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        return ResponseEntity.ok().body(shareGroupService.selectMemberInvite(userSession.getUserNo()));
    }
}
