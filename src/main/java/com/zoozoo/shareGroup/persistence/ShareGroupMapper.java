package com.zoozoo.shareGroup.persistence;

import com.zoozoo.shareGroup.domain.dto.MemberInviteResponseDto;
import com.zoozoo.shareGroup.domain.dto.MemberRequestDto;
import com.zoozoo.shareGroup.domain.dto.MemberResponseDto;
import org.apache.ibatis.annotations.Mapper;

import java.lang.reflect.Member;
import java.util.List;

@Mapper
public interface ShareGroupMapper {
    void insertMember(MemberRequestDto requestDto);
    int selectShareGroupNo(int calNo);
    int isValidMember(int userNo, int calNo, int state);
    void updateMember(int userNo, int calNo, int state);
    int isExistsMember(int userNo, int calNo);
    int selectMemberState(int userNo, int calNo);
    int selectMemberCount(int calNo);
    List<MemberResponseDto> selectMember(int calNo);
    void insertShareGroup(int userNo, int calNo);
    List<MemberInviteResponseDto> selectMemberInvite(int userNo);
}
