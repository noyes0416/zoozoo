package com.zoozoo.calendar.domain;

import com.zoozoo.common.domain.BaseTime;
import com.zoozoo.permission.domain.Permission;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Getter
@Component
@NoArgsConstructor
public class Calendar {
    private int calNo;
    private String calNm;
    private int type;
    private int state;
    private int adminUserNo;
    private BaseTime baseTime;
    private Permission permission;
}
