package com.zoozoo.calendar.domain.dto.req;

import com.zoozoo.common.util.CommonUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@ApiModel(value = "캘린더 조회 요청 모델")
public class CalRequestDto {
    @ApiModelProperty(example = "1")
    private int userNo;
    @ApiModelProperty(example = "1")
    private int calNo;
    @ApiModelProperty(example = "2022")
    private String year;
    @ApiModelProperty(example = "05")
    private String month;
    @ApiModelProperty(example = "")
    private String filter;

    @Builder
    public CalRequestDto(int userNo, int calNo, String ym, String filter)
    {
        CommonUtil commonUtil = new CommonUtil();
        this.userNo = userNo;
        this.calNo = calNo;
        this.year = commonUtil.stringParser(ym, "-" ,0);
        this.month = commonUtil.stringParser(ym, "-" ,1);
        this.filter = filter;
    }
}
