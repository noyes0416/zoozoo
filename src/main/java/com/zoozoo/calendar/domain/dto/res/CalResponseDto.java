package com.zoozoo.calendar.domain.dto.res;

import com.zoozoo.record.domain.Record;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@Getter
@ApiModel(value = "일별 노출 여부 응답 모델")
public class CalResponseDto {
    @ApiModelProperty(example = "10")
    private String day;
    @ApiModelProperty(example = "Y")
    private char display;

    public CalResponseDto(Record record)
    {
        this.day = record.getDay();
        if(record.getRecordNo() > 0)
        {
            this.display = 'Y';
        }
        else
        {
            this.display = 'N';
        }
    }
}
