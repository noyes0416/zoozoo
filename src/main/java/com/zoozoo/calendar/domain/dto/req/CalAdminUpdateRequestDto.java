package com.zoozoo.calendar.domain.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@ApiModel(value = "캘린더 관리자 변경 요청 모델")
public class CalAdminUpdateRequestDto
{
    @JsonIgnore
    private int adminUserNo;
    private int userNo;
    private int calNo;

    @Builder
    public CalAdminUpdateRequestDto(int adminUserNo, int userNo, int calNo)
    {
        this.adminUserNo = adminUserNo;
        this.userNo = userNo;
        this.calNo = calNo;
    }
}
