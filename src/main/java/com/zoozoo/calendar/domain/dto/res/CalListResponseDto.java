package com.zoozoo.calendar.domain.dto.res;

import com.zoozoo.calendar.domain.Calendar;
import com.zoozoo.permission.domain.Permission;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@Getter
@ApiModel(value = "캘린더 리스트 조회 응답 모델")
public class CalListResponseDto {
    @ApiModelProperty(example = "1")
    private int calNo;
    @ApiModelProperty(example = "SHARE_TEST_CAL")
    private String calNm;
    private String auth;
    private char adminFlag;

    public CalListResponseDto(Calendar calendar)
    {
        this.calNo = calendar.getCalNo();
        this.calNm = calendar.getCalNm();
        this.auth = calendar.getPermission().getAuth();
        if(calendar.getAdminUserNo() == calendar.getPermission().getUserNo())
        {
            adminFlag = 'Y';
        }
        else
        {
            adminFlag = 'N';
        }
    }
}
