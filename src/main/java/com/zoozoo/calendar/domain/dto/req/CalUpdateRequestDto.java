package com.zoozoo.calendar.domain.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zoozoo.common.enumeration.CalendarType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@ApiModel(value = "캘린더 수정 요청 모델")
public class CalUpdateRequestDto {
    @JsonIgnore
    private int userNo;
    @ApiModelProperty(example = "CalendarTest")
    private String calNm;
    @JsonIgnore
    private int type = CalendarType.PUBLIC.getCode();
    private int calNo;

    @Builder
    public CalUpdateRequestDto(int calNo, int userNo, String calNm)
    {
        this.calNo = calNo;
        this.userNo = userNo;
        this.calNm = calNm;
    }
}
