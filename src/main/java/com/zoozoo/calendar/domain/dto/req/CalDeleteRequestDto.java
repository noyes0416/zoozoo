package com.zoozoo.calendar.domain.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zoozoo.common.enumeration.CalendarType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@ApiModel(value = "캘린더 삭제 요청 모델")
public class CalDeleteRequestDto {
    @JsonIgnore
    private int userNo;
    @ApiModelProperty(example = "1")
    private int calNo;
    @JsonIgnore
    private int type = CalendarType.PUBLIC.getCode();

    @Builder
    public CalDeleteRequestDto(int userNo, int calNo)
    {
        this.userNo = userNo;
        this.calNo = calNo;
    }
}
