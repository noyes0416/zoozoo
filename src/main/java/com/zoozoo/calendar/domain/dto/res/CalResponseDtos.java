package com.zoozoo.calendar.domain.dto.res;

import com.zoozoo.calendar.domain.dto.req.CalRequestDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@ApiModel(value = "캘린더 조회 응답 모델")
public class CalResponseDtos {
    @ApiModelProperty(example = "1")
    private int calNo;
    @Setter
    @ApiModelProperty(example = "Test Calendar")
    private String calNm;
    @ApiModelProperty(example = "100")
    private String filter;
    @ApiModelProperty(example = "2022")
    private String year;
    @ApiModelProperty(example = "06")
    private String month;
    private List<CalResponseDto> days;

    public CalResponseDtos(List<CalResponseDto> days, CalRequestDto requestDto)
    {
        this.calNo = requestDto.getCalNo();
        this.filter = requestDto.getFilter();
        this.year = requestDto.getYear();
        this.month = requestDto.getMonth();
        this.days = days;
    }

    public void setCalNm(String calNm) {
        this.calNm = calNm;
    }
}
