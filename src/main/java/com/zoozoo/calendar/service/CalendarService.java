package com.zoozoo.calendar.service;

import com.zoozoo.calendar.domain.dto.req.*;
import com.zoozoo.calendar.domain.dto.res.CalListResponseDto;
import com.zoozoo.calendar.domain.dto.res.CalResponseDto;
import com.zoozoo.calendar.domain.dto.res.CalResponseDtos;
import com.zoozoo.common.domain.Result;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CalendarService {
    List<CalListResponseDto> selectCalendarList(int userNo);
    Result insertCalendar(CalCreateRequestDto calCreateRequestDto);
    int selectCalCnt(int userNo);
    Result deleteCalendar(CalDeleteRequestDto calDeleteRequestDto);
    int isValidCal(int type, int state, int calNo);
    Result updateCalendar(CalUpdateRequestDto requestDto);
    int selectCalAdmin(int calNo);
    Result updateCalAdmin(CalAdminUpdateRequestDto requestDto);
}
