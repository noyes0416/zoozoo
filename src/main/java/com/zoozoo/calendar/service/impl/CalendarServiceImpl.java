package com.zoozoo.calendar.service.impl;

import com.zoozoo.calendar.domain.dto.req.CalAdminUpdateRequestDto;
import com.zoozoo.calendar.domain.dto.req.CalCreateRequestDto;
import com.zoozoo.calendar.domain.dto.req.CalDeleteRequestDto;
import com.zoozoo.calendar.domain.dto.req.CalUpdateRequestDto;
import com.zoozoo.calendar.domain.dto.res.CalListResponseDto;
import com.zoozoo.calendar.persistence.CalendarMapper;
import com.zoozoo.calendar.service.CalendarService;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.CalendarType;
import com.zoozoo.common.enumeration.PermissionType;
import com.zoozoo.common.enumeration.PermitState;
import com.zoozoo.common.enumeration.State;
import com.zoozoo.permission.domain.PermissionRequestDto;
import com.zoozoo.permission.service.PermissionService;
import com.zoozoo.shareGroup.service.ShareGroupService;
import com.zoozoo.user.domain.ValidUserRequestDTO;
import com.zoozoo.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CalendarServiceImpl implements CalendarService {
    private final CalendarMapper calendarMapper;
    private final UserService userService;
    private final ShareGroupService shareGroupService;
    private final PermissionService permissionService;

    private String methodName;

    public List<CalListResponseDto> selectCalendarList(int userNo) {
        return calendarMapper.selectCalendarList(userNo)
                .stream().map(CalListResponseDto::new)
                .collect(Collectors.toList());
    }

    @Transactional(rollbackFor = Exception.class)
    public Result insertCalendar(CalCreateRequestDto requestDto)
    {
        methodName = "[insertCalendar]";
        try
        {
            ValidUserRequestDTO isValidUser = ValidUserRequestDTO.builder()
                    .userNo(requestDto.getUserNo())
                    .build();

            if(userService.isValidUser(isValidUser) > 0)
            {
                calendarMapper.insertCalendar(requestDto);

                int calNo = selectFinalCalNo(requestDto);

                Result insertShareGroup = shareGroupService.insertShareGroup(requestDto.getUserNo(), calNo);
                if(insertShareGroup.getCode() != 200)
                {
                    return Result.builder()
                            .code(insertShareGroup.getCode())
                            .message(insertShareGroup.getMessage())
                            .build();
                }

                PermissionRequestDto permissionDto = new PermissionRequestDto(PermissionType.CALENDAR.getCode(), calNo, "CRUD", requestDto.getUserNo(), requestDto.getUserNo(), PermitState.PERMIT.getCode());
                Result insertPermission = permissionService.insertPermission(permissionDto);
                if(insertPermission.getCode() != 200)
                {
                    return Result.builder()
                            .code(insertPermission.getCode())
                            .message(insertPermission.getMessage())
                            .build();
                }

                return Result.builder()
                        .code(200)
                        .message(CommonMessage.OK_MESSAGE)
                        .build();
            }
            else
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : inValid userNo")
                        .build();
            }
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Result updateCalendar(CalUpdateRequestDto requestDto)
    {
        methodName = "[updateCalendar]";
        try
        {
            if(isValidCal(CalendarType.PUBLIC.getCode(), State.Normal.getCode(), requestDto.getCalNo()) == 0)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : InValid CalNo")
                        .build();
            }
            if(selectCalAdmin(requestDto.getCalNo()) != requestDto.getUserNo())
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Only calendar administrators can do this.")
                        .build();
            }

            if(calendarMapper.isValidAdmin(requestDto) > 0)
            {
                calendarMapper.updateCalendar(requestDto);
                return Result.builder()
                        .code(200)
                        .message(CommonMessage.OK_MESSAGE)
                        .build();
            }
            else
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Unauthorized users")
                        .build();
            }

        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Result deleteCalendar(CalDeleteRequestDto requestDto)
    {
        methodName = "[deleteCalendar]";
        try
        {

            if(isValidCal(CalendarType.PUBLIC.getCode(), State.Normal.getCode(), requestDto.getCalNo()) == 0)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : InValid CalNo")
                        .build();
            }
            if(selectCalAdmin(requestDto.getCalNo()) != requestDto.getUserNo())
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Only calendar administrators can do this.")
                        .build();
            }

            if(calendarMapper.isValidCal(CalendarType.PUBLIC.getCode(), State.Normal.getCode(), requestDto.getCalNo()) == 0)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : inValid calNo")
                        .build();
            }
            else if(calendarMapper.isValidCal(CalendarType.PUBLIC.getCode(), State.AbNormal.getCode(), requestDto.getCalNo()) != 0)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : already deleted calNo")
                        .build();
            }
            else
            {
                calendarMapper.deleteCalendar(requestDto);

                return Result.builder()
                        .code(204)
                        .message(CommonMessage.OK_MESSAGE)
                        .build();
            }
        }
        catch(Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public Result updateCalAdmin(CalAdminUpdateRequestDto requestDto)
    {
        methodName = "[updateCalAdmin]";
        try
        {
            if(isValidCal(CalendarType.PUBLIC.getCode(), State.Normal.getCode(), requestDto.getCalNo()) == 0)
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : InValid CalNo")
                        .build();
            }
            if(selectCalAdmin(requestDto.getCalNo()) != requestDto.getAdminUserNo())
            {
                return Result.builder()
                        .code(404)
                        .message(methodName + " : Only calendar administrators can do this.")
                        .build();
            }

            calendarMapper.updateCalAdmin(requestDto);
            return Result.builder()
                    .code(200)
                    .message(CommonMessage.OK_MESSAGE)
                    .build();
        }
        catch (Exception e)
        {
            return Result.builder()
                    .code(500)
                    .message(methodName + e.getMessage())
                    .build();
        }
    }

    public int selectFinalCalNo(CalCreateRequestDto requestDto)
    {
        return Optional.ofNullable(calendarMapper.selectFinalCalNo(requestDto)).orElseGet(()->{return 0;});
    }

    public int selectCalCnt(int userNo)
    {
        return Optional.ofNullable(calendarMapper.selectCalCnt(userNo)).orElseGet(()->{return 0;});
    }

    public int isValidCal(int type, int state, int calNo)
    {
        return calendarMapper.isValidCal(type, state, calNo);
    }

    public int selectCalAdmin(int calNo)
    {
        return Optional.ofNullable(calendarMapper.selectCalAdmin(calNo)).orElseGet(()->{return 0;});
    }
}
