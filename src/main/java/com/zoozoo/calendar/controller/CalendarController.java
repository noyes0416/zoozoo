package com.zoozoo.calendar.controller;

import com.zoozoo.calendar.domain.dto.req.*;
import com.zoozoo.calendar.domain.dto.res.CalListResponseDto;
import com.zoozoo.calendar.domain.dto.res.CalResponseDtos;
import com.zoozoo.calendar.service.CalendarService;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.record.service.RecordService;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

@RequestMapping("/api/v1/calendar")
@RequiredArgsConstructor
@RestController
@Api(description = "캘린더")
public class CalendarController {

    private final CalendarService calendarService;
    private final RecordService recordService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "캘린더 목록 조회", response = CalListResponseDto.class, responseContainer = "List")
    public @ResponseBody ResponseEntity<List<CalListResponseDto>> selectCalendarList(HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        List<CalListResponseDto> responseDto = calendarService.selectCalendarList(userSession.getUserNo());
        return ResponseEntity.ok().body(responseDto);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "캘린더 생성", response = Result.class)
    public @ResponseBody ResponseEntity<Result> insertCalendar(@RequestBody CalCreateRequestDto calCreateRequestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        CalCreateRequestDto requestDto = new CalCreateRequestDto(userSession.getUserNo(), calCreateRequestDto.getCalNm());
        return ResponseEntity.ok().body(calendarService.insertCalendar(requestDto));
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ApiOperation(value = "캘린더 삭제", response = Result.class)
    public @ResponseBody ResponseEntity<Result> deleteCalendar(@RequestBody CalDeleteRequestDto calDeleteRequestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        CalDeleteRequestDto requestDto = new CalDeleteRequestDto(userSession.getUserNo(), calDeleteRequestDto.getCalNo());
        return ResponseEntity.ok().body(calendarService.deleteCalendar(requestDto));
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "캘린더 월별 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "calNo", value = "캘린더 번호", required = true),
            @ApiImplicitParam(name = "ym", value = "연월(yyyy-MM)", required = true),
            @ApiImplicitParam(name = "filter", value = "필터(selectionNos)")
    })
    public @ResponseBody ResponseEntity<CalResponseDtos> selectCalendar(int calNo, String ym, String filter, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        CalRequestDto calRequestDto = new CalRequestDto(userSession.getUserNo(), calNo, ym, filter);

        if(!recordService.selectRecordList(calRequestDto).equals(null))
        {
            return ResponseEntity.ok().body(recordService.selectRecordList(calRequestDto));
        }
        else
        {
            return ResponseEntity.badRequest().body(null);
        }

    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ApiOperation(value = "캘린더명 수정", response = Result.class)
    public @ResponseBody ResponseEntity<Result> updateCalendar(@RequestBody CalUpdateRequestDto requestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        CalUpdateRequestDto calUpdateRequestDto = new CalUpdateRequestDto(requestDto.getCalNo(), userSession.getUserNo(), requestDto.getCalNm());
        return ResponseEntity.ok().body(calendarService.updateCalendar(calUpdateRequestDto));
    }

    @RequestMapping(value = "/admin", method = RequestMethod.PUT)
    @ApiOperation(value = "캘린더 관리자 변경", response = Result.class)
    public @ResponseBody ResponseEntity<Result> updateCalAdmin(@RequestBody CalAdminUpdateRequestDto requestDto, HttpServletRequest request)
    {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        CalAdminUpdateRequestDto calAdminUpdateRequestDto = new CalAdminUpdateRequestDto(userSession.getUserNo(), requestDto.getUserNo(), requestDto.getCalNo());
        return ResponseEntity.ok().body(calendarService.updateCalAdmin(calAdminUpdateRequestDto));
    }
}
