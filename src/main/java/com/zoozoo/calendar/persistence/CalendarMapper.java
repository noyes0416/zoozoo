package com.zoozoo.calendar.persistence;

import com.zoozoo.calendar.domain.Calendar;
import com.zoozoo.calendar.domain.dto.req.CalAdminUpdateRequestDto;
import com.zoozoo.calendar.domain.dto.req.CalCreateRequestDto;
import com.zoozoo.calendar.domain.dto.req.CalDeleteRequestDto;
import com.zoozoo.calendar.domain.dto.req.CalUpdateRequestDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CalendarMapper
{
    List<Calendar> selectCalendarList(int userNo);
    void insertCalendar(CalCreateRequestDto calCreateRequestDto);
    int selectCalCnt(int userNo);
    void insertShareGroup(int userNo, int calNo);
    void deleteCalendar(CalDeleteRequestDto calDeleteRequestDto);
    int isValidCal(int type, int state, int calNo);
    int selectFinalCalNo(CalCreateRequestDto requestDto);
    void updateCalendar(CalUpdateRequestDto requestDto);
    int isValidAdmin(CalUpdateRequestDto requestDto);
    int selectCalAdmin(int calNo);
    void updateCalAdmin(CalAdminUpdateRequestDto requestDto);
    String selectCalNm(int calNo);
}
