package com.zoozoo.user.service;

import com.zoozoo.common.domain.JWTTokenDTO;
import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.State;
import com.zoozoo.user.domain.*;
import com.zoozoo.user.persistence.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.zoozoo.common.domain.CommonMessage.OK_MESSAGE;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    public int isValidUser(ValidUserRequestDTO requestDTO) {

        return userMapper.isValidUser(requestDTO);
    }

    public int selectUserCountByEmail(JWTTokenDTO jwtTokenDTO) {
        return userMapper.selectUserCountByEmail(jwtTokenDTO);
    }

    public int selectUserCountByUserName(String userName) {
        return userMapper.selectUserCountByUserName(userName);
    }

    public UserSessionDTO selectUserInfoByTokenKey(JWTTokenDTO jwtTokenDTO) {
        return userMapper.selectUserInfoByTokenKey(jwtTokenDTO);
    }

    public UserInfoResponseDTO selectUserInfoByUserNo(int userNo) {
        return userMapper.selectUserInfoByUserNo(userNo);
    }

    public List<UserListResponseDTO> selectUserList(UserListRequestDTO requestDTO) {
        return userMapper.selectUserList(requestDTO);
    }

    public List<UserNameListResponseDTO> selectUserByUserName(String userName) {
        return userMapper.selectUserByUserName(userName);
    }

    public List<UserNameListResponseDTO> selectCalMemberByUserName(String userName) {
        return userMapper.selectCalMemberByUserName(userName);
    }

    public void insertUser(JWTTokenDTO jwtTokenDTO) {
        userMapper.insertUser(jwtTokenDTO);
    }

    public void updateUserJWTToken(JWTTokenDTO jwtTokenDTO) {
        userMapper.updateUserJWTToken(jwtTokenDTO);
    }

    public void deleteUserJWTToken(JWTTokenDTO jwtTokenDTO) {
        userMapper.deleteUserJWTToken(jwtTokenDTO);
    }

    public Result updateUserName(UpdateUserNameRequestDTO requestDTO) {
        userMapper.updateUserName(requestDTO);
        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }
}
