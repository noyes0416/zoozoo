package com.zoozoo.user.service;

import com.zoozoo.common.domain.JWTTokenDTO;
import com.zoozoo.common.domain.Result;
import com.zoozoo.user.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    int isValidUser(ValidUserRequestDTO requestDTO);

    int selectUserCountByEmail(JWTTokenDTO jwtTokenDTO);

    int selectUserCountByUserName(String userName);

    UserSessionDTO selectUserInfoByTokenKey(JWTTokenDTO jwtTokenDTO);

    UserInfoResponseDTO selectUserInfoByUserNo(int userNo);

    List<UserListResponseDTO> selectUserList(UserListRequestDTO requestDTO);

    List<UserNameListResponseDTO> selectUserByUserName(String userName);

    List<UserNameListResponseDTO> selectCalMemberByUserName(String userName);

    void insertUser(JWTTokenDTO jwtTokenDTO);

    void updateUserJWTToken(JWTTokenDTO jwtTokenDTO);

    void deleteUserJWTToken(JWTTokenDTO jwtTokenDTO);

    Result updateUserName(UpdateUserNameRequestDTO requestDTO);
}
