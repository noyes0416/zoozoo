package com.zoozoo.user.persistence;

import com.zoozoo.common.domain.JWTTokenDTO;
import com.zoozoo.user.domain.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    int isValidUser(ValidUserRequestDTO requestDTO);

    int selectUserCountByEmail(JWTTokenDTO jwtTokenDTO);

    int selectUserCountByUserName(String userName);

    UserSessionDTO selectUserInfoByTokenKey(JWTTokenDTO jwtTokenDTO);

    UserInfoResponseDTO selectUserInfoByUserNo(int userNo);

    List<UserListResponseDTO> selectUserList(UserListRequestDTO requestDTO);

    List<UserNameListResponseDTO> selectUserByUserName(String userName);

    List<UserNameListResponseDTO> selectCalMemberByUserName(String userName);

    void insertUser(JWTTokenDTO jwtTokenDTO);

    void updateUserJWTToken(JWTTokenDTO jwtTokenDTO);

    void deleteUserJWTToken(JWTTokenDTO jwtTokenDTO);

    void updateUserName(UpdateUserNameRequestDTO requestDTO);
}
