package com.zoozoo.user.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class UpdateUserNameRequestDTO {
    private String newUserName;
    @ApiModelProperty(hidden = true)
    private int userNo;

    public UpdateUserNameRequestDTO() {}

    @Builder
    public UpdateUserNameRequestDTO(final String newUserName) {
        this.newUserName = newUserName;
    }

    public UpdateUserNameRequestDTO setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
