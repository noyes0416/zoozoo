package com.zoozoo.user.domain;

import lombok.Getter;

@Getter
public class UserNameListResponseDTO {
    private int userNo;
    private String userName;
    private int state;
    private String regDt;
}
