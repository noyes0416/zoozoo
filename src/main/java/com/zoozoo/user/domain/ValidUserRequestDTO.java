package com.zoozoo.user.domain;

import com.zoozoo.common.enumeration.State;
import lombok.Builder;
import lombok.Getter;

@Getter
public class ValidUserRequestDTO {
    private final int userNo;
    private final int state;

    @Builder
    public ValidUserRequestDTO(final int userNo) {
        this.userNo = userNo;
        this.state = State.Normal.getCode();
    }
}
