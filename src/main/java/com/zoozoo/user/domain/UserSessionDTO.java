package com.zoozoo.user.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
public class UserSessionDTO {
    private int userNo;
    private String email;
    private int snsType;
    private String tokenKey;
    private String tokenValue;
}
