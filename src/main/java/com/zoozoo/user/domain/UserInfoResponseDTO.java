package com.zoozoo.user.domain;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class UserInfoResponseDTO {
    private int userNo;
    private String userName;
    private String email;
    private int snsType;
    private LocalDateTime regDt;

    @Builder
    public UserInfoResponseDTO(int userNo, String userName, String email, int snsType, LocalDateTime regDt) {
        this.userNo = userNo;
        this.userName = userName;
        this.email = email;
        this.snsType = snsType;
        this.regDt = regDt;
    }
}
