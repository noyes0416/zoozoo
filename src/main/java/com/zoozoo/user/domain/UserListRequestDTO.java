package com.zoozoo.user.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
public class UserListRequestDTO {
    private final int state;

    @Builder
    public UserListRequestDTO (final int state) {
        this.state = state;
    }
}
