package com.zoozoo.user.domain;

import lombok.Getter;

@Getter
public class UserListResponseDTO {
    private int userNo;
    private String userName;
    private String regDt;
}
