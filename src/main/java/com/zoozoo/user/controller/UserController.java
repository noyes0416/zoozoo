package com.zoozoo.user.controller;

import com.zoozoo.user.domain.UpdateUserNameRequestDTO;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.user.domain.*;
import com.zoozoo.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
@RestController
@Api(description = "유저")
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "유저정보 조회")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<UserInfoResponseDTO> getUserInfo(HttpServletRequest request) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        int userNo = userSession.getUserNo();

        UserInfoResponseDTO userInfo = userService.selectUserInfoByUserNo(userNo);

        return ResponseEntity.ok().body(userInfo);
    }

    @ApiOperation(value = "유저명 중복체크")
    @RequestMapping(value = "/checkName", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Character>> checkUserName(@RequestParam String userName) {
        Map<String, Character> map = new HashMap<>();
        int count = userService.selectUserCountByUserName(userName);

        map.put("flag", (count == 1) ? 'Y' : 'N');
        return ResponseEntity.ok().body(map);
    }

    @ApiOperation(value = "유저 리스트 조회")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<UserListResponseDTO>> selectUserList(@RequestParam(defaultValue = "0") int state) {
        UserListRequestDTO requestDTO = UserListRequestDTO.builder()
                .state(state)
                .build();

        return ResponseEntity.ok().body(userService.selectUserList(requestDTO));
    }

    @ApiOperation(value = "주주회원 닉네임 조회 (Like조회)")
    @RequestMapping(value = "/userNameList", method = RequestMethod.GET)
    public ResponseEntity<List<UserNameListResponseDTO>> selectUserByUserName(@RequestParam String userName) {
        return ResponseEntity.ok().body(userService.selectUserByUserName(userName));
    }

    @ApiOperation(value = "캘린더 멤버 닉네임으로 유저 조회 (== 조회)")
    @RequestMapping(value = "/calMemberNameList", method = RequestMethod.GET)
    public ResponseEntity<List<UserNameListResponseDTO>> selectCalMemberByUserName(@RequestParam String userName) {
        return ResponseEntity.ok().body(userService.selectCalMemberByUserName(userName));
    }

    @ApiOperation(value = "유저 닉네임 변경")
    @RequestMapping(value = "/updateUserName", method = RequestMethod.PUT)
    public ResponseEntity<Result> updateUserName(HttpServletRequest request, @RequestBody UpdateUserNameRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setUserNo(userSession.getUserNo());

        Result result = userService.updateUserName(requestDTO);

        return ResponseEntity.ok().body(result);
    }
}
