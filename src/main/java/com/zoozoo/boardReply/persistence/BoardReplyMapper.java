package com.zoozoo.boardReply.persistence;

import com.zoozoo.boardReply.domain.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BoardReplyMapper {
    int selectBoardReplyCount(int boardNo);

    List<ReplyListResponseDTO> selectFirstDepthReplyList(ReplyListRequestDTO requestDTO);

    List<ReplyListResponseDTO> selectSecondDepthReplyList(ReplyListRequestDTO requestDTO);

    int insertReply(InsertReplyRequestDTO requestDTO);

    void insertReplyText(InsertReplyRequestDTO requestDTO);

    void updateReply(UpdateReplyRequestDTO requestDTO);

    void updateReplyText(UpdateReplyRequestDTO requestDTO);

    void deleteReplyDisplayN(DeleteReplyRequestDTO requestDTO);

    void updateReplyDisplayN(int replyNo);

    int selectReplyWriterNo(int replyNo);
}
