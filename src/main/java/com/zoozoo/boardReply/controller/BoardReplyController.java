package com.zoozoo.boardReply.controller;

import com.zoozoo.boardReply.domain.DeleteReplyRequestDTO;
import com.zoozoo.boardReply.domain.InsertReplyRequestDTO;
import com.zoozoo.boardReply.domain.UpdateReplyRequestDTO;
import com.zoozoo.boardReply.service.BoardReplyService;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping(value = "/api/v1/reply")
@RequiredArgsConstructor
@RestController
@Api(description = "커뮤니티 댓글")
public class BoardReplyController {

    private final BoardReplyService boardReplyService;

    @ApiOperation(value = "커뮤니티 댓글, 대댓글 등록")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Result> insertReply(HttpServletRequest request, @RequestBody InsertReplyRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setUserNo(userSession.getUserNo());

        Result result = boardReplyService.insertReply(requestDTO);

        return ResponseEntity.ok().body(result);
    }

    @ApiOperation(value = "커뮤니티 댓글, 대댓글 수정")
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<Result> updateReply(HttpServletRequest request, @RequestBody UpdateReplyRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setUserNo(userSession.getUserNo());

        Result result = boardReplyService.updateReply(requestDTO);

        return ResponseEntity.ok().body(result);
    }

    @ApiOperation(value = "커뮤니티 댓글, 대댓글 삭제")
    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<Result> deleteReply(HttpServletRequest request, @RequestBody DeleteReplyRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setUserNo(userSession.getUserNo());

        Result result = boardReplyService.deleteReply(requestDTO);

        return ResponseEntity.ok().body(result);
    }
}
