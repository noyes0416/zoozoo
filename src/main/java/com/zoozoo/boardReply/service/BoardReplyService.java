package com.zoozoo.boardReply.service;

import com.zoozoo.boardReply.domain.*;
import com.zoozoo.common.domain.Result;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BoardReplyService {

    int selectReplyTotalCount(int boardNo);

    List<ReplyListResponseDTO> selectAllDepthReplyList(ReplyListRequestDTO requestDTO);

    Result insertReply(InsertReplyRequestDTO requestDTO);

    Result updateReply(UpdateReplyRequestDTO requestDTO);

    Result deleteReply(DeleteReplyRequestDTO requestDTO);
}
