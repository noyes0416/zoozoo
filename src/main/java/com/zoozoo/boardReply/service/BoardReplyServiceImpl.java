package com.zoozoo.boardReply.service;

import com.zoozoo.boardReply.domain.*;
import com.zoozoo.boardReply.persistence.BoardReplyMapper;
import com.zoozoo.common.domain.Result;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.zoozoo.common.domain.CommonMessage.OK_MESSAGE;

@Service
@RequiredArgsConstructor
public class BoardReplyServiceImpl implements BoardReplyService {

    private final BoardReplyMapper boardReplyMapper;

    public int selectReplyTotalCount(int boardNo) {
        return boardReplyMapper.selectBoardReplyCount(boardNo);
    }

    public List<ReplyListResponseDTO> selectAllDepthReplyList(ReplyListRequestDTO request) {
        List<ReplyListResponseDTO> originReplyList = boardReplyMapper.selectFirstDepthReplyList(request);

        originReplyList.forEach(originReply -> selectSecondDepthReplyList(originReply, request.getUserNo()));

        return originReplyList;
    }

    private List<ReplyListResponseDTO> selectSecondDepthReplyList(ReplyListResponseDTO originReply, int userNo) {
        ReplyListRequestDTO request = ReplyListRequestDTO.builder()
                .userNo(userNo)
                .boardNo(originReply.getBoardNo())
                .build()
                .setOriginReplyNo(originReply.getReplyNo());

        List<ReplyListResponseDTO> secondDepthReplyList = boardReplyMapper.selectSecondDepthReplyList(request);

        originReply.setSecondDepthReplyList(secondDepthReplyList);

        return secondDepthReplyList;
    }

    @Transactional(rollbackFor = Exception.class)
    public Result insertReply(InsertReplyRequestDTO requestDTO) {
        boardReplyMapper.insertReply(requestDTO);
        int replyNo = requestDTO.getReplyNo();
        boardReplyMapper.insertReplyText(requestDTO.setReplyNo(replyNo));

        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }

    @Transactional(rollbackFor = Exception.class)
    public Result updateReply(UpdateReplyRequestDTO requestDTO) {
        boardReplyMapper.updateReply(requestDTO);
        boardReplyMapper.updateReplyText(requestDTO);

        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }

    public Result deleteReply(DeleteReplyRequestDTO requestDTO) {
        boardReplyMapper.deleteReplyDisplayN(requestDTO);

        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }
}
