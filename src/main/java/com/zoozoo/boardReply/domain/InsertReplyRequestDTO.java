package com.zoozoo.boardReply.domain;

import com.zoozoo.common.enumeration.TextType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class InsertReplyRequestDTO {
    private final int boardNo;
    @ApiModelProperty(hidden = true)
    private int replyNo;
    private final int originReplyNo;
    @ApiModelProperty(hidden = true)
    private int userNo;
    private final String content;
    @ApiModelProperty(hidden = true)
    private final int replyType;

    @Builder
    public InsertReplyRequestDTO (final int boardNo, final int originReplyNo, final String content) {
        this.boardNo = boardNo;
        this.originReplyNo = originReplyNo;
        this.content = content;
        this.replyType = TextType.REPLY.getCode();
    }

    public InsertReplyRequestDTO setReplyNo(final int replyNo) {
        this.replyNo = replyNo;
        return this;
    }

    public InsertReplyRequestDTO setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
