package com.zoozoo.boardReply.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

import java.util.List;

@Getter
public class ReplyListResponseDTO {
    private int boardNo;
    private int replyNo;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int originReplyNo;
    private int userNo;
    private String userName;
    private String sessionUserLikeYN;
    private String displayYN;
    private int likeCount;
    private String content;
    private String regDt;
    private List<ReplyListResponseDTO> secondDepthReplyList;

    public void setSecondDepthReplyList(List<ReplyListResponseDTO> list) {
        this.secondDepthReplyList = list;
    }
}
