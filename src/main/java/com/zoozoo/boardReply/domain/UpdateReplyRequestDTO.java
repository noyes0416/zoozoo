package com.zoozoo.boardReply.domain;

import com.zoozoo.common.enumeration.TextType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class UpdateReplyRequestDTO {
    private final int replyNo;
    @ApiModelProperty(hidden = true)
    private int userNo;
    private final String content;
    private final String displayYN;
    @ApiModelProperty(hidden = true)
    private final int replyType;

    @Builder
    public UpdateReplyRequestDTO(final int replyNo, final String content, final String displayYN) {
        this.replyNo = replyNo;
        this.content = content;
        this.displayYN = displayYN;
        this.replyType = TextType.REPLY.getCode();
    }

    public UpdateReplyRequestDTO setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
