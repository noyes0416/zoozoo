package com.zoozoo.boardReply.domain;

import com.zoozoo.common.enumeration.TextType;
import lombok.Builder;
import lombok.Getter;

@Getter
public class ReplyListRequestDTO {
    private final int userNo;
    private final int boardNo;
    private final int replyType;
    private int originReplyNo;

    @Builder
    public ReplyListRequestDTO(final int userNo, final int boardNo, final int replyType) {
        this.userNo = userNo;
        this.boardNo = boardNo;
        this.replyType = TextType.REPLY.getCode();
    }

    public ReplyListRequestDTO setOriginReplyNo(final int originReplyNo) {
        this.originReplyNo = originReplyNo;
        return this;
    }
}
