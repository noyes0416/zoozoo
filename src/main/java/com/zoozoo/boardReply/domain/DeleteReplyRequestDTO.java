package com.zoozoo.boardReply.domain;

import com.zoozoo.common.domain.CommonMessage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class DeleteReplyRequestDTO {
    private final int replyNo;
    @ApiModelProperty(hidden = true)
    private int userNo;
    @ApiModelProperty(hidden = true)
    private final String displayYN;

    @Builder
    public DeleteReplyRequestDTO(final int replyNo) {
        this.replyNo = replyNo;
        this.displayYN = CommonMessage.COMMON_N;
    }

    public DeleteReplyRequestDTO setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
