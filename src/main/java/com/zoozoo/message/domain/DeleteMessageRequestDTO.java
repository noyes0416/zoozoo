package com.zoozoo.message.domain;

import com.zoozoo.common.domain.CommonMessage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class DeleteMessageRequestDTO {
    private String messageNos;
    @ApiModelProperty(hidden = true)
    private List<String> messageNoList;
    @ApiModelProperty(hidden = true)
    private final String displayN;
    @ApiModelProperty(hidden = true)
    private int userNo;

    public DeleteMessageRequestDTO() {
        this.displayN = CommonMessage.COMMON_N;
    }

    @Builder
    public DeleteMessageRequestDTO(final String messageNos) {
        this.messageNos = messageNos;
        this.displayN = CommonMessage.COMMON_N;
    }

    public DeleteMessageRequestDTO setMessageNoList() {
        this.messageNoList = Arrays.stream(this.messageNos.split("\\^"))
                .collect(Collectors.toList());
        return this;
    }

    public DeleteMessageRequestDTO setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
