package com.zoozoo.message.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
public class MessageDetailRequestDTO {
    private final int messageNo;

    @Builder
    public MessageDetailRequestDTO (final int messageNo) {
        this.messageNo = messageNo;
    }
}
