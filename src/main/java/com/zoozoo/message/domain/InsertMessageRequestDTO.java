package com.zoozoo.message.domain;

import com.zoozoo.common.enumeration.TextType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class InsertMessageRequestDTO {
    @ApiModelProperty(hidden = true)
    private int senderNo;
    private final int receiverNo;
    private final String content;
    @ApiModelProperty(hidden = true)
    private final int messageType;
    @ApiModelProperty(hidden = true)
    private int messageNo;

    @Builder
    public InsertMessageRequestDTO (final int receiverNo, final String content) {
        this.receiverNo = receiverNo;
        this.content = content;
        this.messageType = TextType.MESSAGE.getCode();
    }

    public InsertMessageRequestDTO setSenderNo(final int userNo) {
        this.senderNo = userNo;
        return this;
    }

    public InsertMessageRequestDTO setMessageNo(final int messageNo) {
        this.messageNo = messageNo;
        return this;
    }
}
