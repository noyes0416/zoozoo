package com.zoozoo.message.domain;

import com.zoozoo.common.domain.CommonMessage;
import lombok.Getter;

@Getter
public class MessageSelectResponseDTO {
    private int messageNo;
    private int senderNo;
    private String senderName;
    private String content;
    private String readYN;
    private String regDt;

    public MessageSelectResponseDTO setReadY() {
        this.readYN = CommonMessage.COMMON_Y;
        return this;
    }
}