package com.zoozoo.message.domain;

import com.zoozoo.common.domain.CommonMessage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class ReadMessageRequestDTO {
    private int messageNo;
    @ApiModelProperty(hidden = true)
    private final String readY;

    public ReadMessageRequestDTO() {
        this.readY = CommonMessage.COMMON_Y;
    }

    @Builder
    public ReadMessageRequestDTO (final int messageNo) {
        this.messageNo = messageNo;
        this.readY = CommonMessage.COMMON_Y;
    }
}
