package com.zoozoo.message.domain;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.enumeration.TextType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
public class MessageListRequestDTO {
    private final int type;
    @ApiModelProperty(hidden = true)
    private int userNo;
    @ApiModelProperty(hidden = true)
    private final int messageType;
    @ApiModelProperty(hidden = true)
    private final String displayY;

    @Builder
    public MessageListRequestDTO(final int type) {
        this.type = type;
        this.messageType = TextType.MESSAGE.getCode();
        this.displayY = CommonMessage.COMMON_Y;
    }

    public MessageListRequestDTO setUserNo(final int userNo) {
        this.userNo = userNo;
        return this;
    }
}
