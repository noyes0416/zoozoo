package com.zoozoo.message.service;

import com.zoozoo.common.domain.Result;
import com.zoozoo.message.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MessageService {

    List<MessageSelectResponseDTO> selectMessageList(MessageListRequestDTO requestDTO);

    MessageSelectResponseDTO selectMessageDetail(MessageDetailRequestDTO requestDTO);

    Result insertMessage(InsertMessageRequestDTO requestDTO);

    Result deleteMessage(DeleteMessageRequestDTO requestDTO);
}
