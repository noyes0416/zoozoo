package com.zoozoo.message.service;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.message.domain.*;
import com.zoozoo.message.persistence.MessageMapper;
import com.zoozoo.text.domain.TextCURequestDto;
import com.zoozoo.text.persistence.TextMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.zoozoo.common.domain.CommonMessage.OK_MESSAGE;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {
    private final MessageMapper messageMapper;
    private final TextMapper textMapper;

    public List<MessageSelectResponseDTO> selectMessageList(MessageListRequestDTO requestDTO) {
        return messageMapper.selectMessageList(requestDTO);
    }

    public MessageSelectResponseDTO selectMessageDetail(MessageDetailRequestDTO requestDTO) {
        MessageSelectResponseDTO messageDetail = messageMapper.selectMessageDetail(requestDTO);

        if (messageDetail.getReadYN().equals(CommonMessage.COMMON_N)) {
            this.readMessage(messageDetail);
        }

        return messageDetail;
    }

    private MessageSelectResponseDTO readMessage(MessageSelectResponseDTO messageDetail) {
        ReadMessageRequestDTO readRequestDTO = ReadMessageRequestDTO.builder()
                .messageNo(messageDetail.getMessageNo())
                .build();

        messageMapper.updateMessageReadY(readRequestDTO);

        return messageDetail.setReadY();
    }

    @Transactional(rollbackFor = Exception.class)
    public Result insertMessage(InsertMessageRequestDTO requestDTO) {
        messageMapper.insertMessage(requestDTO);
        requestDTO.setMessageNo(requestDTO.getMessageNo());

        TextCURequestDto textRequest = TextCURequestDto.builder()
                .type(requestDTO.getMessageType())
                .typeValue(requestDTO.getMessageNo())
                .build()
                .setUserNo(requestDTO.getSenderNo())
                .setContent(requestDTO.getContent());

        textMapper.insertText(textRequest);
        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }

    public Result deleteMessage(DeleteMessageRequestDTO requestDTO) {
        messageMapper.updateMessageDisplayN(requestDTO);
        return Result.builder().code(HttpStatus.OK.value()).message(OK_MESSAGE).build();
    }
}
