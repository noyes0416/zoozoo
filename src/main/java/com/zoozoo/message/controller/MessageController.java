package com.zoozoo.message.controller;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.message.domain.*;
import com.zoozoo.message.service.MessageService;
import com.zoozoo.user.domain.UserSessionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping(value = "/api/v1/message")
@RequiredArgsConstructor
@RestController
@Api(description = "메세지")
public class MessageController {

    private final MessageService messageService;

    @ApiOperation(value = "메세지 리스트")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "메세지 타입 (1: 받은 메세지, 2: 보낸 메세지)", required = true)
    })
    public ResponseEntity<List<MessageSelectResponseDTO>> messageList(HttpServletRequest request, @RequestParam int type) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        MessageListRequestDTO requestDTO = MessageListRequestDTO.builder()
                .type(type)
                .build().setUserNo(userSession.getUserNo());

        List<MessageSelectResponseDTO> messageList = messageService.selectMessageList(requestDTO);

        return ResponseEntity.ok().body(messageList);
    }

    @ApiOperation(value = "메세지 상세 내용")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "messageNo", value = "메세지 번호", required = true)
    })
    public ResponseEntity<MessageSelectResponseDTO> messageDetail(HttpServletRequest request, @RequestParam int messageNo) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        MessageDetailRequestDTO requestDTO = MessageDetailRequestDTO.builder()
                .messageNo(messageNo)
                .build();

        MessageSelectResponseDTO message = messageService.selectMessageDetail(requestDTO);

        return ResponseEntity.ok().body(message);
    }

    @ApiOperation(value = "메세지 작성")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Result> insertMessage(HttpServletRequest request, @RequestBody InsertMessageRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setSenderNo(userSession.getUserNo());

        Result result = messageService.insertMessage(requestDTO);

        return ResponseEntity.ok().body(result);
    }

    @ApiOperation(value = "메세지 삭제")
    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<Result> deleteMessage(HttpServletRequest request, @RequestBody DeleteMessageRequestDTO requestDTO) {
        UserSessionDTO userSession = (UserSessionDTO) request.getAttribute(CommonMessage.USER_SESSION);
        requestDTO.setMessageNoList().setUserNo(userSession.getUserNo());

        Result result = messageService.deleteMessage(requestDTO);

        return ResponseEntity.ok().body(result);
    }
}
