package com.zoozoo.message.persistence;

import com.zoozoo.message.domain.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MessageMapper {

    List<MessageSelectResponseDTO> selectMessageList(MessageListRequestDTO requestDTO);

    MessageSelectResponseDTO selectMessageDetail(MessageDetailRequestDTO requestDTO);

    int insertMessage(InsertMessageRequestDTO requestDTO);

    void updateMessageDisplayN(DeleteMessageRequestDTO requestDTO);

    void updateMessageReadY(ReadMessageRequestDTO requestDTO);
}
