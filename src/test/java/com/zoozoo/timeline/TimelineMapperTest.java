package com.zoozoo.timeline;

import com.zoozoo.timeline.domain.MapperResponseDTO;
import com.zoozoo.timeline.persistence.TimelineMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TimelineMapperTest {

    @Autowired
    private TimelineMapper timelineMapper;

    private final int USER_NO_1 = 1;

    @Test
    public void selectCalendarList_Test() {
        List<MapperResponseDTO> list = timelineMapper.selectCalendarList(USER_NO_1);
        MapperResponseDTO firstList = list.get(0);

        assertThat(firstList.getContentType()).isNotNull();
    }

    @Test
    public void selectBestBoardList_Test() {
        List<MapperResponseDTO> list = timelineMapper.selectBestBoardList();
        MapperResponseDTO firstList = list.get(0);

        assertThat(firstList.getContentType()).isNotNull();
    }

    @Test
    public void selectCommunityActiveList_Test() {
        List<MapperResponseDTO> list = timelineMapper.selectCommunityActiveList(USER_NO_1);
        MapperResponseDTO firstList = list.get(0);

        assertThat(firstList.getContentType()).isNotNull();
    }
}
