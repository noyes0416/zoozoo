package com.zoozoo.timeline;

import com.zoozoo.timeline.domain.TimelineListResponseDTO;
import com.zoozoo.timeline.service.TimelineService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TimelineTest {

    private final int USER_NO_1 = 1;

    @Autowired
    private TimelineService timelineService;

    @Test
    public void selectTimelineList_Test() {
        List<TimelineListResponseDTO> timelineList = timelineService.selectTimelineList(USER_NO_1);

        assertThat(timelineList.get(0).getContentType()).isNotNull();
    }
}
