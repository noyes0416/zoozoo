package com.zoozoo.record;

import com.zoozoo.common.domain.Result;
import com.zoozoo.record.domain.dto.req.DrinkInfoDtlRequestDto;
import com.zoozoo.record.domain.dto.req.RecordCURequestDto;
import com.zoozoo.record.domain.dto.req.RecordRequestDto;
import com.zoozoo.record.domain.dto.res.RecordResponseDto;
import com.zoozoo.record.persistence.RecordMapper;
import com.zoozoo.record.service.RecordService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RecordTest
{
    @Autowired
    private RecordService recordService;
    private RecordMapper  recordMapper;

    private int calNo = 1;
    private int userNo = 2;
    private String YMD = "2022-05-01";

    @Test
    public void selectRecord()
    {
        RecordRequestDto request = new RecordRequestDto(calNo, YMD);
        RecordResponseDto response = recordService.selectRecord(request, userNo);
        assertThat(response.getYear()).isEqualTo("2022");
        assertThat(response.getMonth()).isEqualTo("05");
        assertThat(response.getDay()).isEqualTo("01");
    }

    @Test
    @Transactional
    public void CURecord()
    {
        String location = "jebuDo";
        String text = "UpdatedText by userNo:" + userNo;
        String imagePath = "TestPath";
        String sharedMemberNos = "1^2";
        int price = 5000;

        DrinkInfoDtlRequestDto dto = new DrinkInfoDtlRequestDto(1, "100", "오미자주");
        List<DrinkInfoDtlRequestDto> dtlRequestDtos = new ArrayList<>();
        dtlRequestDtos.add(dto);
        RecordCURequestDto requestDto = new RecordCURequestDto(userNo, calNo, YMD, price, location
                                                              ,sharedMemberNos, 'N', 'N', text, imagePath, dtlRequestDtos);
        Result result = recordService.registerRecord(requestDto);
        assertThat(result.getCode()).isEqualTo(200);

        RecordResponseDto response = recordService.selectRecord(new RecordRequestDto(calNo, YMD), userNo);
        assertThat(response.getDrinkInfo().getLocation()).isEqualTo(location);
        assertThat(response.getText()).isEqualTo(text);
        assertThat(response.getDrinkInfo().getSharedMemberNos()).isEqualTo(sharedMemberNos);
        assertThat(response.getDrinkInfo().getPrice()).isEqualTo(price);
//        assertThat(response.getImage()).isEqualTo(imagePath);
    }

//    @Test
//    public void deleteRecord()
//    {
//        Result result = recordService.deleteRecord(userNo, 1);
//        assertThat(result.getCode()).isEqualTo(200);
//    }
}
