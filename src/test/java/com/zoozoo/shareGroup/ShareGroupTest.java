package com.zoozoo.shareGroup;

import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.ShareGroupStateType;
import com.zoozoo.shareGroup.domain.dto.MemberReplyRequestDto;
import com.zoozoo.shareGroup.domain.dto.MemberRequestDto;
import com.zoozoo.shareGroup.service.ShareGroupService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ShareGroupTest {
    private int adminUserNo = 1;
    private int userNo = 7;
    private int calNo = 1;

    @Autowired
    private ShareGroupService shareGroupService;

    @Test
    @Transactional
    public void insertMember()
    {
        MemberRequestDto requestDto = new MemberRequestDto(adminUserNo, calNo, userNo);
        Result result = shareGroupService.insertMember(requestDto);

        assertThat(result.getCode()).isEqualTo(200);
        assertThat(shareGroupService.isValidMember(requestDto.getUserNo(), requestDto.getCalNo(), ShareGroupStateType.Invited.getCode())).isEqualTo(1);
    }

    @Test
    @Transactional
    public void updateMember()
    {
        MemberReplyRequestDto requestDto = new MemberReplyRequestDto(userNo, calNo, 'Y');
        Result result = shareGroupService.replyInvite(requestDto);

        assertThat(result.getCode()).isEqualTo(200);
        assertThat(shareGroupService.isValidMember(requestDto.getUserNo(), requestDto.getCalNo(), ShareGroupStateType.Normal.getCode())).isEqualTo(1);
    }

//    @Test
//    public void deleteMember()
//    {
//        MemberRequestDto requestDto = new MemberRequestDto(adminUserNo, calNo, userNo);
//        Result result = shareGroupService.deleteMember(requestDto);
//
//        assertThat(result.getCode()).isEqualTo(200);
//        assertThat(shareGroupService.isValidMember(requestDto.getUserNo(), requestDto.getCalNo(), ShareGroupStateType.Deleted.getCode())).isEqualTo(1);
//    }
}
