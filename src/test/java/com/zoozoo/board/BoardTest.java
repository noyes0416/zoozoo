package com.zoozoo.board;

import com.zoozoo.board.domain.*;
import com.zoozoo.board.persistence.BoardMapper;
import com.zoozoo.board.service.BoardService;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class BoardTest {

    private int USERNO = 1;
    private int BOARDNO = 1;
    private int SELECTIONTYPE_DRINK = 1;
    private int SELECTIONNO_BEER_LAGER = 121;
    private int SELECTIONNO_WINE_RED = 111;

    private int PAGE_NO = 0;
    private int PAGE_SIZE = 20;

    @Autowired
    private BoardService boardService;

    @Autowired
    private BoardMapper boardMapper;

    @Test
    public void getBoardList_Test() {
        BoardListRequestDTO requestDTO = BoardListRequestDTO.builder()
                .pageNo(PAGE_NO)
                .pageSize(PAGE_SIZE)
                .selectionType(SELECTIONTYPE_DRINK)
                .selectionNos(String.valueOf(SELECTIONNO_BEER_LAGER))
                .build().setUserNo(USERNO);

        List<BoardListResponseDTO> boardList = boardService.selectBoardList(requestDTO);

        assertThat(boardList.get(0).getUserNo()).isEqualTo(USERNO);
        assertThat(boardList.get(0).getSelectionNo()).isEqualTo(SELECTIONNO_BEER_LAGER);
    }

    @Test
    public void getBoardDetail_Test() {
        BoardDetailRequestDTO requestDTO = BoardDetailRequestDTO.builder()
                .userNo(USERNO)
                .boardNo(BOARDNO)
                .build();

        BoardDetailResponseDTO boardDetail = boardService.selectBoardDetail(requestDTO);

        assertThat(boardDetail.getUserNo()).isEqualTo(USERNO);
        assertThat(boardDetail.getBoardNo()).isEqualTo(BOARDNO);
        assertThat(boardDetail.getSelectionNo()).isEqualTo(SELECTIONNO_BEER_LAGER);
        assertThat(boardDetail.getViewCount()).isGreaterThan(0);
    }

    @Test
    @Transactional
    public void insertBoard_Test() {
        InsertBoardRequestDTO requestDTO = InsertBoardRequestDTO.builder()
                .selectionNo(SELECTIONNO_WINE_RED)
                .title("JUnit Test Title")
                .content("JUnit Test Content")
                .build().setUserNo(USERNO);

        Result result = boardService.insertBoard(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

    @Test
    @Transactional
    public void updateBoard_Test() {
        UpdateBoardRequestDTO requestDTO = UpdateBoardRequestDTO.builder()
                .boardNo(BOARDNO)
                .selectionNo(SELECTIONNO_WINE_RED)
                .title("JUnit Test Title UPDATE")
                .content("JUnit Test Content UPDATE")
                .displayYN(CommonMessage.COMMON_Y)
                .build().setUserNo(USERNO);

        Result result = boardService.updateBoard(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

    @Test
    @Transactional
    public void deleteBoard_Test() {
        DeleteBoardRequestDTO deleteRequestDTO = DeleteBoardRequestDTO.builder()
                .userNo(USERNO)
                .boardNo(BOARDNO)
                .build();

        boardService.deleteBoard(deleteRequestDTO);

        BoardDetailRequestDTO detailRequestDTO = BoardDetailRequestDTO.builder()
                .userNo(USERNO)
                .boardNo(BOARDNO)
                .build();

        BoardDetailResponseDTO boardDetail = boardMapper.selectBoardDetail(detailRequestDTO);

        assertThat(boardDetail).isNull();
    }

    @Test
    public void BoardListRequestDTO_setSelectionNosArray_Test() {
        BoardListRequestDTO requestDTO = BoardListRequestDTO.builder()
                .pageNo(PAGE_NO)
                .pageSize(PAGE_SIZE)
                .selectionType(SELECTIONTYPE_DRINK)
                .selectionNos("121,122")
                .build();

        List<Integer> list = requestDTO.getSelectionNosList();
        assertThat(list.size()).isGreaterThan(0);
    }
}
