package com.zoozoo.calendar;

import com.zoozoo.calendar.domain.dto.req.CalCreateRequestDto;
import com.zoozoo.calendar.domain.dto.req.CalDeleteRequestDto;
import com.zoozoo.calendar.domain.dto.req.CalRequestDto;
import com.zoozoo.calendar.domain.dto.req.CalUpdateRequestDto;
import com.zoozoo.calendar.domain.dto.res.CalListResponseDto;
import com.zoozoo.calendar.domain.dto.res.CalResponseDto;
import com.zoozoo.calendar.domain.dto.res.CalResponseDtos;
import com.zoozoo.calendar.persistence.CalendarMapper;
import com.zoozoo.calendar.service.CalendarService;
import com.zoozoo.common.domain.Result;
import com.zoozoo.record.service.RecordService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CalendarTest {
    private int userNo = 1;
    private int calNo = 1;
    private String YM;
    private String filter;
    private int zero = 0;
    private int one = 1;

    @Autowired
    private CalendarService calendarService;
    private RecordService recordService;
    private CalendarMapper calendarMapper;


    @Test
    public void selectCalendar()
    {
        YM = "2022-05";
        filter = null;
        CalRequestDto calRequestDto = new CalRequestDto(userNo, calNo, YM, filter);
        CalResponseDtos calResponseDtos = recordService.selectRecordList(calRequestDto);

        assertThat(calResponseDtos.getCalNo()).isEqualTo(calNo);
        assertThat(calResponseDtos.getFilter()).isEqualTo(filter);

        YM = "2022-05";
        filter = "";
        CalRequestDto calRequestDtoS = new CalRequestDto(userNo, calNo, YM, filter);
        CalResponseDtos calResponseDtosS = recordService.selectRecordList(calRequestDto);

        assertThat(calResponseDtosS.getCalNo()).isEqualTo(calNo);
        assertThat(calResponseDtosS.getFilter()).isEqualTo(filter);

        YM = "2022-06";
        filter = "340";

        CalRequestDto calRequestDtoT = new CalRequestDto(userNo, calNo, YM, filter);
        CalResponseDtos calResponseDtosT = recordService.selectRecordList(calRequestDto);

        assertThat(calResponseDtosT.getCalNo()).isEqualTo(calNo);
        assertThat(calResponseDtosT.getFilter()).isEqualTo(filter);
        assertThat(calResponseDtosT.getDays().get(18).getDisplay()).isEqualTo('Y');
    }

    @Test
    public void selectCalendarList() {
        List<CalListResponseDto> calList = calendarService.selectCalendarList(one);

        assertThat(calList.get(0).getCalNo()).isEqualTo(one);
    }

    @Test
    @Transactional
    public void createCalendar() {
        int bfShareCalCnt = calendarService.selectCalCnt(one);

        CalCreateRequestDto calCreateRequestDto =  new CalCreateRequestDto(one, "TestCalendar");
        calendarService.insertCalendar(calCreateRequestDto);
        int afShareCalCnt = calendarService.selectCalCnt(one);

        assertThat(bfShareCalCnt + 1).isEqualTo(afShareCalCnt);
    }

//    @Test
//    public void deleteCalendar()
//    {
//        int bfShareCalCnt = calendarService.selectCalCnt(one);
//
//        CalDeleteRequestDto calDeleteRequestDto =  new CalDeleteRequestDto(one, 1);
//        Result result = calendarService.deleteCalendar(calDeleteRequestDto);
//        int afShareCalCnt = calendarService.selectCalCnt(one);
//
//        assertThat(bfShareCalCnt-1).isEqualTo(afShareCalCnt);
//    }

    @Test
    @Transactional
    public void updateCalendar()
    {
        String YM = "2022-05";
        CalUpdateRequestDto requestDto = new CalUpdateRequestDto(one, one, "Updated CalName");
        Result result = calendarService.updateCalendar(requestDto);
        CalRequestDto calRequestDto = new CalRequestDto(one, one, YM, "");
    }
}
