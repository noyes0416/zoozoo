package com.zoozoo.likes;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.TextType;
import com.zoozoo.likes.domain.InsertLikesRequestDTO;
import com.zoozoo.likes.domain.DeleteLikesRequestDTO;
import com.zoozoo.likes.service.LikesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class LikesTest {

    private int USER_NO_1= 1;
    private int BOARD_NO_1 = 1;
    private int REPLY_NO_1 = 1;

    private int BOARD_TYPE = TextType.BOARD.getCode();
    private int REPLY_TYPE = TextType.REPLY.getCode();

    @Autowired
    private LikesService likesService;

    @Test
    @Transactional
    public void insertBoardLikes_Test() {
        InsertLikesRequestDTO requestDTO = InsertLikesRequestDTO.builder()
                .type(BOARD_TYPE)
                .typeValue(BOARD_NO_1)
                .build().setUserNo(USER_NO_1);

        Result result = likesService.insertLikes(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

    @Test
    @Transactional
    public void insertReplyLikes_Test() {
        InsertLikesRequestDTO requestDTO = InsertLikesRequestDTO.builder()
                .type(REPLY_TYPE)
                .typeValue(REPLY_NO_1)
                .build().setUserNo(USER_NO_1);

        Result result = likesService.insertLikes(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

    @Test
    @Transactional
    public void deleteBoardLikes_Test() {
        DeleteLikesRequestDTO requestDTO = DeleteLikesRequestDTO.builder()
                .type(BOARD_TYPE)
                .typeValue(BOARD_NO_1)
                .build().setUserNo(USER_NO_1);

        Result result = likesService.deleteLikes(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);

    }
}
