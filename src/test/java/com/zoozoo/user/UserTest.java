package com.zoozoo.user;

import com.zoozoo.common.domain.Result;
import com.zoozoo.common.enumeration.State;
import com.zoozoo.user.domain.UpdateUserNameRequestDTO;
import com.zoozoo.user.domain.UserListRequestDTO;
import com.zoozoo.user.domain.UserListResponseDTO;
import com.zoozoo.user.domain.UserNameListResponseDTO;
import com.zoozoo.user.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserTest {

    private int STATE_ALL = 0;
    private int FIRST_USER_NO = 1;
    private String NEW_USER_NAME = "NEW 애마곤듀";

    @Autowired
    private UserService userService;

    @Test
    public void selectUserList_Test() {
        UserListRequestDTO allListRequestDTO = UserListRequestDTO.builder().state(STATE_ALL).build();
        UserListRequestDTO normalListRequestDTO = UserListRequestDTO.builder().state(State.Normal.getCode()).build();
        UserListRequestDTO abnormalListRequestDTO = UserListRequestDTO.builder().state(State.AbNormal.getCode()).build();

        List<UserListResponseDTO> allList = userService.selectUserList(allListRequestDTO);
        List<UserListResponseDTO> nomalList = userService.selectUserList(normalListRequestDTO);
        List<UserListResponseDTO> abnormalList = userService.selectUserList(abnormalListRequestDTO);

        UserListResponseDTO allUser = allList.get(allList.size() - 1);
        UserListResponseDTO normalUser = allList.get(nomalList.size() - 1);
        //UserListResponseDTO abNormalUser = allList.get(abnormalList.size() - 1);

        assertThat(allUser.getUserNo()).isEqualTo(FIRST_USER_NO);
        assertThat(normalUser.getUserNo()).isEqualTo(FIRST_USER_NO);
        assertThat(abnormalList).isNull();
    }

    @Test
    public void selectUserNameList_Test() {
        String userName = "애마";
        String assertUserName = "애마곤듀";

        List<UserNameListResponseDTO> responseDTO = userService.selectUserByUserName(userName);
        UserNameListResponseDTO firstUser = responseDTO.get(0);

        assertThat(firstUser.getUserName()).isEqualTo(assertUserName);
    }

    @Test
    public void updateUserName_Test() {
        UpdateUserNameRequestDTO requestDTO = UpdateUserNameRequestDTO.builder()
                .newUserName(NEW_USER_NAME).build().setUserNo(FIRST_USER_NO);

        Result result = userService.updateUserName(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
    }
}
