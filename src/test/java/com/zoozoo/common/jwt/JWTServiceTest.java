package com.zoozoo.common.jwt;

import com.zoozoo.common.domain.JWTTokenDTO;
import com.zoozoo.common.domain.OAuthResponseDTO;
import com.zoozoo.signIn.domain.RecreateJWTToken;
import com.zoozoo.user.domain.UserSessionDTO;
import com.zoozoo.user.persistence.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class JWTServiceTest {

    private String googleEmail = "gw.myung@kt.com";
    private int googleSNSType = 1;

    private JWTTokenDTO jwtTokenDTO;
    private OAuthResponseDTO oAuthResponseDTO;

    @Autowired
    private JWTService jwtService;

    @Autowired
    private UserMapper userMapper;

    @Before
    public void doBefore() {
        oAuthResponseDTO = oAuthResponseDTO.builder()
                .email(googleEmail)
                .snsType(googleSNSType)
                .build();

        jwtTokenDTO = jwtService.createToken(oAuthResponseDTO);
    }

    @Test
    @Transactional
    public void createToken_Test() {
        UserSessionDTO userSessionDTO = userMapper.selectUserInfoByTokenKey(jwtTokenDTO);

        assertThat(googleEmail).isEqualTo(userSessionDTO.getEmail());
    }

    @Test
    @Transactional
    public void recreateToken_Test() {
        RecreateJWTToken recreateJWTToken = RecreateJWTToken.builder()
                .email(oAuthResponseDTO.getEmail())
                .snsType(oAuthResponseDTO.getSnsType())
                .build();

        JWTTokenDTO reJWTTokenDTO = jwtService.recreateToken(recreateJWTToken);
        UserSessionDTO userSessionDTO = userMapper.selectUserInfoByTokenKey(reJWTTokenDTO);

        assertThat(googleEmail).isEqualTo(userSessionDTO.getEmail());
    }

    @Test
    public void verifyToken_Test() throws InterruptedException {
        jwtService.verifyToken(jwtTokenDTO.getTokenKey());
    }
}
