package com.zoozoo.common.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UtilTest {

    @Test
    public void createRandomString_Test() {
        System.out.println(CommonUtil.createRandomString());
    }

    @Test
    public void createExpandRandomString_Test() {
        String userName = CommonUtil.createRandomString();
        String expandUserName = CommonUtil.createExpandRandomString(userName);

        assertThat(userName).isNotEqualTo(expandUserName);
    }
}
