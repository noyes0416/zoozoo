package com.zoozoo.boardReply;

import com.zoozoo.boardReply.domain.*;
import com.zoozoo.boardReply.service.BoardReplyService;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class BoardReplyTest {

    private int USERNO = 1;
    private int BOARDNO = 1;
    private int REPLYNO_1DEPTH = 1;
    private int REPLYNO_2DEPTH = 2;

    @Autowired
    private BoardReplyService boardReplyService;

    @Test
    public void getBoardReplyList_Test() {
        ReplyListRequestDTO requestDTO = ReplyListRequestDTO.builder()
                .userNo(USERNO)
                .boardNo(BOARDNO)
                .build();

        List<ReplyListResponseDTO> replyList = boardReplyService.selectAllDepthReplyList(requestDTO);

        ReplyListResponseDTO originReplyList = replyList.get(0);
        ReplyListResponseDTO secondDepthReplyList = originReplyList.getSecondDepthReplyList().get(0);

        assertThat(originReplyList.getUserNo()).isEqualTo(USERNO);
        assertThat(originReplyList.getBoardNo()).isEqualTo(BOARDNO);
        assertThat(originReplyList.getReplyNo()).isEqualTo(REPLYNO_1DEPTH);

        assertThat(secondDepthReplyList.getUserNo()).isEqualTo(USERNO);
        assertThat(secondDepthReplyList.getBoardNo()).isEqualTo(BOARDNO);
    }

    @Test
    @Transactional
    public void insert1depthReply_Test() {
        InsertReplyRequestDTO requestDTO = InsertReplyRequestDTO.builder()
                .boardNo(BOARDNO)
                .content("insertReply Test Content")
                .build().setUserNo(USERNO);

        Result result = boardReplyService.insertReply(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

    @Test
    @Transactional
    public void insert2depthReply_Test() {
        InsertReplyRequestDTO requestDTO = InsertReplyRequestDTO.builder()
                .boardNo(BOARDNO)
                .originReplyNo(REPLYNO_1DEPTH)
                .content("insert2depthReply Test Content")
                .build().setUserNo(USERNO);

        Result result = boardReplyService.insertReply(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

    @Test
    @Transactional
    public void update1depthReply_Test() {
        UpdateReplyRequestDTO requestDTO = UpdateReplyRequestDTO.builder()
                .replyNo(REPLYNO_1DEPTH)
                .content("Update1depthReply Test Content")
                .displayYN(CommonMessage.COMMON_Y)
                .build().setUserNo(USERNO);

        Result result = boardReplyService.updateReply(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

    @Test
    @Transactional
    public void update2depthReply_Test() {
        UpdateReplyRequestDTO requestDTO = UpdateReplyRequestDTO.builder()
                .replyNo(REPLYNO_2DEPTH)
                .content("Update2depthReply Test Content")
                .displayYN(CommonMessage.COMMON_Y)
                .build().setUserNo(USERNO);

        Result result = boardReplyService.updateReply(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

    @Test
    @Transactional
    public void deleteReply_Test() {
        DeleteReplyRequestDTO requestDTO = DeleteReplyRequestDTO.builder()
                .replyNo(REPLYNO_1DEPTH)
                .build().setUserNo(USERNO);

        Result result = boardReplyService.deleteReply(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }
}
