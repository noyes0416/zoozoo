package com.zoozoo.report;

import com.zoozoo.board.persistence.BoardMapper;
import com.zoozoo.boardReply.persistence.BoardReplyMapper;
import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.common.domain.Result;
import com.zoozoo.report.domain.ReportBoardRequestDTO;
import com.zoozoo.report.domain.ReportReplyRequestDTO;
import com.zoozoo.report.persistence.ReportMapper;
import com.zoozoo.report.service.ReportService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ReportTest {

    private int BOARD_NO = 1;
    private int BOARD_WRITER_NO = 1;
    private int REPLY_NO = 1;
    private int REPLY_WRITER_NO = 1;
    private int REPORTER_NO = 2;

    @Autowired
    private ReportMapper reportMapper;

    @Autowired
    private BoardMapper boardMapper;

    @Autowired
    private BoardReplyMapper replyMapper;

    @Autowired
    private ReportService reportService;

    @Test
    @Transactional
    public void reportBoard_isReporterIsBoardWriter_TRUE_Test() {
        ReportBoardRequestDTO requestDTO = ReportBoardRequestDTO.builder()
                .boardNo(BOARD_NO)
                .build().setReporterNo(BOARD_WRITER_NO);

        Result result = reportService.reportBoard(requestDTO);

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.REPORTER_IS_SAME_WITH_WRITER);
    }

    @Test
    @Transactional
    public void reportBoard_isReporterIsBoardWriter_FALSE_Test() {
        ReportBoardRequestDTO requestDTO = ReportBoardRequestDTO.builder()
                .boardNo(BOARD_NO)
                .build().setReporterNo(REPORTER_NO);

        Boolean result = reportMapper.isReporterIsBoardWriter(requestDTO);

        assertThat(result).isEqualTo(false);
    }

    @Test
    @Transactional
    public void reportBoard_Test() {
        ReportBoardRequestDTO requestDTO = ReportBoardRequestDTO.builder()
                .boardNo(BOARD_NO)
                .build().setReporterNo(REPORTER_NO);

        Result result = reportService.reportBoard(requestDTO);

        int boardReportCount = reportMapper.selectBoardReportedCount(requestDTO);
        if (boardReportCount >= CommonMessage.BLOCK_TEXT_BASE_REPORT_COUNT) {
            assertThat(boardMapper.selectBoardCount(requestDTO.getBoardNo())).isEqualTo(0);
        }

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

    @Test
    @Transactional
    public void reportReply_Test() {
        ReportReplyRequestDTO requestDTO = ReportReplyRequestDTO.builder()
                .replyNo(BOARD_NO)
                .build().setReporterNo(REPORTER_NO);

        Result result = reportService.reportReply(requestDTO);

        int replyReportCount = reportMapper.selectReplyReportedCount(requestDTO);
        if (replyReportCount >= CommonMessage.BLOCK_TEXT_BASE_REPORT_COUNT) {
            assertThat(replyMapper.selectBoardReplyCount(requestDTO.getReplyNo())).isEqualTo(0);
        }

        assertThat(result.getCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getMessage()).isEqualTo(CommonMessage.OK_MESSAGE);
    }

}
