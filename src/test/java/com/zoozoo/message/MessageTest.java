package com.zoozoo.message;

import com.zoozoo.common.domain.CommonMessage;
import com.zoozoo.message.domain.*;
import com.zoozoo.message.service.MessageService;
import org.apache.ibatis.annotations.Delete;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MessageTest {

    private int MESSAGE_TYPE_RECEIVED = 1;
    private int MESSAGE_TYPE_SENT = 2;

    private int MESSAGE_NO_1 = 1;
    private String MESSAGE_NO_1_STRING = "1";
    private String MESSAGE_NOS = "1^2^3";

    private int USER_NO_1 = 1;
    private int USER_NO_2 = 2;

    @Autowired
    private MessageService messageService;

    @Test
    public void selectMessageReceivedList_Test() {
        MessageListRequestDTO requestDTO = MessageListRequestDTO.builder()
                .type(MESSAGE_TYPE_RECEIVED)
                .build().setUserNo(USER_NO_1);

        List<MessageSelectResponseDTO> messageList = messageService.selectMessageList(requestDTO);
        MessageSelectResponseDTO messageDTO = messageList.get(0);

        assertThat(messageDTO.getSenderNo()).isEqualTo(USER_NO_2);
    }

    @Test
    public void selectMessageSentList_Test() {
        MessageListRequestDTO requestDTO = MessageListRequestDTO.builder()
                .type(MESSAGE_TYPE_SENT)
                .build().setUserNo(USER_NO_1);

        List<MessageSelectResponseDTO> messageList = messageService.selectMessageList(requestDTO);
        MessageSelectResponseDTO messageDTO = messageList.get(0);

        assertThat(messageDTO.getSenderNo()).isEqualTo(USER_NO_1);
    }

    @Test
    @Transactional
    public void selectMessageDetail_Test() {
        MessageDetailRequestDTO requestDTO = MessageDetailRequestDTO.builder()
                .messageNo(MESSAGE_NO_1)
                .build();

        MessageSelectResponseDTO message = messageService.selectMessageDetail(requestDTO);

        assertThat(message.getMessageNo()).isEqualTo(MESSAGE_NO_1);
        assertThat(message.getReadYN()).isEqualTo(CommonMessage.COMMON_Y);
    }

    @Test
    @Transactional
    public void insertMessage_Test() {
        InsertMessageRequestDTO insertRequestDTO = InsertMessageRequestDTO.builder()
                .receiverNo(USER_NO_2)
                .content("InsertMessage_Test()")
                .build()
                .setSenderNo(USER_NO_1);

        messageService.insertMessage(insertRequestDTO);

        MessageDetailRequestDTO detailRequestDTO = MessageDetailRequestDTO.builder()
                .messageNo(insertRequestDTO.getMessageNo())
                .build();

        MessageSelectResponseDTO message = messageService.selectMessageDetail(detailRequestDTO);

        assertThat(message.getMessageNo()).isEqualTo(insertRequestDTO.getMessageNo());
        assertThat(message.getSenderNo()).isEqualTo(USER_NO_1);
    }

    @Test
    @Transactional
    public void deleteMessage_IsRunning_MULTIPLE_Test() {
        DeleteMessageRequestDTO requestDTO = DeleteMessageRequestDTO.builder()
                .messageNos(MESSAGE_NOS)
                .build()
                .setUserNo(USER_NO_1)
                .setMessageNoList();

        messageService.deleteMessage(requestDTO);
    }

    @Test
    @Transactional
    public void deleteMessage_IsRunning_ONE_Test() {
        DeleteMessageRequestDTO requestDTO = DeleteMessageRequestDTO.builder()
                .messageNos(MESSAGE_NO_1_STRING)
                .build()
                .setUserNo(USER_NO_1)
                .setMessageNoList();

        messageService.deleteMessage(requestDTO);
    }
}
